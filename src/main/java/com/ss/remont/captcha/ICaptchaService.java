package com.ss.remont.captcha;

public interface ICaptchaService {
	
	void processResponse(final String response) throws ReCaptchaInvalidException;

	String getReCaptchaSite();

	String getReCaptchaSecret();
}
