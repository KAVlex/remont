package com.ss.remont.entity;

public enum BidStatus {
    WAITING_RESPONSE, REGISTERED, CANCELED_CLIENT, CANCELED_STATION, COMPLETED, REJECTED, PROCESSING, IN_WORK;
    @Override
    public String toString()
    {
        switch (this){
            case WAITING_RESPONSE: return "Ожидание ответов";
            case REGISTERED: return "Зарегистрировано";
            case CANCELED_CLIENT: return "Отменено клиентом";
            case CANCELED_STATION: return "Отменено станцией";
            case COMPLETED: return "Завершено";
            case REJECTED: return "Отклонено";
            case PROCESSING: return "В обработке";
            case IN_WORK: return "В работе";
            default: throw new IllegalArgumentException();
        }
    }

    public static BidStatus findByString(String s)
    {
        for(BidStatus status : values())
        {
            if(status.toString().equals(s))
                return status;
        }
        throw new EnumConstantNotPresentException(BidStatus.class,s);
    }
}