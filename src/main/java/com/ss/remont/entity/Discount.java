package com.ss.remont.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;

/**
 * Created by Tensky on 08.12.2016.
 */
@Entity
@Table(name = "discount")
public class Discount {

	public Discount() {
	}
	
	public Discount(String title, String info, Images photo, Date date_start, Date date_end,
//                    DiscountStatus status,
			Station station) {
		this.title = title;
		this.info = info;
		this.photo = photo;
		this.date_start = date_start;
		this.date_end = date_end;
//		this.status = status;
		this.station = station;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JsonManagedReference
	private Station station;

	@Column(nullable = false, unique = false, length = 100, name = "title")
	private String title;

	@Column(nullable = false, unique = false, length = 512, name = "info")
	private String info;

	@JoinColumn(nullable = true)
    @OneToOne
    private Images photo;
	public void setPhoto(Images photo){this.photo = photo;}
	public Images getPhoto(){return this.photo;}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date timestamp = new Date();

	@Column(name = "date_start", nullable = false, unique = false)
	private Date date_start;

	@Column(name = "date_end", nullable = false, unique = false)
	private Date date_end;

//	@Column(name = "status", nullable = false, unique = false)
//	private DiscountStatus status;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Station getStation() {
		return this.station;
	}
	// TODO: 08.12.2016 Добавить здесь заголовок, инфо, фото, дату начала акции,
	// дату окончания, статус
	// TODO: 08.12.2016 Добавить в entity.station колонку discount

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfo() {
		return this.info;
	}

	public void setDate_start(Date date_start) {
		this.date_start = date_start;
	}

	public Date getDate_start() {
		return this.date_start;
	}

	public void setDate_end(Date date_end) {
		this.date_end = date_end;
	}

	public Date getDate_end() {
		return this.date_end;
	}

//	public void setStatus(DiscountStatus status) {
//		this.status = status;
//	}

//	public DiscountStatus getStatus() {
//		return this.status;
//	}
}
