package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author KAVlex Год выпуска
 */
@Entity
@Table(name = "car_year")
public class CarYear implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 4, nullable = false)
	private Integer year;

	@JsonBackReference
	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private CarModel model;

	public CarYear() {
	}

	public CarYear(Integer year, CarModel model) {
		this.year = year;
		this.model = model;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setModel(CarModel model) {
		this.model = model;
	}

	public CarModel getModel() {
		return this.model;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
