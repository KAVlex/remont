package com.ss.remont.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonView;
import com.ss.remont.auth.AuthUser;
import com.ss.remont.controller.ViewsJson;

@Entity
@Table(name = "message", indexes = {
	    @Index(columnList = "response_id", name = "response_idx", unique = false) })
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Response response;

	@JsonView(ViewsJson.Message.class)
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp = new Date();

	@JsonView(ViewsJson.Message.class)
	@Column
	private String message;

	@ManyToOne
	private AuthUser recipient;
	
	@Column
	private Boolean read;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AuthUser getRecipient() {
		return recipient;
	}

	public void setRecipient(AuthUser recipient) {
		this.recipient = recipient;
	}

	public Boolean isRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	@JsonView(ViewsJson.Message.class)
	public Boolean isClientMessage(){
		return recipient.equals(response.getStation());
	}
}
