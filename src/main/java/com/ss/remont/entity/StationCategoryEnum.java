package com.ss.remont.entity;


/**
 * Created by Tensky on 25.03.2017.
 */
public enum StationCategoryEnum
{
    AUTO_SERVICE,//Автосервис
    AUTO_SHOP,//Автомагазин
    CAR_WASH,//Автомойка
    MOUNTING,//Шиномонтаж
    BODY_REPAIR,//Кузовной ремонт
    EVACUATOR,//Эвакуатор
    AUTOEXAMINATION,//Автоэкспертиза
    TUNING_ATELIER//Тюнинг ателье
    ;

    @Override
    public String toString()
    {
        switch (this)
        {
            case AUTO_SERVICE:
                return "Автосервис";
            case AUTO_SHOP:
                return "Автомагазин";
            case AUTOEXAMINATION:
                return "Автоэкспертиза";
            case BODY_REPAIR:
                return "Кузовной ремонт";
            case CAR_WASH:
                return "Автомойка";
            case EVACUATOR:
                return "Эвакуатор";
            case MOUNTING:
                return "Шиномонтаж";
            case TUNING_ATELIER:
                return "Тюнинг ателье";
            default:
                throw new IllegalArgumentException();
        }
    }

    public static StationCategoryEnum findByString(String category)
    {
        for (StationCategoryEnum stationCategory :
                values())
        {
            if (category.equals(stationCategory.toString()))
                return stationCategory;
        }
        throw new EnumConstantNotPresentException(StationCategoryEnum.class,category);
    }
}
