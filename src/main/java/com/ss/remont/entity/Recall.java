package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "recall")
public class Recall {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	public void setId(Integer id){this.id = id;}
	public Integer getId(){return this.id;}

	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp = new Date();
	public void setTimestamp(Date timestamp){this.timestamp = timestamp;}
	public Date getTimestamp(){return this.timestamp;}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Client client;
	public void setClient(Client client){this.client = client;}
	public Client getClient(){return this.client;}
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JsonManagedReference
	private Station station;
	public void setStation(Station station){this.station = station;}
	public Station getStation(){return this.station;}
	
	@Column(nullable = false, length = 1)
	private Integer rating;
	public void setRating(Integer rating){this.rating = rating;}
	public Integer getRating(){return this.rating;}

	@Column(nullable = false, length = 256)
	private String comment;
	public void setComment(String comment){this.comment = comment;}
	public String getComment(){return this.comment;}

    @OneToOne
    private Bid bid;
    public void setBid(Bid bid){this.bid = bid;}
    public Bid getBid(){return this.bid;}


    public Recall(){}

    public Recall(Client client,Station station,Bid bid,Integer rating,String comment){
        this.timestamp=new Date();
        this.client=client;
        this.station=station;
        this.rating=rating;
        this.comment=comment;
        this.bid=bid;
    }
}
