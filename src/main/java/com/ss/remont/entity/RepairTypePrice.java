package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by Tensky on 26.12.2016.
 */
@Entity
@Table(name="station_repairs")
public class RepairTypePrice
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public void setId(Integer id){this.id = id;}
    public Integer getId(){return this.id;}

    @ManyToOne
    @JsonManagedReference
    private Station station;
    public void setStation(Station station){this.station = station;}
    public Station getStation(){return this.station;}

    @OneToOne
    private RepairType repairType;
    public void setRepairType(RepairType repairType){this.repairType = repairType;}
    public RepairType getRepairType(){return this.repairType;}

    @Column
    private Double price;
    public void setPrice(Double price){this.price = price;}
    public Double getPrice(){return this.price;}

    public RepairTypePrice(){}
    public RepairTypePrice(Station station,RepairType repairType, Double price)
    {
        this.station=station;
        this.repairType=repairType;
        this.price=price;
    }
}
