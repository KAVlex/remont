package com.ss.remont.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ss.remont.auth.AuthUser;

@Entity
@Table(name = "admin")
@DiscriminatorValue("ADMIN")
public class Admin  extends AuthUser {

}
