package com.ss.remont.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "city")
public class City extends Dictionary {

	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + "]";
	}

    @Embedded
    private Coordinates coordinates;
    public void setCoordinates(Coordinates coordinates){this.coordinates = coordinates;}
    public Coordinates getCoordinates(){return this.coordinates;}

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "city")
    private List<Station> stations;
    public void setStations(List<Station> stations){this.stations = stations;}
    public List<Station> getStations(){return this.stations;}

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "city")
    private List<Bid> bids;
    public void setBids(List<Bid> bids){this.bids = bids;}
    public List<Bid> getBids(){return this.bids;}
    public City(){}

    public City(String name,Double longitude, Double latitude){
        this.name=name;
        this.coordinates.setLongitude(longitude);
        this.coordinates.setLatitude(latitude);
    }
}
