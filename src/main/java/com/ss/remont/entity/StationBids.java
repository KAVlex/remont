package com.ss.remont.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "station_bids", indexes = {
	    @Index(columnList = "station_id", name = "station_idx", unique = false) })
public class StationBids {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Bid bid;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Station station;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modify = new Date();
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	private BidStatus status;
	
	public StationBids(){
	}
	
	public StationBids(Bid bid, Station station, BidStatus status) {
		super();
		this.bid = bid;
		this.station = station;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bid getBid() {
		return bid;
	}

	public void setBid(Bid bid) {
		this.bid = bid;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	public BidStatus getStatus() {
		return status;
	}

	public void setStatus(BidStatus status) {
		this.status = status;
	}
	
}
