package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.ss.remont.controller.ViewsJson;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.swing.*;

import com.ss.remont.repository.StationBidsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * Заявка
 * @author KAVlex
 */
@Entity
@Table(name = "bid")
public class Bid {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd-MM-yyyy hh:mm")
	private Date timestamp = new Date();

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Car car;

	@JsonView({ ViewsJson.Response.class })
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Client client;

	@Column(nullable = false, length = 255)
	private String content;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	private BidStatus status;

	@Column(nullable = true, name = "desired_date")
	@Temporal(TemporalType.DATE)
	private Date desiredDate;

	@Column(name = "need_evacuator", nullable = true)
	private Boolean needEvacuator = false;

	@Column(name = "own_parts", nullable = true)
	private Boolean ownParts = false;

    @Column(name = "need_shipment")
    private Boolean needShipment = false;
    public void setNeedShipment(Boolean needShipment){this.needShipment = needShipment;}
    public Boolean getNeedShipment(){return this.needShipment;}

	@Column(name = "quickly", nullable = true)
	private Boolean quickly = false;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "bid_repairtypes")
	private List<RepairType> repairTypes;

    @OneToMany
    private List<Images> photos;
    public void setPhotos(List<Images> photos){this.photos = photos;}
    public List<Images> getPhotos(){return this.photos;}

    @Enumerated(EnumType.STRING)
    private EvacuatorType evacuatorType;
    public void setEvacuatorType(EvacuatorType evacuatorType){this.evacuatorType = evacuatorType;}
    public EvacuatorType getEvacuatorType(){return this.evacuatorType;}

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Station station;
	
	@Column(name = "location", nullable = true)
	private String location;

    @ManyToOne
    private City city;
    public void setCity(City city){this.city = city;}
    public City getCity(){return this.city;}

    @OneToOne
    private Recall recall;
    public void setRecall(Recall recall){this.recall = recall;}
    public Recall getRecall(){return this.recall;}

    @Column(name="closed", nullable = true)// TODO: 23.01.2017 Заменить на nullable = false
    private Boolean closed=false;
    public void setClosed(Boolean closed){this.closed = closed;}
    public Boolean getClosed(){return this.closed;}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setStatus(BidStatus status) {
		this.status = status;
	}

	public BidStatus getStatus() {
		return this.status;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getClient() {
		return this.client;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Car getCar() {
		return this.car;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return this.content;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Station getStation() {
		return this.station;
	}

	public void setOwnParts(Boolean ownParts) {
		this.ownParts = ownParts;
	}

	public Boolean getOwnParts() {
		return this.ownParts;
	}

	public void setRepairTypes(List<RepairType> repairTypes) {
		this.repairTypes = repairTypes;
	}

	public List<RepairType> getRepairTypes() {
		return this.repairTypes;
	}

	public void setNeedEvacuator(Boolean needEvacuator) {
		this.needEvacuator = needEvacuator;
	}

	public Boolean getNeedEvacuator() {
		return this.needEvacuator;
	}

	public void setDesiredDate(Date desiredDate) {
		this.desiredDate = desiredDate;
	}

	public Date getDesiredDate() {
		return this.desiredDate;
	}

	public Bid() {
		super();
	}

	public Bid(Client client, Station station, BidStatus status, Car car, List<RepairType> repairTypes,
			boolean needEvacuator, boolean ownParts, Date desiredDate, String content, boolean quickly, String location) {
		super();
		this.client = client;
		this.station = station;
		this.status = status;
		this.car = car;
		this.repairTypes = repairTypes;
		this.needEvacuator = needEvacuator;
		this.ownParts = ownParts;
		this.desiredDate = desiredDate;
		this.content = content;
		this.quickly = quickly;
		this.location = location;
	}



	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Boolean getQuickly() {
		return quickly;
	}

	public void setQuickly(Boolean quickly) {
		this.quickly = quickly;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
