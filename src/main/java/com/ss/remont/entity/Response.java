package com.ss.remont.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonView;
import com.ss.remont.controller.ViewsJson;

@Entity
@Table(name = "response")
public class Response {

	@JsonView({ ViewsJson.Response.class })
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@JsonView({ ViewsJson.Response.class })
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Bid bid;


	@JsonView({ ViewsJson.Response.class })
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Station station;

	@JsonView({ ViewsJson.Response.class })
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd-MM-yyyy hh:mm")
	private Date time = new Date();

	@JsonView({ ViewsJson.Response.class })
	@Column(length = 10, nullable = false)
	private Double cost;

	@JsonView({ ViewsJson.Response.class })
	@Column(length = 256, nullable = true)
	private String comment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bid getBid() {
		return bid;
	}

	public void setBid(Bid bid) {
		this.bid = bid;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
