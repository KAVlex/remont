package com.ss.remont.entity;
/**
 * Created by Tensky on 25.03.2017.
 */
public enum EvacuatorType
{
    COMMON_WINCH,//Обычный с лебедкой
    SLIDING_PLATFORM,//Сдвижная платформа
    WITH_MANIPULATOR,//Эвакуатор с манипулятором
    CARGO_TRUCK;//Грузовой эвакуатор


    @Override
    public String toString()
    {
        switch (this)
        {
            case CARGO_TRUCK: return "Грузовой эвакуатор";
            case COMMON_WINCH: return "Обычный с лебедкой";
            case SLIDING_PLATFORM: return "Сдвижная платформа";
            case WITH_MANIPULATOR: return "Эвакуатор с манипулятором";
            default: throw new IllegalArgumentException();
        }
    }

    public static EvacuatorType findByString(String evacType)
    {
        for(EvacuatorType evacuatorType: values())
        {
            if(evacType.equals(evacuatorType.toString()))
                return evacuatorType;
        }
        throw new EnumConstantNotPresentException(EvacuatorType.class,evacType);
    }
}
