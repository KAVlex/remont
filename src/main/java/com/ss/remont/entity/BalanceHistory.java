package com.ss.remont.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ss.remont.auth.AuthUser;

@Entity
@Table(name = "balance_history")
public class BalanceHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	
	@Column(nullable = false, length = 256)
	private String comment;
	

	@JoinColumn(nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Station station;
	
	
	 
	 @Column()
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date modifyDate;


	@JoinColumn(nullable = false)
	 @ManyToOne(fetch = FetchType.EAGER, optional = false)
	 private AuthUser author;
	
	@Column(nullable = false)
	private Double balance;
	 

	 public Double getBalance() {
		return balance;
	}


	public void setBalance(Double balance) {
		this.balance = balance;
	}


	public Date getModifyDate() {
		return modifyDate;
	}


	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	 
	public AuthUser getAuthor() {
		return author;
	}


	public void setAuthor(AuthUser author) {
		this.author = author;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public Station getStation() {
		return station;
	}


	public void setStation(Station station) {
		this.station = station;
	}

}
