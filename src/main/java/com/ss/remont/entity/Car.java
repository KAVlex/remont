package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "car")
public class Car implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private CarMark mark;

	public void setMark(CarMark mark) {
		this.mark = mark;
	}

	public CarMark getMark() {
		return this.mark;
	}

	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private CarModel model;

	public void setModel(CarModel model) {
		this.model = model;
	}

	public CarModel getModel() {
		return this.model;
	}

	// при наличии двунаправленной связи будет достаточно только CarYear?
	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private CarYear year;

	public void setYear(CarYear year) {
		this.year = year;
	}

	public CarYear getYear() {
		return this.year;
	}

	@Column(name = "reg_number", length = 10, nullable = true,unique = true)
	private String regNumber;

    @Column(name = "mileage")
    private Integer mileage = 0;
    public void setMileage(Integer mileage){this.mileage = mileage;}
    public Integer getMileage(){return this.mileage;}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getRegNumber() {
		return this.regNumber;
	}

	@Column(nullable = true, length = 64)
	private String color;

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return this.color;
	}

	public Car() {
	}

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Client client;

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getClient() {
		return this.client;
	}

	public Car(CarMark carMark, CarModel carModel, CarYear carYear, String color, String regNumber, Client client) {
		this.mark = carMark;
		this.model = carModel;
		this.year = carYear;
		this.color = color;
		this.regNumber = regNumber;
		this.client = client;
	}
	
	public Car(CarMark carMark, CarModel carModel, CarYear carYear, Client client) {
		this.mark = carMark;
		this.model = carModel;
		this.year = carYear;
		this.client = client;
	}

    public Car(CarMark carMark, CarModel carModel, CarYear carYear, String color, String regNumber, Integer mileage, Client client)
    {
        this.mark = carMark;
        this.model = carModel;
        this.year = carYear;
        this.color = color;
        this.regNumber = regNumber;
        if(mileage!=null)
            this.mileage = mileage;
        this.client = client;
    }

	@Override
	public String toString() {
		return mark.getName() + " " + model.getName() + " " + year.getYear();
	}

}
