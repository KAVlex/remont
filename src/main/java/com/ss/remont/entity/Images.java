package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ss.remont.auth.AuthUser;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tensky on 14.11.2016.
 */
@Entity
@Table(name = "images")
public class Images {

    //Колонки
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public void setId(Integer id){this.id = id;}
    public Integer getId(){return this.id;}

    @JoinColumn
    @ManyToOne
    private AuthUser authUser;
    public void setAuthUser(AuthUser authUser){this.authUser = authUser;}
    public AuthUser getAuthUser(){return this.authUser;}

    @Column(name = "name", nullable = false)
    private String name;
    public void setName(String name){this.name = name;}
    public String getName(){return this.name;}

    @Column(name = "type", nullable = false)
    private ImagesType type;
    public void setType(ImagesType type){this.type = type;}
    public ImagesType getType(){return this.type;}

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date=new Date();
    public void setDate(Date date){this.date = date;}
    public Date getDate(){return this.date;}

    @Column(name = "path_base",nullable = false)
    private String pathBase;
    public void setPathBase(String pathBase){this.pathBase = pathBase;}
    public String getPathBase(){return this.pathBase;}

    @Column(name = "path_small",nullable = false)
    private String pathSmall;
    public void setPathSmall(String pathSmall){this.pathSmall = pathSmall;}
    public String getPathSmall(){return this.pathSmall;}

    @Column(name="path_medium",nullable = false)
    private String pathMedium;
    public void setPathMedium(String pathMedium){this.pathMedium = pathMedium;}
    public String getPathMedium(){return this.pathMedium;}

    @Column(name="path_large",nullable = false)
    private String pathLarge;
    public void setPathLarge(String pathLarge){this.pathLarge = pathLarge;}
    public String getPathLarge(){return this.pathLarge;}

    public Images(){}
    public Images(AuthUser authUser, String name, ImagesType imagesType,
                  String pathBase, String pathSmall, String pathMedium, String pathLarge)
    {
        this.authUser=authUser;
        this.name=name;
        this.type=imagesType;
        this.pathBase=pathBase;
        this.pathSmall=pathSmall;
        this.pathMedium=pathMedium;
        this.pathLarge=pathLarge;
    }


}
