package com.ss.remont.entity;

import javax.persistence.*;

/**
 * Created by Tensky on 30.12.2016.
 */
@Entity(name = "Faq")
public class Faq
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public void setId(Integer id){this.id = id;}
    public Integer getId(){return this.id;}

    @Column
    private String question;
    public void setQuestion(String question){this.question = question;}
    public String getQuestion(){return this.question;}

    @Column(length = 512)
    private String answer;
    public void setAnswer(String answer){this.answer = answer;}
    public String getAnswer(){return this.answer;}

    public Faq(){}

    public Faq(String question, String answer){
        this.question=question;
        this.answer=answer;
    }
}
