package com.ss.remont.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ss.remont.auth.AuthUser;

@Entity
@Table(name = "client")
@DiscriminatorValue("CLIENT")
public class Client extends AuthUser {

	@JsonManagedReference
	@OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Car> cars;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "client_favorites")
	private List<Station> favorites;

	@JsonManagedReference
	@OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
	private List<Bid> bids;

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public List<Car> getCars() {
		return this.cars;
	}

	public void setBids(List<Bid> bids) {
		this.bids = bids;
	}

	public List<Bid> getBids() {
		return this.bids;
	}

	public void setFavorites(List<Station> favorites) {
		this.favorites = favorites;
	}

	public List<Station> getFavorites() {
		return this.favorites;
	}

}
