package com.ss.remont.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Date;

/**
 * Created by Tensky on 15.12.2016.
 */
@Entity
@Table(name = "order_call")
public class OrderCall {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "phone", length = 20, nullable = false)
	private String phoneNumber;

	@ManyToOne
	@JsonBackReference
	private Station station;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp = new Date();
    public void setTimestamp(Date timestamp){this.timestamp = timestamp;}
    public Date getTimestamp(){return this.timestamp;}

    @Column(name="answered", nullable = false, columnDefinition = "boolean default false")
    private Boolean answered;
    public void setAnswered(Boolean answered){this.answered = answered;}
    public Boolean getAnswered(){return this.answered;}

	public OrderCall(String name, String phoneNumber, Station station) {
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.station = station;
        this.answered=false;
	}


	public OrderCall() {
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Station getStation() {
		return this.station;
	}

}
