package com.ss.remont.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "repair_type")
public class RepairType extends Dictionary {

	@Override
	public String toString() {
		return "RepairType [id=" + id + ", name=" + name + "]";
	}

}
