package com.ss.remont.entity;
/**
 * Created by Tensky on 25.03.2017.
 */
public enum ExaminationType
{
    VISITING,//Выездная экспертиза
    OFFICE;//У офиса


    @Override
    public String toString()
    {
        switch (this)
        {
            case OFFICE: return "Экспертиза у офиса";
            case VISITING: return "Выездная экспертиза";
            default: throw new IllegalArgumentException();
        }
    }

    public static ExaminationType findByString(String examType)
    {
        for(ExaminationType examinationType: values())
        {
            if(examType.equals(examinationType.toString()))
                return examinationType;
        }
        throw new EnumConstantNotPresentException(ExaminationType.class,examType);
    }
}
