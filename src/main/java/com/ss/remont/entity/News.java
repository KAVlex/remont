package com.ss.remont.entity;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "news")
public class News {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp = new Date();

	@Column(nullable = false, length = 128)
	private String title;

	@Column(nullable = false, length = 2048)
	private String content;

    @JoinColumn(nullable = true)
    @OneToOne
    private Images preview_image;
    public void setPreview_image(Images preview_image){this.preview_image = preview_image;}
    public Images getPreview_image(){return this.preview_image;}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

    public News(){}
    public News(String title, String content, Images preview_image)
    {
        this.title=title;
        this.content=content;
        this.preview_image=preview_image;
    }

}
