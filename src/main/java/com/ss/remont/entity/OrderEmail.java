package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tensky on 10.01.2017.
 */

@Entity
@Table(name = "order_email")
public class OrderEmail
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", length = 64, nullable = false)
    private String email;

    @ManyToOne
    @JsonBackReference
    private Station station;

    @Column(name="comment", nullable = false)
    private String comment;
    public void setComment(String comment){this.comment = comment;}
    public String getComment(){return this.comment;}

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp = new Date();
    public void setTimestamp(Date timestamp){this.timestamp = timestamp;}
    public Date getTimestamp(){return this.timestamp;}


    @Column(name="answered", nullable = false, columnDefinition = "boolean default false")
    private Boolean answered;
    public void setAnswered(Boolean answered){this.answered = answered;}
    public Boolean getAnswered(){return this.answered;}

    public OrderEmail(String name, String email, String comment, Station station) {
        this.name = name;
        this.email = email;
        this.comment = comment;
        this.station = station;
        this.answered=false;
    }

    public OrderEmail() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Station getStation() {
        return this.station;
    }
}
