package com.ss.remont.entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Tensky on 30.03.2017.
 */
@Entity(name = "StationCategory")
public class StationCategory
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public void setId(Integer id){this.id = id;}
    public Integer getId(){return this.id;}

    @Enumerated(value = EnumType.STRING)
    private StationCategoryEnum stationCategory;
    public void setStationCategory(StationCategoryEnum stationCategory){this.stationCategory = stationCategory;}
    public StationCategoryEnum getStationCategory(){return this.stationCategory;}

    public StationCategory(){}

    public StationCategory(StationCategoryEnum stationCategoryEnum)
    {
        this.stationCategory = stationCategoryEnum;
    }
}
