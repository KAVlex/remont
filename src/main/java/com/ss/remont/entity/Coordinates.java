package com.ss.remont.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by Tensky on 26.02.2017.
 */
@Embeddable
public class Coordinates
{
    @Column
    private Double longitude;
    public void setLongitude(Double longitude){this.longitude = longitude;}
    public Double getLongitude(){return this.longitude;}

    @Column
    private Double latitude;
    public void setLatitude(Double latitude){this.latitude = latitude;}
    public Double getLatitude(){return this.latitude;}

    public Coordinates(){}
    public Coordinates(Double longitude, Double latitude)
    {
        this.longitude=longitude;
        this.latitude=latitude;
    }
}
