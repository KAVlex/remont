package com.ss.remont.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.ss.remont.auth.AuthUser;

@Entity
@Table(name = "station")
@DiscriminatorValue("STATION")
public class Station extends AuthUser {

	@Embedded
	private Address address = new Address();

	@Column(nullable = true, length = 64)
	private String site;

	@Column(nullable = true, length = 1024)
	private String info;

	@Column(nullable = true)
	private Boolean dealer;

	@Column(nullable = true)
	private Boolean freelancer;

	@Column(nullable = true, length = 10)
	private Double balance;

	@Column(nullable = false, length = 1)
	private Integer rating=0;

	@Column(nullable = true, length = 512)
	private String simplePrice;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "station")
	@JsonBackReference
	private List<Discount> discounts;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "station_images",
            joinColumns = @JoinColumn(name = "station_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "images_id",
                    referencedColumnName = "id"))
    private List<Images> gallery;
    public void setGallery(List<Images> gallery){this.gallery = gallery;}
    public List<Images> getGallery(){return this.gallery;}

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "station")
    @JsonBackReference
    private List<OrderCall> orderCalls;

    @ManyToMany
    private List<StationCategory> stationCategories = new ArrayList<>();
    public void setStationCategories(List<StationCategory> stationCategories){this.stationCategories = stationCategories;}
    public List<StationCategory> getStationCategories(){return this.stationCategories;}

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "station")
    @JsonBackReference
    private List<Recall> recalls;
    public void setRecalls(List<Recall> recalls){this.recalls = recalls;}
    public List<Recall> getRecalls(){return this.recalls;}

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "station")
    @JsonBackReference
    private List<OrderEmail> orderEmails;
    public void setOrderEmails(List<OrderEmail> orderEmails){this.orderEmails = orderEmails;}
    public List<OrderEmail> getOrderEmails(){return this.orderEmails;}

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "station")
    @JsonBackReference
    private List<RepairTypePrice> repairTypePrices;
    public void setRepairTypePrices(List<RepairTypePrice> repairTypePrices){this.repairTypePrices = repairTypePrices;}
    public List<RepairTypePrice> getRepairTypePrices(){return this.repairTypePrices;}

    @ManyToOne
    private City city;
    public void setCity(City city){this.city = city;}
    public City getCity(){return this.city;}

    @JsonBackReference
    @OneToOne(fetch = FetchType.LAZY)
    private StationDirector stationDirector;
    public void setStationDirector(StationDirector stationDirector){this.stationDirector = stationDirector;}
    public StationDirector getStationDirector(){return this.stationDirector;}

    @Column
    private Boolean clientCommunication = true;
    public void setClientCommunication(Boolean clientCommunication){this.clientCommunication = clientCommunication;}
    public Boolean getClientCommunication(){return this.clientCommunication;}
	
	public Address getAddress() {
		return this.address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}	

	public String getInfo() {
		return this.info;
	}

	public boolean getDealer() {
		return this.dealer;
	}

	public Integer getRating() {
		return this.rating;
	}
	
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getSimplePrice() {
		return simplePrice;
	}

	public void setSimplePrice(String simplePrice) {
		this.simplePrice = simplePrice;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public void setDealer(Boolean dealer) {
		this.dealer = dealer;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	public List<Discount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}

	public List<OrderCall> getOrderCalls() {
		return orderCalls;
	}

	public void setOrderCalls(List<OrderCall> orderCalls) {
		this.orderCalls = orderCalls;
	}



	public Boolean getFreelancer() {
		return freelancer;
	}

	public void setFreelancer(Boolean freelancer) {
		this.freelancer = freelancer;
	}


//	public List<RepairType> getRepairs() {
//		return repairs;
//	}
//
//	public void setRepairs(List<RepairType> repairs) {
//		this.repairs = repairs;
//	}

	public String getFullInfo() {
		String br = "<br/>";// newline
		String completeInfo = "";
		String dealerString;
		if (dealer != null && dealer) {
			dealerString = "Является официальным дилером";
		} else {
			dealerString = "Не является официальным дилером";
		}
		String link = "Сайт: " + (this.site != null ? "<a href=\"" + this.site + "\">" + this.site + "</a>" : " - ");// ссылка
		String addr = "<address>" + "Адрес: " + this.address.getAddr() + "</address>";// курсив
																						// адрес
		String rate = "Рейтинг: " + ((this.rating != null) ? this.rating : " - ");
		String information = "<strong>Иформация:</strong>" + br + (this.info != null ? this.info : " - ");
		// String bid = "<a href=\"" + this.site + "\"><img src=\"/common/images/bid.png/\"></img></a>";

		completeInfo = addr + rate + br + dealerString + br + link + br + information;
		return completeInfo;
	}


}
