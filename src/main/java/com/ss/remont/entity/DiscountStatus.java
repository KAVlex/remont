package com.ss.remont.entity;

/**
 * Created by Tensky on 09.12.2016.
 */
public enum DiscountStatus
{
    INACTIVE, ACTIVE
}
