package com.ss.remont.entity;

import javax.persistence.*;

/**
 * Created by Tensky on 16.12.2016.
 */
@Entity
@Table(name = "feedback")
@DiscriminatorValue("FEEDBACK")
public class Feedback {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", nullable = false, length = 64)
	private String name;

	@Column(name = "email", nullable = false, length = 64)
	private String email;

	@Column(name = "comment", nullable = false, length = 512)
	private String comment;
	
	@Column(name = "active", nullable = false, columnDefinition = "boolean default true" )
	private Boolean active;

	@Column(nullable = true, length = 20)
	private String phone;

	public Feedback() {
	}

	public Feedback(String name, String email, String comment, String phone) {
		this.name = name;
		this.email = email;
		this.comment = comment;
		this.phone = phone;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return this.email;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return this.comment;
	}
	
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
