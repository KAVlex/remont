package com.ss.remont.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by Tensky on 18.02.2017.
 */
@Entity(name="StationDirector")
public class StationDirector
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    public void setId(Integer id){this.id = id;}
    public Integer getId(){return this.id;}


    @Column(nullable = false,length = 60)
    private String lastName;
    public void setLastName(String lastName){this.lastName = lastName;}
    public String getLastName(){return this.lastName;}

    @Column(nullable = false,length = 60)
    private String firstName;
    public void setFirstName(String firstName){this.firstName = firstName;}
    public String getFirstName(){return this.firstName;}

    @Column(nullable = false, length = 60)
    private String patronymic;
    public void setPatronymic(String patronymic){this.patronymic = patronymic;}
    public String getPatronymic(){return this.patronymic;}

    @Column(nullable = false, length = 12)
    private String phoneNumber;
    public void setPhoneNumber(String phoneNumber){this.phoneNumber = phoneNumber;}
    public String getPhoneNumber(){return this.phoneNumber;}

    @Column(nullable = false,length = 60)
    private String email;
    public void setEmail(String email){this.email = email;}
    public String getEmail(){return this.email;}

    @JsonManagedReference
    @OneToOne(fetch = FetchType.LAZY,mappedBy = "stationDirector")
    private Station station;
    public void setStation(Station station){this.station = station;}
    public Station getStation(){return this.station;}

    public StationDirector(){}
    public StationDirector(String firstName, String patronymic, String lastName, String email, String phoneNumber, Station station)
    {
        this.firstName=firstName;
        this.patronymic=patronymic;
        this.lastName=lastName;
        this.email=email;
        this.phoneNumber=phoneNumber;
        this.station=station;
    }
}
