package com.ss.remont.entity;

public enum SmsStatus {
	/**
	 * В очереди на отправку
	 */
	QUEUE,

	/**
	 * В процессе отправки
	 */
	PROCESS,

	/**
	 * Успешно отправлено
	 */
	SUCCESS,

	/**
	 * Ошибка при отправке
	 */
	ERROR;

    @Override
    public String toString()
    {
        switch (this){
            case ERROR: return "Ошибка при отправке";
            case SUCCESS: return "Успешно отправлено";
            case PROCESS: return "В процессе отправки";
            case QUEUE: return "В очереди на отправку";
            default: throw new IllegalArgumentException();
        }
    }

    public static SmsStatus findByString(String s)
    {
        for(SmsStatus status : values())
        {
            if(status.toString().equals(s))
                return status;
        }
        return null;
    }
}
