package com.ss.remont.auth;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonView;
import com.ss.remont.controller.ViewsJson;
import com.ss.remont.entity.Images;


@Entity
@Table(name = "auth_user")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "role", discriminatorType=DiscriminatorType.STRING)
public class AuthUser {

	@JsonView({ ViewsJson.Response.class })
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private AuthorityType type;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "role", nullable = false, insertable = false, updatable = false)
	private Role role;
	
	@Column(nullable = true)
	private Boolean locked;
	
	@JsonView({ ViewsJson.Response.class })
	@Column(nullable = false, unique = true, length = 100)
	private String email;
	
	@Column(name = "email_notice", nullable = true)
	private Boolean emailNotice = true;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}


	public AuthorityType getType() {
		return type;
	}

	public void setType(AuthorityType type) {
		this.type = type;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
	
	@Column(nullable = false, length = 100)
	private String name;

	
	@Column(nullable = true, length = 20)
	private String phone;

    @JoinColumn(nullable = true)
    @OneToOne
    private Images photo;
    public void setPhoto(Images photo){this.photo = photo;}
    public Images getPhoto(){return this.photo;}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
	public Boolean isEmailNotice() {
		return emailNotice;
	}

	public void setEmailNotice(Boolean emailNotice) {
		this.emailNotice = emailNotice;
	}
}
