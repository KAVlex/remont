package com.ss.remont.auth;

public enum AuthorityType {
	SIMPLE, VK, OK, FACEBOOK, TWITTER, GOOGLE, LINKEDIN;
}
