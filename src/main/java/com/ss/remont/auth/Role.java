package com.ss.remont.auth;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority{

	CLIENT, STATION, ADMIN;
	
	@Override
	public String getAuthority() {
		return toString();
	}

}
