package com.ss.remont.auth;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.ss.remont.repository.ClientRepository;
import com.ss.remont.repository.StationRepository;

@Component
public class CurrentUserImpl implements CurrentUser {

	@Autowired
	ClientRepository clientRepository;
	
	@Autowired
	StationRepository stationRepository;
	
	@Override
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	@Override
	public Principal getPrincipal() {
		return (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	@Override
	public AuthUser getRegistrationAuth() {
		RegistrationAuth registrationAuth = (RegistrationAuth) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		return registrationAuth.getRegistrationAuth();
	}

	@Override
	public AuthUser getUser() {
		AuthUser authUser = getRegistrationAuth();
		switch (authUser.getRole()) {
		case CLIENT:
			return clientRepository.findOne(authUser.getId());
		case STATION:
			return stationRepository.findOne(authUser.getId());
		default:
			return authUser;
		}
	}

	@Override
	public boolean isAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication() != null &&
				 SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
				 !(SecurityContextHolder.getContext().getAuthentication() 
				          instanceof AnonymousAuthenticationToken);
	}

	@Override
	public boolean isClient() {
		AuthUser authUser = getRegistrationAuth();
		return authUser.getRole().equals(Role.CLIENT);
	}

}
