package com.ss.remont.auth;

import com.ss.remont.auth.role.ClientRegistrationFactory;
import com.ss.remont.auth.role.StationRegistrationFactory;
import com.ss.remont.service.SaveUser;

public abstract class RoleRegistrationFactory {
	
	
	protected AuthUser authUser;
	private Role role;
	protected SaveUser saveUser;
	

	public RoleRegistrationFactory(){
		this.authUser = generateUser();
	}
	
	
	protected abstract AuthUser generateUser();
	public abstract void saveAuthWithRole();
	
	
	public AuthUser getAuthUser() {
		return authUser;
	}
	
	
	public void importRegistrationForm(EmailRegistrationForm form){
		authUser.setName(form.getName());
		authUser.setPhone(form.getPhone());
		authUser.setLocked(true);
		authUser.setType(AuthorityType.SIMPLE);
		authUser.setRole(role);
		authUser.setEmail(form.getEmail());
	}

	
	public static RoleRegistrationFactory getFactory(SaveUser saveUser,Role role){
		RoleRegistrationFactory result = null;
		if(role.equals(Role.CLIENT)){
			result = new ClientRegistrationFactory();
		}else{
			result = new StationRegistrationFactory();
		}
		result.role = role;
		result.saveUser = saveUser;
		return result;
	}
	
	public Role getRole() {
		return role;
	}


}
