package com.ss.remont.auth;


public class EmailRegistrationForm {
	
	private String name;
    private String email;
    private String phone;
    private String role;
    private String password;
    private String repass;
    private String address;
	private Double latitude;
    private Double longitude;
    private boolean freelancer = false;
	private boolean dealer = false;
    
	public boolean isFreelancer() {
		return freelancer;
	}
	public void setFreelancer(Boolean freelancer) {
		if(freelancer!=null)
		  this.freelancer = freelancer;
	}
	
    public boolean isDealer() {
		return dealer;
	}
	public void setDealer(boolean dealer) {
		this.dealer = dealer;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRepass() {
		return repass;
	}
	public void setRepass(String repass) {
		this.repass = repass;
	}
    
}
