package com.ss.remont.auth.type;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.ss.remont.auth.EmailActivateForm;
import com.ss.remont.auth.EmailRegistrationForm;
import com.ss.remont.auth.RegistrationTypeFactory;
import com.ss.remont.auth.Role;
import com.ss.remont.auth.SimpleAuthUser;
import com.ss.remont.mail.Confirm;
import com.ss.remont.mail.template.RegistrationTemplate;

public class SimpleAuthTypeFactory extends RegistrationTypeFactory{

	@Override
	public String afterSave(Object dbObject,Object registrationForm) {
		EmailRegistrationForm erf = (EmailRegistrationForm)registrationForm;
		SimpleAuthUser sau = (SimpleAuthUser)dbObject;
		if(authUser.getRole().equals(Role.CLIENT)){
			Confirm confirm = new Confirm();
			
			confirm.setEmail(erf.getEmail());
		    String check = saveUser.getEmailCheck(erf.getEmail(),sau.getPassword());
		    String activationLink = "http://localhost:8080/registration/activateUser?id=" + 
                                    String.valueOf(sau.getId());
		    try {
				activationLink += "&check=" + URLEncoder.encode(check,"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		    confirm.setActivationLink(activationLink);
		    confirm.setPassword(erf.getPassword());
		    RegistrationTemplate tempalte = new RegistrationTemplate(confirm);
		    saveUser.mailService.sendEmail(tempalte);
		}
		return null;
	}
	
	@Override
	public String activateUser(Object dbObject,Object activationForm) {
		EmailActivateForm eaf  = (EmailActivateForm)activationForm;
	    SimpleAuthUser sau = (SimpleAuthUser)dbObject;
		String trueCheck = saveUser.getEmailCheck(sau.getAuthUser().getEmail(), sau.getPassword());
		System.out.println("True=" + trueCheck);
		System.out.println("USER=" + eaf.getCheck());
		if(trueCheck.equals(eaf.getCheck())){
			  sau.getAuthUser().setLocked(false);
		}
		return null;
	}

	
	@Override
	public Object saveUser(Object registrationForm) {
		EmailRegistrationForm erf = (EmailRegistrationForm)registrationForm;
		SimpleAuthUser sau = new SimpleAuthUser();
		sau.setAuthUser(authUser);
		sau.setPassword(erf.getPassword());
		sau = saveUser.simpleAuthUserRepository.save(sau);
		return sau;
	}

}
