package com.ss.remont.auth;

import com.ss.remont.auth.type.SimpleAuthTypeFactory;
import com.ss.remont.service.SaveUser;

public abstract class RegistrationTypeFactory {
	

	protected AuthUser authUser;
	protected SaveUser saveUser;
	
	
	
	public abstract String afterSave(Object dbObject,Object registrationForm);
	
	public abstract String activateUser(Object dbObject,Object activationForm);
	
	public abstract Object saveUser(Object registrationForm);
	
	public static RegistrationTypeFactory getFactory(AuthUser authUser,SaveUser saveUser){
		RegistrationTypeFactory result = null;
		if(authUser.getType().equals(AuthorityType.SIMPLE)){
		   result = new SimpleAuthTypeFactory(); 	
		}
		if(result!=null){
			result.authUser = authUser;
			result.saveUser = saveUser;
		}
		return result;
	}
	
	

}
