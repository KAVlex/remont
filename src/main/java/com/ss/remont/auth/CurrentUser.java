package com.ss.remont.auth;

import java.security.Principal;

import org.springframework.security.core.Authentication;

public interface CurrentUser extends RegistrationAuth {

	public Authentication getAuthentication();

	public Principal getPrincipal();

	public AuthUser getUser();
	
	public boolean isAuthenticated(); 
	
	public boolean isClient();
}
