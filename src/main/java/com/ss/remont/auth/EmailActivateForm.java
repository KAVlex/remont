package com.ss.remont.auth;

public class EmailActivateForm {
	
	private Integer id;
	
	private String check;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCheck() {
		return check;
	}

	public void setCheck(String check) {
		this.check = check;
	}

}
