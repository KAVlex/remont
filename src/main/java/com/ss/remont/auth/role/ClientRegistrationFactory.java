package com.ss.remont.auth.role;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.RoleRegistrationFactory;
import com.ss.remont.entity.Client;


public class ClientRegistrationFactory extends RoleRegistrationFactory {

	
	@Override
	protected AuthUser generateUser() {
		return new Client();
	}

	@Override
	public void saveAuthWithRole() {
		saveUser.clientRepository.save((Client)authUser);
	}
	

}
