package com.ss.remont.auth.role;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.EmailRegistrationForm;
import com.ss.remont.auth.RoleRegistrationFactory;
import com.ss.remont.entity.Address;
import com.ss.remont.entity.Station;

public class StationRegistrationFactory extends RoleRegistrationFactory {



	@Override
	public void importRegistrationForm(EmailRegistrationForm form) {
		Station station = (Station)getAuthUser();
		Double balance = new Double(0);
		station.setBalance(balance);
		Address address = new Address();
		address.setAddr(form.getAddress());
		address.setLatitude(form.getLatitude());
		address.setLongitude(form.getLongitude());
		station.setAddress(address);
		station.setDealer(form.isDealer());
		station.setFreelancer(form.isFreelancer());
		super.importRegistrationForm(form);
	}

	@Override
	protected AuthUser generateUser() {
		return new Station();
	}

	@Override
	public void saveAuthWithRole() {
	  saveUser.stationRepository.save((Station)authUser);	
	}


}
