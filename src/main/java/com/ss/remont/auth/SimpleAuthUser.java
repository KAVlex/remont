package com.ss.remont.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import com.ss.remont.utils.DigestUtils;



@Entity
@Table(name = "simple_auth_user")
public class SimpleAuthUser implements RegistrationAuth,
                                       UserDetails{
	
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable=false, length=100)
	private String password;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="authuser_id")
	private AuthUser authUser; 
	
	
	public AuthUser getAuthUser() {
		return authUser;
	}

	public void setAuthUser(AuthUser authUser) {
		this.authUser = authUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@PrePersist
	@PreUpdate
    void onPrePersist() {
    	this.password = DigestUtils.md5(this.password);
    }
	/*
	public boolean checkPassword(String password) {
		String hex = DigestUtils.md5(password);
	    return this.password.equals(hex);
	}*/

	@Override
	public AuthUser getRegistrationAuth() {
		return authUser;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		SimpleGrantedAuthority sia = new SimpleGrantedAuthority(authUser.getRole().toString());
		List<SimpleGrantedAuthority> authList = new ArrayList<>();
		authList.add(sia);
		return authList;
	}

	@Override
	public String getUsername() {
		return getAuthUser().getPhone();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !getAuthUser().getLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return !getAuthUser().getLocked();
	}

	
	
}
