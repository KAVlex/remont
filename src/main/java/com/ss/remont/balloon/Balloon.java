package com.ss.remont.balloon;

import com.ss.remont.entity.Response;
import com.ss.remont.entity.Station;
import com.ss.remont.repository.StationRepository;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.Model;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Tensky on 16.11.2016.
 */

public class Balloon
{

    @Autowired
    StationRepository stationRepository;

    String type = "FeatureCollection";
    Features[] features;


    public void setFeatures(Features[] features)
    {
        this.features = features;
    }

    /**
     * Формирование объеккта Balloon, который содержит в себе все метки и
     * информацию о них.
     *
     * @param stationRepository для работы с таблицы <code>station</code>
     * @return Balloon со всеми метками
     */
    public void getAllBalloons(StationRepository stationRepository)
    {

        List<Station> all = stationRepository.findAll();
        fillStations(all);
    }

    public Balloon()
    {
        this.features = null;
    }



    public <T> Balloon(List<T> someList) throws IOException, TemplateException
    {
        T t;
        if (someList == null)
        {
            this.features = null;
            return;
        }
        else if (someList.size() < 1)
        {
            this.features = null;
            return;
        }
        t = someList.get(0);
        if (t instanceof Station)
        {
            fillStations((List<Station>) someList);
        }
        else if (t instanceof Response)
        {
            fillResponses((List<Response>) someList);
        }
        else
        {
            this.features = null;
        }
    }

    private void fillResponses(List<Response> responses) throws IOException, TemplateException
    {
        Features[] features = new Features[responses.size()];
        int i = 0;
        for (Response response : responses)
        {
            Station station = response.getStation();
            Geometry geometry = new Geometry("Point", new double[]{station.getAddress().getLongitude(), station.getAddress().getLatitude()});
//            Properties properties = new Properties(String.valueOf(response.getCost()),makeResponseBalloonContent(response), response.getCost().toString(), station.getName());
            Properties properties = new Properties(String.valueOf(response.getCost()), makeResponseBalloonContent(response));
            Features feature = new Features("Feature", i, geometry, properties);
            features[i++] = feature;
        }
        this.features = features;
    }

    private String makeResponseBalloonContent(Response response) throws IOException, TemplateException
    {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, Object> model = modelAndView.getModel();
        model.put("response",response);
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_25);
        configuration.setDirectoryForTemplateLoading(new File("src/main/resources/templates/balloon"));
        Template template =configuration.getTemplate("balloonContent.ftl", "UTF-8");

        return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
    }

    private void fillStations(List<Station> stations)
    {
        // Массив меток, размером в количество записей в таблице station
        Features[] features = new Features[stations.size()];

        // ID метки (используется в конструкторе Feature)
        int i = 0;
        for (Station station : stations)
        {
            // Тут координаты метки
            Geometry geometry = new Geometry("Point", new double[]{station.getAddress().getLongitude(), station.getAddress().getLatitude()});
            // Тут информация о метках (содержание баллуна и хинт)
            Properties properties = new Properties(station.getFullInfo(), station.getInfo(), station.getInfo());
            // Метка с координатами и информацией помещается в массив меток
            Features feature = new Features("Feature", i, geometry, properties);
            String bid = "<br/><a class='btn' href=\"" + "/new-bid/" + station.getId() + "\">Оставить заявку</a><br/>";
            feature.getProperties().setBalloonContent(feature.getProperties().getBalloonContent() + bid);
            features[i++] = feature;
        }
        this.features = features;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Features[] getFeatures()
    {
        return features;
    }

}
