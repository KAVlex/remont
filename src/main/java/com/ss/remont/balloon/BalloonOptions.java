package com.ss.remont.balloon;

import javax.validation.constraints.NotNull;

/**
 * Created by Tensky on 19.01.2017.
 */
public class BalloonOptions
{
    private String preset;
    public void setPreset(String preset){this.preset = preset;}
    public String getPreset(){return this.preset;}

    private Boolean draggable=false;
    public void setDraggable(Boolean draggable){this.draggable = draggable;}
    public Boolean getDraggable(){return this.draggable;}

//    private String iconLayout=BalloonOptionsKeyWords.DEFAULT_IMAGE_WITH_CONTENT;
//    public void setIconLayout(String iconLayout){this.iconLayout = iconLayout;}
//    public String getIconLayout(){return this.iconLayout;}


    public BalloonOptions(){
        this.preset= BalloonOptionsKeyWords.ISLANDS_GREEN_STRETCHY_ICON;
    }

    public BalloonOptions(String preset, @NotNull Boolean draggable){
        this.preset=preset;
        this.draggable=draggable;
    }

    public BalloonOptions(java.lang.String preset){
        this.preset=preset;
    }
}
