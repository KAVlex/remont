package com.ss.remont.balloon;

/**
 * Created by Tensky on 20.01.2017.
 */
public class BalloonOptionsKeyWords
{
    public static final String ISLANDS_BLUE_STRETCHY_ICON = "islands#blueStretchyIcon";
    public static final String ISLANDS_ORANGE_STRETCHY_ICON = "islands#orangeStretchyIcon";
    public static final String ISLANDS_DARKBLUE_STRETCHY_ICON = "islands#darkblueStretchyIcon";
    public static final String ISLANDS_PINK_STRETCHY_ICON = "islands#pinkStretchyIcon";
    public static final String ISLANDS_DARKGREEN_STRETCHY_ICON = "islands#darkgreenStretchyIcon";
    public static final String ISLANDS_RED_STRETCHY_ICON = "islands#redStretchyIcon";
    public static final String ISLANDS_DARKORANGE_STRETCHY_ICON = "islands#darkorangeStretchyIcon";
    public static final String ISLANDS_VIOLET_STRETCHY_ICON = "islands#violetStretchyIcon";
    public static final String ISLANDS_GREEN_STRETCHY_ICON = "islands#greenStretchyIcon";
    public static final String ISLANDS_WHITE_STRETCHY_ICON = "islands#whiteStretchyIcon";
    public static final String ISLANDS_GREY_STRETCHY_ICON = "islands#greyStretchyIcon";
    public static final String ISLANDS_YELLOW_STRETCHY_ICON = "islands#yellowStretchyIcon";
    public static final String ISLANDS_LIGHTBLUE_STRETCHY_ICON = "islands#lightblueStretchyIcon";
    public static final String ISLANDS_BROWN_STRETCHY_ICON = "islands#brownStretchyIcon";
    public static final String ISLANDS_NIGHT_STRETCHY_ICON = "islands#nightStretchyIcon";
    public static final String ISLANDS_BLACK_STRETCHY_ICON = "islands#blackStretchyIcon";

    public static final String DEFAULT_IMAGE_WITH_CONTENT ="default#imageWithContent";
    public static final String DEFAULT_IMAGE = "default#image";
}
