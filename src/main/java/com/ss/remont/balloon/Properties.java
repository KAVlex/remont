package com.ss.remont.balloon;

/**
 * Created by Tensky on 16.11.2016.
 */
public class Properties
{
    /**
     * Содержимое балуна (текст)
     */
    private String balloonContent;
    public void setBalloonContent(String balloonContent){this.balloonContent = balloonContent;}
    public String getBalloonContent(){return this.balloonContent;}


    /**
     * "Еще одна метка" как написано на сайте
     * UPD: как оказалось это если более одного объекта находятся в непосредственной близости
     * друг от друга в пределах кластера, то "Еще одна метка", это как кнопка, ссылкой на описание своей метки,
     * желательно изменить значение clusterCaption на название станции
     */
    private String clusterCaption;
    public void setClusterCaption(String clusterCaption){this.clusterCaption = clusterCaption;}
    public String getClusterCaption(){return this.clusterCaption;}


    /**
     * Текст подсказки
     */
    private String hintContent;
    public void setHintContent(String hintContent){this.hintContent = hintContent;}
    public String getHintContent(){return this.hintContent;}



    private String iconContent;
    public void setIconContent(String iconContent){this.iconContent = iconContent;}
    public String getIconContent(){return this.iconContent;}

//    public Properties(String balloonContent, String hintContent)
//    {
//        this.balloonContent = balloonContent;
//        this.hintContent=hintContent;
//        this.clusterCaption="Еще одна метка";
//    }

    public Properties(String balloonContent, String hintContent, String clusterCaption)
    {
        this.balloonContent=balloonContent;
        this.hintContent=hintContent;
        this.clusterCaption=clusterCaption;
    }

    public Properties(String iconContent, String balloonContent)
    {
        this.iconContent=iconContent;
        this.balloonContent=balloonContent;
    }

    public Properties(String iconContent, String balloonContent, String hintContent, String clusterCaption)
    {
        this.iconContent=iconContent;
        this.balloonContent=balloonContent;
        this.hintContent=hintContent;
        this.clusterCaption=clusterCaption;
    }
}
