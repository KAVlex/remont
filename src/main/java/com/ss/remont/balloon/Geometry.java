package com.ss.remont.balloon;

/**
 * Created by Tensky on 16.11.2016.
 */
public class Geometry
{
    String type;
    double[] coordinates;

    public Geometry(String type,double[] coordinates)
    {
        this.type=type;
        this.coordinates=coordinates;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}
}
