package com.ss.remont.balloon;

/**
 * Created by Tensky on 16.11.2016.
 */
public class Features
{
    String type;
    int id;
    Geometry geometry;
    Properties properties;

    public Features(String type, int id, Geometry geometry, Properties properties)
    {
        this.type=type;
        this.id=id;
        this.geometry=geometry;
        this.properties=properties;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
}
