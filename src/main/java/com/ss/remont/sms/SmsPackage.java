package com.ss.remont.sms;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmsPackage {

	private String key;
	private String hash;

	private List<SmsDTO> smsDTOs;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public List<SmsDTO> getSmsDTOs() {
		return smsDTOs;
	}

	public void setSmsDTOs(List<SmsDTO> smsDTOs) {
		this.smsDTOs = smsDTOs;
	}

	@Override
	public String toString() {
		return "SmsPackage [key=" + key + ", hash=" + hash + ", smsDTOs=" + smsDTOs + "]";
	}

}
