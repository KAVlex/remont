package com.ss.remont.sms;

import java.util.Calendar;

import com.ss.remont.utils.DigestUtils;

public class HashGenerator {
	
	private static final String REQUEST_KEYWORD = "REQUEST";
	private static final String RESPONSE_KEYWORD = "RESPONSE";
	private static final int SYNC_SECONDS = 600;
	private static final int ROUNDED_MINUTE = 5;

	public static boolean checkRequestHash(String key, String hash){
		boolean isSynchronized = false;
		try {
			Calendar calendar = Calendar.getInstance();
			Calendar keyCalendar = Calendar.getInstance();
			keyCalendar.setTimeInMillis(Long.valueOf(key));
			long secondsBetween = Math.abs((calendar.getTimeInMillis() - keyCalendar.getTimeInMillis())/1000);
			isSynchronized = secondsBetween < SYNC_SECONDS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return generateRequestHash(key).equals(hash) && isSynchronized;
	}
	
	public static String generateRequestHash(String key){
		return generateKey(REQUEST_KEYWORD, key);
	}
	
	public static String generateResponseHash(String key){
		return generateKey(RESPONSE_KEYWORD, key);
	}
	
	private static String generateKey(String keyword, String key){
		return DigestUtils.md5(keyword + key);
	}
	
	@SuppressWarnings("unused")
	private static int getRoundedMinutes(Calendar calendar){
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % ROUNDED_MINUTE;
		int add = mod < (ROUNDED_MINUTE/2 + 1) ? -mod : (ROUNDED_MINUTE - mod);
		calendar.add(Calendar.MINUTE, add);
		return calendar.get(Calendar.MINUTE);
	}

}
