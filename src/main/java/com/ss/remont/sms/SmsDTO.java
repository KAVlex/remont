package com.ss.remont.sms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ss.remont.entity.Sms;
import com.ss.remont.entity.SmsStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmsDTO {

	private Long id;
	private String phone;
	private String message;
	private SmsStatus status;

	public SmsDTO(){
	}
	
	public SmsDTO(Sms sms){
		this.id = sms.getId();
		this.phone = sms.getPhone();
		this.message = sms.getMessage();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SmsStatus getStatus() {
		return status;
	}

	public void setStatus(SmsStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SmsDTO [id=" + id + ", phone=" + phone + ", message=" + message + ", status=" + status + "]";
	}

}
