package com.ss.remont.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.context.annotation.Bean;

import com.ss.remont.controller.URIPath;
import com.ss.remont.service.AuthUserDetailsService;
import com.ss.remont.utils.DigestUtils;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Integer TOKEN_VALIDITY_SECONDS = 86400;
	private static final String REMEMBER_ME_PARAM = "remember-me-param";
	private static final String MY_REMEMBER_ME = "my-remember-me";
	private static final String JSESSIONID = "JSESSIONID";
	
	@Autowired
	DataSource dataSource;

	@Autowired
	AuthUserDetailsService authUserDetailsService;

	public class MD5Encoder implements PasswordEncoder {

		@Override
		public boolean isPasswordValid(String src, String userInput, Object arg2) {
			System.out.println("db=" + src);
			userInput = DigestUtils.md5(userInput);
			System.out.println("userInput=" + userInput);
			return userInput.equals(src);
		}

		@Override
		public String encodePassword(String src, Object arg1) {
			System.out.println("enc=" + src);
			return DigestUtils.md5(src);
		}

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	        http
	        .authorizeRequests()
	        .antMatchers(URIPath.BASE, URIPath.UPLOAD_FILE,
	        		URIPath.DISCOUNTS + URIPath.PATH_ALL_DIRECTORIES,
	        		URIPath.NEWS, URIPath.NEWS_VIEW + URIPath.PATH_ALL_CHARACTERS, URIPath.NEWS_NEXTPORTION,
	        		URIPath.MAP, URIPath.MAP_GET_ALL_BALLOONS, URIPath.MAP_GET_FILTER, URIPath.MAP_SEND_FILTER, 
	                URIPath.USER_QUEUE_MESSAGES,
	                URIPath.APP + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.TOPIC_ACTIVE,
	                URIPath.CHAT + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.COMMON + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.MAIL + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.WEBJARS + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.RESOURCES + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.REGISTRATION + URIPath.ALL_CHARACTERS,
	                URIPath.API + URIPath.PATH_ALL_CHARACTERS,
	                URIPath.TEST_UPLOAD,
	                URIPath.GET_MODELS,
	                URIPath.STATION + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.NEW_BID,
	                URIPath.NEW_BID + URIPath.PATH_ALL_CHARACTERS,
	                URIPath.AUTOSERVICE + URIPath.PATH_ALL_DIRECTORIES,
                    URIPath.IMAGES  +  URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.FEEDBACK,
	                URIPath.FEEDBACK + URIPath.PATH_ALL_CHARACTERS,
	                URIPath.FILES + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.SEARCH,
	                URIPath.SEARCH + URIPath.PATH_ALL_DIRECTORIES,
	                URIPath.FAQ,
	                URIPath.SMS_QUEUE, URIPath.SMS_MARK, URIPath.SMS_MARK_ID,
	                URIPath.REPAIR_PASSWORD)
	        	.permitAll()
	            .antMatchers( URIPath.STATIONS_CARD,
	                          URIPath.STATIONS_CARD + URIPath.PATH_ALL_DIRECTORIES,
	                          URIPath.NEWS_CREATE,
	                          URIPath.ADMIN + URIPath.PATH_ALL_DIRECTORIES,
	                          URIPath.FAQ_EDIT + URIPath.PATH_ALL_DIRECTORIES).hasAuthority("ADMIN")
	            .antMatchers(URIPath.ADMIN_REPORT_STATION_BALANCE).hasAnyAuthority("ADMIN","STATION")
	            .antMatchers(URIPath.RESPONSE_ADD).hasAuthority("STATION")
	            
	        	.anyRequest().authenticated()
	        	.and()
	        	.formLogin()
	        		.loginPage(URIPath.LOGIN)
	        			.usernameParameter("phone").passwordParameter("password")
	        				.permitAll()
	        	.and()
				.logout().logoutRequestMatcher(new AntPathRequestMatcher(URIPath.LOGOUT)).clearAuthentication(true)
				.logoutSuccessUrl(URIPath.BASE).deleteCookies(JSESSIONID).invalidateHttpSession(true)
	        			.permitAll();
	        
			http.rememberMe(). 
			tokenRepository(persistentTokenRepository()).
	                rememberMeParameter(REMEMBER_ME_PARAM).
	                rememberMeCookieName(MY_REMEMBER_ME).
	                tokenValiditySeconds(TOKEN_VALIDITY_SECONDS);
			
			http.sessionManagement().invalidSessionUrl(URIPath.LOGIN);
		http.csrf().ignoringAntMatchers(URIPath.SMS_QUEUE, URIPath.SMS_MARK_ID, URIPath.SMS_MARK, URIPath.MESSAGE_MARK);
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(authUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public MD5Encoder passwordEncoder() {
		MD5Encoder encoder = new MD5Encoder();
		return encoder;
	}
	
	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		return tokenRepository;
	}

}
