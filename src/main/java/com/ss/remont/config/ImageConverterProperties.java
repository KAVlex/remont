package com.ss.remont.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("converter")
public class ImageConverterProperties {

	private String imageSizeLarge = "600x500!";
	private String imageSizeMedium = "250x250!";
	private String imageSizeSmall = "50x50!";
	private String imageQuality = "90";

	private final String postfixLarge = ".l.jpg";
	private final String postfixMedium = ".m.jpg";
	private final String postfixSmall = ".s.jpg";

	public String getImageSizeLarge() {
		return imageSizeLarge;
	}

	public void setImageSizeLarge(String imageSizeLarge) {
		this.imageSizeLarge = imageSizeLarge;
	}

	public String getImageSizeMedium() {
		return imageSizeMedium;
	}

	public void setImageSizeMedium(String imageSizeMedium) {
		this.imageSizeMedium = imageSizeMedium;
	}

	public String getImageSizeSmall() {
		return imageSizeSmall;
	}

	public void setImageSizeSmall(String imageSizeSmall) {
		this.imageSizeSmall = imageSizeSmall;
	}

	public String getImageQuality() {
		return imageQuality;
	}

	public void setImageQuality(String imageQuality) {
		this.imageQuality = imageQuality;
	}

	public String getPostfixLarge() {
		return postfixLarge;
	}

	public String getPostfixMedium() {
		return postfixMedium;
	}

	public String getPostfixSmall() {
		return postfixSmall;
	}
}
