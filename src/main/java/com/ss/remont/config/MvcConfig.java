package com.ss.remont.config;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.ss.remont.controller.URIPath;
import com.ss.remont.controller.ViewPath;


@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {


		
	
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	    viewResolver.setViewClass(JstlView.class);
	    //viewResolver.setPrefix("/WEB-INF/views/");
	    viewResolver.setSuffix(".jsp");
	    registry.viewResolver(viewResolver);
	}

	
	
	@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController(URIPath.BASE).setViewName(ViewPath.HOME);
        registry.addViewController(URIPath.REGISTRATION).setViewName(ViewPath.REGISTRATION);
        registry.addViewController(URIPath.REGISTRATION_WAIT_NOTIFICATION).setViewName(ViewPath.WAIT_NOTIFICATION);
        registry.addViewController(URIPath.REGISTRATION_REGERROR).setViewName(ViewPath.REGERROR);
        registry.addViewController(URIPath.HELLO).setViewName(ViewPath.HELLO);
        registry.addViewController(URIPath.LOGIN).setViewName(ViewPath.LOGIN);
		registry.addViewController(URIPath.MESSAGES).setViewName(ViewPath.MESSAGES);
		registry.addViewController(URIPath.NEWS).setViewName(ViewPath.NEWS);
		registry.addViewController(URIPath.FILES).setViewName(ViewPath.FILES);
		registry.addViewController(URIPath.MAP).setViewName(ViewPath.MAP);
		registry.addViewController(URIPath.STATIONS_CARD).setViewName(ViewPath.STATIONS_CARD);
		registry.addViewController(URIPath.STATIONS_CARD_OPEN_STATION).setViewName(ViewPath.OPEN_STATION);
		registry.addViewController(URIPath.ADMIN_REPORT_STATION_BALANCE).setViewName(ViewPath.STATION_BALANCE);
		registry.addViewController(URIPath.ADMIN_CLIENTS_CARD).setViewName(ViewPath.CLIENTS_CARD);
		registry.addViewController(URIPath.ADMIN_SHOW_FEEDBACK).setViewName(ViewPath.ADMIN_FEEDBACK);
		registry.addViewController(URIPath.NEW_BID).setViewName(ViewPath.NEW_BID);
		registry.addViewController(URIPath.REPAIR_PASSWORD).setViewName(ViewPath.REPAIR_PASSWORD);
		
        
    }



	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		 registry.addResourceHandler("/resources/**")
	        .addResourceLocations("classpath:/static/")
	        .setCachePeriod(60 * 60 * 24 * 365) /* one year */
	        .resourceChain(true);
		 registry.addResourceHandler("/mail/**")
	        .addResourceLocations("classpath:/static/mail/")
	        .setCachePeriod(60 * 60 * 24 * 365) /* one year */
	        .resourceChain(true);
	}
	
	

	@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.indentOutput(true);
        builder.failOnEmptyBeans(false);
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
    }	

}