package com.ss.remont.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Tensky on 02.02.2017.
 */
@Component
@ConfigurationProperties("sms")
public class SmsConfig
{
    private Integer smsInPage;
    public void setSmsInPage(Integer smsInPage){this.smsInPage = smsInPage;}
    public Integer getSmsInPage(){return this.smsInPage;}
}
