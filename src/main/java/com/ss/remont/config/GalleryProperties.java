package com.ss.remont.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Tensky on 13.01.2017.
 */
@Component
@ConfigurationProperties("gallery")
public class GalleryProperties
{
    private Integer maxNumberOfImages;
    public void setMaxNumberOfImages(Integer maxNumberOfImages){this.maxNumberOfImages = maxNumberOfImages;}
    public Integer getMaxNumberOfImages(){return this.maxNumberOfImages;}
}
