package com.ss.remont.config;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.template.TemplateException;


@Configuration
public class MailConfig {
	
    @Value("${mail.host}")
    private String host;
    @Value("${mail.username}")
    private String mailUsername;
    @Value("${mail.password}")
    private String mailPassword;
    @Value("${mail.smtp.auth}")
    private String auth;
    @Value("${mail.smtp.port}")
    private int port;
    @Value("${mail.smtp.starttls.enable}")
    private String starttls;
	
    private static final String TEMPLATE_PATH = "classpath:/static/mail/";
    
    @Bean
    public JavaMailSender javaMailSender()
    {
        JavaMailSenderImpl msender=new JavaMailSenderImpl();
        Properties mailProperties=new Properties();
        mailProperties.put("mail.smtp.auth",auth);
        //mailProperties.put("mail.smtp.ssl.enable",ssl);
        //mailProperties.put("spring.mail.properties.mail.smtp.socketFactory.class",socketclass);
        if (starttls != null)
        	mailProperties.put("mail.smtp.starttls.enable", starttls);
        msender.setJavaMailProperties(mailProperties);
        msender.setHost(host);
        msender.setPort(port);
        msender.setUsername(mailUsername);
        msender.setPassword(mailPassword);
        return msender;
    }
    
    
    
    @Bean
    public FreeMarkerConfigurer getFreemarkerConfig() throws IOException, TemplateException { 
        FreeMarkerConfigurer result = new FreeMarkerConfigurer();
        result.setDefaultEncoding("UTF-8");
        result.setTemplateLoaderPaths(TEMPLATE_PATH); 
        return result;
    }
    
    
    
    


}
