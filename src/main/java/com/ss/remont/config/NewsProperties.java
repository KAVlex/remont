package com.ss.remont.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Tensky on 17.01.2017.
 */
@Component
@ConfigurationProperties("news")
public class NewsProperties
{
    private Integer numberOfNewsInPortion;
    public void setNumberOfNewsInPortion(Integer numberOfNewsInPortion){this.numberOfNewsInPortion = numberOfNewsInPortion;}
    public Integer getNumberOfNewsInPortion(){return this.numberOfNewsInPortion;}


}
