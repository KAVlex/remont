package com.ss.remont.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.Response;
import com.ss.remont.mail.template.NewMessageNoticeTemplate;
import com.ss.remont.repository.AuthUserRepository;
import com.ss.remont.repository.MessageRepository;
import com.ss.remont.repository.ResponseRepository;
import com.ss.remont.service.MailService;

@Controller
public class MessageController {

	private SimpMessagingTemplate template;
	
	private static final long NOT_ACTIVE_TIME = 86400000;

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private CurrentUser currentUser;

	@Autowired
	AuthUserRepository authUserRepository;

	@Autowired
	ResponseRepository responseRepository;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	ActiveUserService activeUserService;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	@Autowired
	public MessageController(SimpMessagingTemplate template) {
		this.template = template;
	}

	@MessageMapping(URIPath.CHAT)
	public void greeting(Message<Object> message, @Payload ChatMessage chatMessage) throws Exception {
		Principal principal = message.getHeaders().get(SimpMessageHeaderAccessor.USER_HEADER, Principal.class);
		String authedSender = principal.getName();
		chatMessage.setSender(authedSender);
		chatMessage.setRead(false);
		String recipient = chatMessage.getRecipient();
		com.ss.remont.entity.Message msg = saveMessage(chatMessage);
		chatMessage.setId(msg.getId());
		chatMessage.setTimestamp(sdf.format(msg.getTimestamp()));
		if (!authedSender.equals(recipient)) {
			template.convertAndSendToUser(authedSender, URIPath.QUEUE_MESSAGES, chatMessage);
		}
		template.convertAndSendToUser(recipient, URIPath.QUEUE_MESSAGES, chatMessage);
		AuthUser recip = msg.getRecipient();
		if (notActiveUser(recip.getEmail()) && recip.isEmailNotice() != null
				&& recip.isEmailNotice()) {
			NewMessageNoticeTemplate noticeTemplate = new NewMessageNoticeTemplate(msg);
			mailService.sendEmail(noticeTemplate);
		}
	}

	private boolean notActiveUser(String user){
		return (activeUserService.lastAccess(user) > NOT_ACTIVE_TIME); 
	}
	
	private com.ss.remont.entity.Message saveMessage(ChatMessage chatMessage) {
		com.ss.remont.entity.Message message = new com.ss.remont.entity.Message();
		message.setMessage(chatMessage.getMessage());
		message.setRead(false);
		message.setResponse(responseRepository.findOne(chatMessage.getResponse()));
		message.setRecipient(authUserRepository.findOneByPhone(chatMessage.getRecipient()));
		return messageRepository.save(message);
	}

	@RequestMapping(value = URIPath.MESSAGE_MARK, method = RequestMethod.POST)
	public @ResponseBody Boolean markMessage(@RequestParam(name = "id", required = true) Long mId){
		AuthUser authUser = currentUser.getRegistrationAuth();
		if (authUser != null){
			com.ss.remont.entity.Message message = messageRepository.findByIdAndRecipientId(mId, authUser.getId());
			if (message != null){
				message.setRead(true);
				message = messageRepository.save(message);
				ChatMessage chatMessage = messageToChatMessage(message);
				//получатель прочитал, информируем отправителя
				template.convertAndSendToUser(chatMessage.getSender(), URIPath.QUEUE_MESSAGES, chatMessage);
				return message.isRead();
			}
		}
		return null;
	}
	
	@RequestMapping(value = URIPath.RESPONSE_MESSAGES, method = RequestMethod.GET)
	public @ResponseBody ResponseMessagesDTO findResponseMessages(@RequestParam(name = "id", required = false) Integer rId,
																@RequestParam(name = "bidId", required = false) Integer bidId,
			@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable)
			throws RestException {
		try {
			AuthUser authUser = currentUser.getRegistrationAuth();
			Response response = null;
			if (rId == null){
				response = responseRepository.findByStationIdAndBidId(authUser.getId(), bidId);
				if (response != null){
					rId = response.getId();	
				}
			}else{
				response = responseRepository.findOne(rId);
			}
			ResponseMessagesDTO dto = new ResponseMessagesDTO();
			dto.setResponse(new ResponseDTO(response));
			List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
			List<com.ss.remont.entity.Message> messages = new ArrayList<com.ss.remont.entity.Message>();
			Sort sort = new Sort(Sort.Direction.ASC, "timestamp");
			Pageable page = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
			switch (authUser.getRole()) {
			case CLIENT:
				messages = messageRepository.findByResponseIdAndResponseBidClientId(rId, authUser.getId(), page);
				break;
			case STATION:
				messages = messageRepository.findByResponseIdAndResponseStationId(rId, authUser.getId(), page);
				break;
			default:
				throw new RestException("Неподдерживаемая роль");
			}
			for (com.ss.remont.entity.Message message : messages) {
				chatMessages.add(messageToChatMessage(message));
			}
			dto.setMessages(chatMessages);
			return dto;
		} catch (Exception e) {
			throw new RestException(e);
		}
	}

	private ChatMessage messageToChatMessage(com.ss.remont.entity.Message message) {
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setId(message.getId());
		chatMessage.setMessage(message.getMessage());
		chatMessage.setResponse(message.getResponse().getId());
		chatMessage.setRead(message.isRead());
		chatMessage.setBid(message.getResponse().getBid().getId());
		String recipientEmail = message.getRecipient().getPhone();		//<--нужен логин
		chatMessage.setRecipient(recipientEmail);
		String clientEmail = message.getResponse().getBid().getClient().getPhone();	//<--нужен логин
		String stationEmail = message.getResponse().getStation().getPhone();	//<--нужен логин
		chatMessage.setSender(recipientEmail.equals(stationEmail) ? clientEmail : stationEmail);
		chatMessage.setTimestamp(sdf.format(message.getTimestamp()));
		return chatMessage;
	}

}