package com.ss.remont.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.BidStatus;
import com.ss.remont.entity.Response;
import com.ss.remont.entity.Station;
import com.ss.remont.entity.StationBids;
import com.ss.remont.repository.ResponseRepository;
import com.ss.remont.repository.StationBidsRepository;
import com.ss.remont.service.NotificationService;

@Controller
public class ResponseController {

	@Autowired
	private ResponseRepository responseRepository;

	@Autowired
	private CurrentUser currentUser;
	
	@Autowired
	private StationBidsRepository stationBidsRepository;
	
	@Autowired
	private NotificationService notificationService;

//	@JsonView(ViewsJson.Response.class)
	@RequestMapping(value = URIPath.BID_RESPONSES, method = RequestMethod.GET)
	public @ResponseBody List<ResponseDTO> findBidResponses(@RequestParam("id") Integer bId, Pageable pageable)
			throws RestException {
		try {
			AuthUser authUser = currentUser.getRegistrationAuth();
			List<Response> responses = responseRepository.findByBidIdAndBidClientId(bId, authUser.getId(), pageable);
			List<ResponseDTO> dtos = new ArrayList<ResponseDTO>();
			for (Response response : responses) {
				ResponseDTO responseDTO = new ResponseDTO(response);
				dtos.add(responseDTO);
			}
			return dtos;
		} catch (Exception e) {
			throw new RestException(e);
		}
	}

	//@JsonView(ViewsJson.Response.class)
	@RequestMapping(value = URIPath.STATION_RESPONSES, method = RequestMethod.GET)
	public @ResponseBody List<ResponseDTO> findStationResponses(Pageable pageable) throws RestException {
		try {
			AuthUser authUser = currentUser.getRegistrationAuth();
			if (authUser != null) {
				List<Response> responses = responseRepository.findByStationId(authUser.getId(), pageable);
				List<ResponseDTO> dtos = new ArrayList<ResponseDTO>();
				for (Response response : responses) {
					ResponseDTO responseDTO = new ResponseDTO(response);
					dtos.add(responseDTO);
				}
				return dtos;
			}
			throw new RestException("Станция не найдена");
		} catch (Exception e) {
			throw new RestException(e);
		}
	}
	

	@RequestMapping(value = URIPath.RESPONSE_ADD, method = RequestMethod.POST)
	public String addResposne(	@RequestParam(name="cost",required=true)Double cost,
            					@RequestParam(name="comment",required=false)String comment,
            					@RequestParam(name="sBidId",required=true) Long sBidId){
		Station station = (Station)currentUser.getUser();
		StationBids stationBid = stationBidsRepository.findOne(sBidId);
		Response response = new Response();
		response.setStation(station);
		response.setCost(cost);
		response.setComment(comment);
		response.setBid(stationBid.getBid());
		responseRepository.save(response);
		stationBid.setStatus(BidStatus.REGISTERED);
		stationBidsRepository.save(stationBid);
		//TODO send email
		notificationService.addInfoMessage("Ваш ответ успешно дабавлен");
		return "redirect:/mycabinet";
	}
	
}
