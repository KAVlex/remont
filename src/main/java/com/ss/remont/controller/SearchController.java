package com.ss.remont.controller;

import com.ss.remont.entity.Discount;
import com.ss.remont.entity.News;
import com.ss.remont.entity.Station;
import com.ss.remont.repository.DiscountRepository;
import com.ss.remont.repository.NewsRepository;
import com.ss.remont.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Tensky on 27.12.2016.
 */
@Controller
public class SearchController {
	@Autowired
	StationRepository stationRepository;
	@Autowired
	NewsRepository newsRepository;
	@Autowired
	DiscountRepository discountRepository;

	// TODO: 27.12.2016 Временно, потом убрать во вьюху
	@RequestMapping(URIPath.SEARCH)
	public String searchPage() {
		return "search";
	}

	@RequestMapping(value = URIPath.SEARCH, method = RequestMethod.POST)
	public String search(@RequestParam("search_request") String request, Model model) {
		if (request.equals(new String("")) || request == null) {
			return null;
		}

		List<News> news = newsRepository.searchFor(request);
		List<Station> stations = stationRepository.searchFor(request);
		List<Discount> discounts = discountRepository.searchFor(request);

		model.addAttribute("news", news);
		model.addAttribute("stations", stations);
		model.addAttribute("discounts", discounts);
		return "search";
	}
}
