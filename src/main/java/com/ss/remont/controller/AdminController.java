package com.ss.remont.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ss.remont.entity.BalanceHistory;
import com.ss.remont.entity.CarMark;
import com.ss.remont.entity.CarModel;
import com.ss.remont.entity.CarYear;
import com.ss.remont.entity.Client;
import com.ss.remont.entity.Feedback;
import com.ss.remont.entity.Station;
import com.ss.remont.mail.template.FeedBackTemplate;
import com.ss.remont.pages.Link;
import com.ss.remont.pages.Pages;
import com.ss.remont.repository.CarMarkRepository;
import com.ss.remont.repository.CarModelRepository;
import com.ss.remont.repository.CarYearRepository;
import com.ss.remont.repository.ClientRepository;
import com.ss.remont.repository.FeedbackRepository;
import com.ss.remont.repository.StationRepository;
import com.ss.remont.service.DataService;
import com.ss.remont.service.MailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@Controller
public class AdminController {
	
	@Autowired
	StationRepository stationRepository;
	
	@Autowired
	ClientRepository clientRepository;
	
	@Autowired
    FeedbackRepository feedbackRepository;
	
	@Autowired
	@Qualifier("dataService")
	DataService dataService;  
	
	
	@Autowired
	MailService mailService;

	@Autowired
	CarMarkRepository carMarkRepository;
	
	@Autowired
	CarModelRepository carModelRepository;
	
	@Autowired
	CarYearRepository carYearRepository;
	
	@RequestMapping(URIPath.ADMIN_OPEN_CONFIGURATION)
	public String openConfiguration(Model model){
		Resource resource = new ClassPathResource("application.properties");
		try{	
			InputStream in = resource.getInputStream();
			int c = 0;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while((c=in.read())>0){
				baos.write(c);
			}
			byte buffer[] = baos.toByteArray();
			baos.close();
			in.read(buffer);
			in.close();
			model.addAttribute("config", new String(buffer,"UTF-8"));
		}catch(Exception e){
			e.printStackTrace();
		}
        return "open-config";
	}
	
	
	@RequestMapping(URIPath.ADMIN_SAVE_CONFIGURATION)
	public String saveConfiguration(@RequestParam(name="configuration",required=true) String configuration){
		URL url =  AdminController.class.getResource("application.properties");
		try{
			File file = new File(url.toURI());	
			FileOutputStream out = new FileOutputStream(file);
			out.write(configuration.getBytes("UTF-8"));
			out.close();
		}catch(Exception e){
			
		}
        return "news/";
	}
	
	
	
	@RequestMapping(URIPath.ADMIN_REPORT_STATION_BALANCE)
	public String stationBalanceReport(@RequestParam(name="idStation",required=false)Integer idStation,
			                           @RequestParam(name="fromDate",required=false)String fromDate,
                                       @RequestParam(name="toDate",required=false)String toDate,
                                       @RequestParam(name="operation",required=false)Integer operation,
                                       Model model){
		
		
		Iterable<Station> it = stationRepository.findAll();
		model.addAttribute("stations", it);
		List<BalanceHistory> list = null;
		if(operation==null)operation=0;
		switch(operation){
		case 0:{
			list = dataService.findBalanceHistoryByStationIdAndModifyDateBetween(idStation, fromDate,
					toDate);
			break;
		}
		case 1:{
			
			list = dataService.findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceGreaterThan(idStation, fromDate, toDate);
			break;
		}
		case 2:{
			
			list = dataService.findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceLessThan(idStation, fromDate, toDate);
			break;
		}
		}
		
		model.addAttribute("balances", list);
		model.addAttribute("fromDate", fromDate);
		model.addAttribute("toDate", toDate);
		model.addAttribute("balanceAction", URIPath.ADMIN_REPORT_STATION_BALANCE);
        return "station-balance";
	}
	
	
	@RequestMapping(URIPath.ADMIN_CLIENTS_CARD)
	public String editClients(@RequestParam(name="page",required=false)Integer page,
			                  Model model){
		if(page==null)page=new Integer(0);
		final PageRequest pageRequest = new PageRequest(page.intValue(), 20);
		Page<Client> listClients   = clientRepository.findAll(pageRequest);
		model.addAttribute("clients", listClients);
		
		String base = URIPath.ADMIN_SEARCH_CLIENT + "?page=" + String.valueOf(page);
        Pages pagegenerator = new Pages(base, page, listClients.getTotalPages());
        ArrayList<Link> pages = pagegenerator.generatePages();
        model.addAttribute("pages",pages);
		
		return "clients-card";
	}
	
	
	
	
	@RequestMapping(URIPath.ADMIN_SEARCH_CLIENT)
	public String searchClients(@RequestParam(name="search",required=false)String search,
			                    @RequestParam(name="page",required=false)Integer page,
			                    @RequestParam(name="locked",required=true)Integer locked,
			                    Model model){
		if(locked==null)locked = new Integer(-1);
		if(page==null)page=new Integer(0);
		final PageRequest pageRequest = new PageRequest(page.intValue(), 20);
		Page<Client> listClients = null;
		if(locked.intValue()==-1){
			listClients = clientRepository.findAllClientBySearchWordContains(pageRequest,search); 
		}else{
			boolean lock = locked.intValue()==0?false:true;
			listClients = clientRepository.findAllClientBySearchWordContainsAndLocked(pageRequest,search,lock); 
		}
		model.addAttribute("clients", listClients);
		String base = URIPath.ADMIN_SEARCH_CLIENT + "?search=" + search + 
				                                    "&locked=" + String.valueOf(locked) + 
				                                    "&page=";
		Pages pagegenerator = new Pages(base, page, listClients.getTotalPages());
		ArrayList<Link> pages = pagegenerator.generatePages();
		model.addAttribute("pages",pages);
		return "clients-card";
	}
	
	
	
	
	@RequestMapping(URIPath.ADMIN_FEEDBACK)
	public String listFeedBack(@RequestParam(name="page",required=false)Integer page,
			                   Model model){
		if(page==null)page=new Integer(0);
		final PageRequest pageRequest = new PageRequest(page.intValue(), 20);
		Page<Feedback> listFeedBack = null;
		listFeedBack = feedbackRepository.findAllFeedbackByActive(pageRequest,true);
		model.addAttribute("listFeedBack", listFeedBack);
		String base = URIPath.ADMIN_FEEDBACK + "?page=";
		Pages pagegenerator = new Pages(base, page, listFeedBack.getTotalPages());
		ArrayList<Link> pages = pagegenerator.generatePages();
		model.addAttribute("pages",pages);
		return "admin-feedback";
	}
	

	@RequestMapping(URIPath.ADMIN_SHOW_FEEDBACK)
	public String showFeedBack(@RequestParam(name="id",required=true)Integer id,
			                   Model model){
		
		Feedback feedback = feedbackRepository.findOne(id);
		model.addAttribute("feedback", feedback);
		return "show-feedback";
	}
	
	
	@PostMapping(URIPath.ADMIN_ANSWER_FEEDBACK)
	public String answerFeedBack(@RequestParam(name="id",required=true)Integer id,
			                     @RequestParam(name="answer",required=true)String answer,
			                   Model model){
		System.out.println(answer);
		Feedback feedback = feedbackRepository.findOne(id);
		FeedBackTemplate fbt = new FeedBackTemplate(feedback,answer) ;
		mailService.sendEmail(fbt);
		feedback.setActive(false);
		feedbackRepository.save(feedback);
		return listFeedBack(0,model);
	}
	
	
	@RequestMapping(URIPath.ADMIN_SEARCH_MARKS)
	public String searchMark(
			                   @RequestParam(name="search",required=false)String search,
			                   @RequestParam(name="page",required=false)Integer pageId,
			                   Model model
			                   ){
		if(pageId==null)pageId=new Integer(0);
		Page<CarMark> page = null;
		final PageRequest pageRequest = new PageRequest(pageId.intValue(), 20);
		if(search==null || search.equals("")){
		  page = carMarkRepository.findAll(pageRequest);
		}else{
		  page = carMarkRepository.findAllByNameContains(pageRequest,search);
		}
		model.addAttribute("marks", page);
		String base = URIPath.ADMIN_SEARCH_MARKS + "?page=";
		Pages pagegenerator = new Pages(base, pageId, page.getTotalPages());
		ArrayList<Link> pages = pagegenerator.generatePages();
		model.addAttribute("pages",pages);
		
		return "admin-search-mark";
	}
	
	
	@RequestMapping(URIPath.ADMIN_MENU_MARK)
	public String menuMark(@RequestParam(name="id",required=false)Integer id,
			               @RequestParam(name="action",required=false)String action,
			               Model model){
		String result = null;
		switch(action){
		case "Добавить":{
			result = "admin-edit-mark";
			model.addAttribute("markName","");
			model.addAttribute("action",action);
			break;
		}
		case "Редактировать":{
			result = "admin-edit-mark";
			CarMark carMark = carMarkRepository.findOne(id);
			model.addAttribute("markName",carMark.getName());
			model.addAttribute("action",action);
			model.addAttribute("id",carMark.getId());
			break;
		}
		case "Удалить":{
			String message = "Успешно удалено";
			carMarkRepository.delete(id);
			model.addAttribute("message", message);
			return searchMark(null,0,model);
		}
		case "Модели":{
		  CarMark carMark = carMarkRepository.findOne(id);
		  return searchModel(carMark.getName(),model);	
		}
		}
		return result;
	}
	
	@RequestMapping(URIPath.ADMIN_UPDATE_MARK)
	public String updateMark(@RequestParam(name="markName",required=true)String name,
			                 @RequestParam(name="id",required=false)Integer id,
			                 Model model){
		
		CarMark carMark = null;
		String message = null;
		
		carMark = carMarkRepository.findOneByName(name);
		message = "Такая марка уже существует";
		if(carMark!=null){
		  	model.addAttribute("message", message);
			return searchMark(name,0,model);
		}
		if(id!=null){
			carMark = carMarkRepository.findOne(id);
		}else{
			carMark = new CarMark();
		}
		carMark.setName(name);
		carMarkRepository.save(carMark);
		message = "Успешно сохранено";
		model.addAttribute("message", message);
		return searchMark(name,0,model);
		
	}
	

	@RequestMapping(URIPath.ADMIN_SEARCH_MODEL)
	public String searchModel(@RequestParam(name="markName",required=false)String name,
			                 Model model){
		List<CarModel> list = null;

		model.addAttribute("markName", name);
		if(name==null){
           model.addAttribute("message", "введите строку для поиска");			
		}else{
			CarMark carMark = carMarkRepository.findOneByName(name);
			if(carMark!=null){
				list = carMark.getModels();
				model.addAttribute("models", list);
			}
			
		}
		
		return "admin-search-model";
	}
	

	
	
	@RequestMapping(URIPath.ADMIN_MENU_MODEL)
	public String menuModel(@RequestParam(name="id",required=false)Integer id,
			               @RequestParam(name="markName",required=true)String markName,
			               @RequestParam(name="action",required=true)String action,
			               Model model){
		String result = null;
		switch(action){
		case "Добавить":{
			result = "admin-edit-model";
			model.addAttribute("modelName","");
			model.addAttribute("markName",markName);
			model.addAttribute("action",action);
			break;
		}
		case "Редактировать":{
			result = "admin-edit-model";
			CarModel carModel = carModelRepository.findOne(id);
			model.addAttribute("modelName",carModel.getName());
			model.addAttribute("markName",markName);
			model.addAttribute("year",0);
			model.addAttribute("action",action);
			model.addAttribute("id",id);
			break;
		}
		case "Удалить":{
			String message = "Успешно удалено";
			carModelRepository.delete(id);
			model.addAttribute("message", message);
			return searchModel(markName,model);
		}
		}
		return result;
	}
	
	
	@RequestMapping(URIPath.ADMIN_UPDATE_MODEL)
	public String updateModel(@RequestParam(name="modelName",required=true)String name,
			                  @RequestParam(name="id",required=false)Integer id,
			                  @RequestParam(name="year",required=false)Integer year,
			                  @RequestParam(name="markName",required=true)String markName,
			                 Model model){
		
		CarMark carMark = null;
		String message = null;
		
		carMark = carMarkRepository.findOneByName(markName);
		
		if(carMark==null){
			message = "Марки не существует";
		  	model.addAttribute("message", message);
			return searchMark("",0,model);
		}
		List<CarModel>models = carMark.getModels();
		CarModel carModel    = null,
				currentModel = null,
				hasModel     = null;
		for(int i=0;i<models.size();i++){
			currentModel = models.get(i);
			if(currentModel.getId().equals(id)){
				carModel = currentModel;
			}
			if(currentModel.getName().equals(name)){
				List<CarYear> years = currentModel.getYear();
				CarYear carYear = null;
				for(int j=0;j<years.size();j++){
				  	if(years.get(j).getYear().intValue()==year.intValue()){
				  		carYear = years.get(j); 
				  	}
				}
				if(carYear!=null) hasModel = currentModel;
			}
		}
		message = null;
		if(carModel==null){
			if(hasModel==null){
				carModel = new CarModel();
				carModel.setMark(carMark);
				carModel.setName(name);
				carModelRepository.save(carModel);
				CarYear carYear = new CarYear();
				carYear.setModel(carModel);
				carYear.setYear(year);
				carYearRepository.save(carYear);
				message = "Успешно сохранено";
			}else{
				message = "Нельзя завершить переименование: модель с таким именем уже существует";
			}
		}else{
			if(hasModel!=null){
				message = "Нельзя завершить переименование: модель с таким именем уже существует";	
			}else{
				carModel.setName(name);
				message = "Успешно сохранено";
				carModelRepository.save(carModel);
			}
		}
		
		model.addAttribute("message", message);
		return searchModel(markName,model);
		
	}
	

}
