package com.ss.remont.controller;

import com.ss.remont.balloon.Balloon;
import com.ss.remont.entity.RepairType;
import com.ss.remont.entity.Station;
import com.ss.remont.repository.RepairTypeRepository;
import com.ss.remont.repository.StationRepository;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tensky on 16.11.2016.
 */
@RestController
public class MapController {

	@Autowired
	private StationRepository stationRepository;

	@Autowired
	private RepairTypeRepository repairTypeRepository;

	/**
	 * По GET запросу <code>getAllStationBalloons</code> возвращает строку в формате
	 * <code>json</code>, которая содержит в себе все метки для карты. Данные
	 * берутся из таблицы <code>station</code>.
	 * <p>
	 * <b/>Пример запроса:
	 * </p>
	 * <p>
	 * 
	 * <pre>
	 * <code>
	 * $.ajax({
	 * url: "getAllStationBalloons",
	 * type: "POST"
	 * }).done(function (data) {objectManager.add(data);});
	 * </code>
	 * </pre>
	 *
	 * @return строка в формате json
	 */
	@RequestMapping(value = URIPath.MAP_GET_ALL_BALLOONS, method = RequestMethod.GET)
	public @ResponseBody Balloon getAllBalloons() {
		Balloon balloon = new Balloon();
		balloon.getAllBalloons(stationRepository);
		return balloon;
	}

	@RequestMapping(URIPath.MAP)
	public ModelAndView map(Model model) {
		List<RepairType> repairTypes = (List<RepairType>) repairTypeRepository.findAll();
		model.addAttribute("repairTypes", repairTypes);
		return new ModelAndView(ViewPath.MAP);
	}

	@RequestMapping(value = URIPath.MAP_SEND_FILTER, method = RequestMethod.POST)
	@ResponseBody
	public Balloon newFilter(@RequestParam(name = "repairTypes") String[] repairTypes) throws IOException, TemplateException
    {
        List<Station> stations = stationRepository.findAll();
        if(repairTypes==null)
        {
//            balloon.fillByList(stations);
//            return balloon;
            return new Balloon(stations);
        }
        else if(repairTypes.length<1)
        {
//            balloon.fillByList(stations);
//            return balloon;
            return new Balloon(stations);
        }
		int[] ids = new int[repairTypes.length];
		ArrayList<Integer> repairTypesList = new ArrayList<>();
		int i = 0;
		for (int id : ids) {
			id = Integer.parseInt(repairTypes[i++]);
			repairTypesList.add(id);
		}

		List<Station> stationsByRepairTypes = stationRepository.findStationByRepairTypes(repairTypesList,
                Long.valueOf(repairTypesList.size()));
		if (stationsByRepairTypes.size() < 1) {
			return null;
		}
//		balloon.fillByList(stationsByRepairTypes);
//		return balloon;
        return new Balloon(stationsByRepairTypes);
	}

}
