package com.ss.remont.controller;

import org.springframework.ui.Model;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.RegistrationAuth;
import com.ss.remont.entity.BalanceHistory;
import com.ss.remont.entity.Station;
import com.ss.remont.pages.Link;
import com.ss.remont.pages.Pages;
import com.ss.remont.repository.BalanceHistoryRepository;
import com.ss.remont.repository.StationRepository;
import com.ss.remont.utils.DigestUtils;

@Controller
public class StationsCard {
	
	
	@Autowired
	public StationRepository stationRepository;
	
	@Autowired
	BalanceHistoryRepository balanceHistoryRepository;
	
	
	private static final String payPassword  = "4QrcOUm6Wau+VuBX8g+IPg==";

	
	@RequestMapping(URIPath.STATIONS_CARD_GET_PAGE)
	public String showStationPage(@RequestParam(name="page",required=false) Integer page,
			                      Model model){
		String result = "stations-card";
		if(page==null){
			page = new Integer(0);
		}
		final PageRequest pageRequest = new PageRequest(page.intValue(), 20);
	    Page<Station> listStations = stationRepository.findAll(pageRequest);
	    model.addAttribute("listStations", listStations);
	    Pages pagegenerator = new Pages(URIPath.STATIONS_CARD_GET_PAGE + "?page=", page, listStations.getTotalPages());
        ArrayList<Link> pages = pagegenerator.generatePages();
	    model.addAttribute("pages", pages);
		return result;
	}
	
	
	
	@RequestMapping(value = URIPath.ADMIN_SEARCH_STATION)
	public String searchStations(@RequestParam(name="search",required=true)String search,
			                    @RequestParam(name="page",required=false)Integer page,
			                    Model model){
		if(page==null)page=new Integer(0);
		final PageRequest pageRequest = new PageRequest(page.intValue(), 20);
		Page<Station> listStations = null;
		Integer id = null;
		try{
		  id = new Integer(search);
		}catch (Exception e) {
			id=0;
		}
		System.out.println(search.length());
		listStations = stationRepository.findAllStationsByNameContainsOrId(pageRequest,search,id); 
        System.out.println("Size="  + String.valueOf(listStations.getSize()));
		model.addAttribute("listStations", listStations);
		String base = URIPath.ADMIN_SEARCH_STATION + "?search=" + search + 
				                                    "&page=";
		Pages pagegenerator = new Pages(base, page, listStations.getTotalPages());
		ArrayList<Link> pages = pagegenerator.generatePages();
		model.addAttribute("pages",pages);
		return "stations-card";
	}
	
	
	@RequestMapping(URIPath.STATIONS_CARD_OPEN_STATION)
	public String editStation(@RequestParam(name="id",required=true) Integer idStation,
			                  Model model){
		Station station = stationRepository.findOne(idStation);
	    model.addAttribute("station", station);
		return "open-station";
	}
	
	@RequestMapping(URIPath.STATIONS_CARD_CLOSE_STATION)
	public String saveStation(@RequestParam(name="idStation",required=true) Integer idStation,
			                  @RequestParam(name="changeSum",required=true)Double balance,
			                  @RequestParam(name="comment",required=false)String comment,
			                  @RequestParam(name="locked",required=false) Boolean activate,
			                  @RequestParam(name="password",required=true)String password,
			                  Model model){
		//
		Station station = stationRepository.findOne(idStation);
		if(!balance.equals(0)){
			
			String entered = DigestUtils.md5(password) ; 
			if(payPassword.equals(entered)){
				BalanceHistory balanceHistory = new BalanceHistory();
				balanceHistory.setStation(station);
				if(balance.doubleValue()>0){
					comment = "Зачисление " + comment; 
				}else{
					comment = "Списание " + comment;
				}
				balanceHistory.setComment(comment);
				balanceHistory.setBalance(balance);
				java.util.Date currentDate = new java.util.Date();
				balanceHistory.setModifyDate(currentDate);
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				final RegistrationAuth registrationAuth = (RegistrationAuth) (authentication.getPrincipal());
				AuthUser authUser = registrationAuth.getRegistrationAuth(); 
				balanceHistory.setAuthor(authUser);
				balanceHistoryRepository.save(balanceHistory);
				balance = station.getBalance() + balance;
				station.setBalance(balance);
			}
		}
		
		boolean result = activate==null?true:!activate.booleanValue();
		station.setLocked(result);
		stationRepository.save(station);
		return "redirect:" + URIPath.STATIONS_CARD_GET_PAGE;
	}
	

}
