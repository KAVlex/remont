package com.ss.remont.controller;

import java.util.List;

public class ResponseMessagesDTO {

	private ResponseDTO response;
	private List<ChatMessage> messages;
	
	public ResponseDTO getResponse() {
		return response;
	}
	public void setResponse(ResponseDTO response) {
		this.response = response;
	}
	public List<ChatMessage> getMessages() {
		return messages;
	}
	public void setMessages(List<ChatMessage> messages) {
		this.messages = messages;
	}
	
	
}
