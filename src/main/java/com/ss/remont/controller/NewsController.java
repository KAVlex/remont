package com.ss.remont.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.ss.remont.auth.CurrentUser;
import com.ss.remont.config.NewsProperties;
import com.ss.remont.entity.Images;
import com.ss.remont.entity.ImagesType;
import com.ss.remont.repository.ImagesRepository;
import com.ss.remont.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.ss.remont.entity.News;
import com.ss.remont.repository.NewsRepository;
import com.ss.remont.service.NotificationService;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class NewsController {

    @Autowired
    NewsProperties newsProperties;

	@Autowired
	private NewsRepository newsRepository;
	
	@Autowired
	private NotificationService notificationService;

    @Autowired
    StorageService storageService;

    @Autowired
    ImagesRepository imagesRepository;

    @Autowired
    CurrentUser currentUser;

	@RequestMapping(URIPath.NEWS_VIEW_ID)
	public String view(@PathVariable("id") Integer id, Model model) {
		News news = newsRepository.findOne(id);
        if (news == null){
			notificationService.addErrorMessage("Не удалось найти новость #" + id);
            return URIPath.REDIRECT+URIPath.NEWS;
		}
        List<News> last4News = newsRepository.findTop4ByOrderByIdDesc();
        News previousFrom = newsRepository.getPreviousByRelevance(news.getTimestamp());
        News nextFrom = newsRepository.getNextByRelevance(news.getTimestamp());
        model.addAttribute("previous",previousFrom);
        model.addAttribute("next",nextFrom);
        model.addAttribute("last4News",last4News);
	    model.addAttribute("news", news);
	    return URIPath.NEWS_VIEW;
	}

    @RequestMapping(value = URIPath.NEWS_NEXTPORTION,method = RequestMethod.GET)
    @ResponseBody
    public Page<News> getNextPage(@PathVariable("pageNumber") String pageNumber)
    {
        Integer pageIndex = Integer.valueOf(pageNumber);
        return newsRepository.getPage(
                new PageRequest(pageIndex,newsProperties.getNumberOfNewsInPortion(),
                        Sort.Direction.DESC,"timestamp"));
    }

	@RequestMapping(URIPath.NEWS)
	public String news(Model model) {
        Page<News> getPage = newsRepository.getPage(
                new PageRequest(0,newsProperties.getNumberOfNewsInPortion(),
                        Sort.Direction.DESC,"timestamp"));
        model.addAttribute("news", getPage);
        model.addAttribute("last",getPage.isLast());
	    return URIPath.NEWS_INDEX;
	}
	
	@RequestMapping(URIPath.NEWS_CREATE)
	public String newsCreate(@ModelAttribute News news) {
	    return "news/create";
	}


    @PostMapping(URIPath.NEWS_CREATE)
    public String create(@RequestParam Map<String,String> requestParams, @RequestParam("preview_image")MultipartFile multipartFile) throws IOException
    {
        Images preview_image;
        String title = requestParams.get("title");
        String content = requestParams.get("content");
        if(content.length()>2048)
        {
            notificationService.addErrorMessage("Содержание не может превышать 2048 символов");
            return URIPath.NEWS_CREATE;
        }

        if(multipartFile!=null)
        {
            preview_image = imagesRepository.save(storageService.store(currentUser.getUser(), multipartFile, ImagesType.NEWS));
        }else{preview_image=null;}

        if(title==null||content==null)
        {
            notificationService.addErrorMessage("Заполните заголовок и содержание новости");
            return URIPath.NEWS_CREATE;
        }
        News news = newsRepository.save(new News(title, content, preview_image));
        return URIPath.REDIRECT+URIPath.NEWS_VIEW+URIPath.PATH_SEPARATOR+news.getId();
    }
//	@PostMapping(URIPath.NEWS_CREATE)
//	public String create(@Valid News news, BindingResult result, Model model, @Param("preview_image")MultipartFile multipartFile) throws IOException
//    {
//		if (result.hasErrors()) {
//			notificationService.addErrorMessage(result.getAllErrors().get(0).getDefaultMessage());
//			return URIPath.NEWS_CREATE;
//		}
//
//        news = newsRepository.save(news);
//		notificationService.addInfoMessage("Новость успешно добавлена!");
//		model.addAttribute("news", news);
//	    return URIPath.NEWS;
//	}
}
