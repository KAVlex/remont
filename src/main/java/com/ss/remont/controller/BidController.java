package com.ss.remont.controller;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.CurrentUser;
import com.ss.remont.auth.EmailRegistrationForm;
import com.ss.remont.auth.RegistrationAuth;
import com.ss.remont.auth.Role;
import com.ss.remont.entity.*;
import com.ss.remont.repository.*;
import com.ss.remont.service.NotificationService;
import com.ss.remont.service.SaveUser;
import com.ss.remont.storage.StorageService;
import com.ss.remont.utils.CheckBoxParser;
import com.ss.remont.utils.PasswordUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Created by Tensky on 28.11.2016.
 */
@Controller
public class BidController
{

    @Autowired
    private CarModelRepository carModelRepository;

    @Autowired
    private CarMarkRepository carMarkRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private BidRepository bidRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarYearRepository carYearRepository;

    @Autowired
    private RepairTypeRepository repairTypeRepository;

    @Autowired
    private CurrentUser currentUser;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private StationBidsRepository stationBidsRepository;

    @Autowired
    private SaveUser saveUser;

    @Autowired
    StorageService storageService;

    @Autowired
    ImagesRepository imagesRepository;

    @Autowired
    StationCategoryRepository stationCategoryRepository;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(URIPath.NEW_BID_STATION)
    public String createBidForASingleStation(@PathVariable(value = "station") Integer station_id, Model model)
    {
        Station station = stationRepository.findOne(station_id);
        if (!station.getClientCommunication())
            return URIPath.REDIRECT + URIPath.BASE;
        if (station != null)
        {
            model.addAttribute("stationID", station_id);
            model.addAttribute("station", station);
            List<StationCategoryEnum> stationCategories = new ArrayList<>();
            station.getStationCategories().forEach(stationCategory -> stationCategories.add(stationCategory.getStationCategory()));
            model.addAttribute("stationCategories", stationCategories);
            model.addAttribute("cities", station.getCity());
        }
        addAttributesToBidModel(model);
        return "new-bid";
    }

    @RequestMapping(value = URIPath.NEW_BID_GET_CAR_MARKS, method = RequestMethod.GET)
    @ResponseBody
    public List<CarMark> getCarMarks()
    {
        List<CarMark> carMarks = (List<CarMark>) carMarkRepository.findAll();
//        System.out.println("[getCarMarks]: returning carMarks list");
        return carMarks;
    }

    @RequestMapping(value = URIPath.NEW_BID_GET_CITIES, method = RequestMethod.GET)
    @ResponseBody
    public List<City> getCities()
    {
        List<City> cities = (List<City>) cityRepository.findAll();
//        System.out.println("[getCities]: returning cities list");
        return cities;
    }

    @RequestMapping(value = URIPath.NEW_BID_GET_CAR_MODELS_BY_MARK, method = RequestMethod.GET)
    @ResponseBody
    public List<CarModel> getCarModels(@RequestParam(name = "carMark") String stringCarMark)
    {
        Integer carMarkId = Integer.parseInt(stringCarMark);
        List<CarModel> carModels = carModelRepository.findCarModelByMarkId(carMarkId);
        return carModels;
    }

    @RequestMapping(value = "/new-bid/test", method = RequestMethod.POST)
    public String createBid(@RequestParam(name = "carMark") CarMark carMark,
                            @RequestParam(name = "carModel") CarModel carModel,
                            @RequestParam(name = "need-evacuator", required = false) String needEvacuatorCB,
                            @RequestParam(name = "own-parts", required = false) String ownPartsCB,
                            @RequestParam(name = "desired-datetime") String desiredDateTime,
                            @RequestParam(name = "comment") String comment, @RequestParam(name = "carYear") Integer year,
                            @RequestParam(name = "color") String color, @RequestParam(name = "regNumber") String regNumber,
                            @RequestParam(name = "station", required = false) String strStationId,
                            @RequestParam(name = "telephone", required = true) String telephoneNumber) throws ParseException
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        RegistrationAuth registrationAuth = (RegistrationAuth) authentication.getDetails();
        boolean needEvacuator = isCheckbox(needEvacuatorCB);
        boolean ownParts = isCheckbox(ownPartsCB);
        String username = registrationAuth.getRegistrationAuth().getName();
        System.out.println("[createBid]: Current user: " + username);
        System.out.println("[createBid]: carMark: " + carMark.getName());
        System.out.println("[createBid]: carModel: " + carModel.getName());
        System.out.println("[createBid]: needEvacuator: " + needEvacuator);
        System.out.println("[createBid]: ownParts: " + ownParts);
        System.out.println("[createBid]: desired-time: " + desiredDateTime);
        System.out.println("[createBid]: color: " + color);
        System.out.println("[createBid]: year: " + year);
        System.out.println("[createBid]: regNumber: " + regNumber);
        System.out.println("[createBid]: telephone number: " + telephoneNumber);
        System.out.println("[createBid]: comment: " + comment);
        System.out.println("[createBid]: station ID: " + strStationId);
        return "new-bid";
    }

    @RequestMapping(value = URIPath.NEW_BID, method = RequestMethod.GET)
    public String newBid(Model model)
    {
        model.addAttribute("stationCategories", StationCategoryEnum.values());
        model.addAttribute("cities", cityRepository.findAll());
        addAttributesToBidModel(model);
        return "new-bid";
    }

    private void addAttributesToBidModel(Model model)
    {
        List<CarMark> carMarks = (List<CarMark>) carMarkRepository.findAll();
        ArrayList<Integer> years = new ArrayList<>();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1950; i < currentYear; i++)
        {
            years.add(i);
        }
        List<RepairType> repairTypes = (List<RepairType>) repairTypeRepository.findAll();
        model.addAttribute("carMarks", carMarks);
        model.addAttribute("years", years);
        model.addAttribute("repairTypes", repairTypes);
        if (currentUser.isAuthenticated() && currentUser.isClient())
        {
            Client client = (Client) currentUser.getUser();
            if (client.getCars() != null && client.getCars().size() > 0)
            {
                model.addAttribute("cars", client.getCars());
            }
        }
    }

    @RequestMapping(value = URIPath.NEW_BID_CREATE, method = RequestMethod.POST)
    public String createNewBid(@RequestParam Map<String, String> params,
                               @RequestParam(required = false) List<Integer> repairTypesIds, @RequestParam("image") List<MultipartFile> multipartFiles)
    {
        Client client = currentUser.isAuthenticated() ? (Client) currentUser.getUser() : registerClient(params);
        if (client != null)
        {
            Car car = null;
            Station station = null;
            Date date = null;
            try
            {
                car = findOrAddCar(client, params);
                station = findStation(params);
                date = parseDate(params);
            } catch (Exception e)
            {
                e.printStackTrace();
                notificationService.addErrorMessage("Неверные параметры запроса - " + e.getMessage());
                return "redirect:" + URIPath.BASE;
            }
            StationCategoryEnum stationCategory = StationCategoryEnum.findByString(params.get("stationCategory"));
            String comment = params.get("type_work");
            String location = params.get("location");
            Boolean needEvacuator = CheckBoxParser.parseCheckBoxValue(params.getOrDefault("evacuator", null));
            Boolean needShipment = CheckBoxParser.parseCheckBoxValue(params.getOrDefault("need-shipment", null));
            Boolean ownParts = CheckBoxParser.parseCheckBoxValue(params.getOrDefault("own_parts", null));
            List<RepairType> repairTypes = repairTypeRepository.listOfRepairTypesByIds(repairTypesIds);
            boolean quickly = CheckBoxParser.parseCheckBoxValue(params.get("quickly"));
            Bid bid = new Bid();
            bid.setClient(client);
            bid.setStation(station);
            bid.setStatus(BidStatus.WAITING_RESPONSE);
            bid.setCar(car);
            bid.setRepairTypes(repairTypes);
            bid.setQuickly(quickly);
            bid.setDesiredDate(date);
            bid.setCity(cityRepository.findByName(location));
            bid.setContent(comment);
            switch (stationCategory)
            {
                case AUTO_SERVICE:
                {
                    bid.setNeedEvacuator(needEvacuator);
                    bid.setOwnParts(ownParts);
                    bid.setNeedShipment(false);
                    break;
                }
                case AUTO_SHOP:
                {
                    bid.setNeedEvacuator(false);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(needShipment);
                    break;
                }
                case AUTOEXAMINATION:
                {
                    bid.setNeedEvacuator(needEvacuator);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(false);
                    break;
                }
                case BODY_REPAIR:
                {
                    bid.setNeedEvacuator(needEvacuator);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(false);
                    if (multipartFiles != null)
                    {
                        try
                        {
                            List<Images> images = new ArrayList<>();
                            if (!multipartFiles.isEmpty())
                            {
                                images = storageService.store(client, multipartFiles, ImagesType.BID);
                                images = (List<Images>) imagesRepository.save(images);
                                if (images == null)
                                    throw new IOException();
                            }
                            bid.setPhotos(images);
                        } catch (IOException e)
                        {
                            notificationService.addErrorMessage("Ошибка при сохранении изображений");
                            return URIPath.REDIRECT + URIPath.BASE;
                        }
                    }
                    break;
                }
                case CAR_WASH:
                {
                    bid.setNeedEvacuator(needEvacuator);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(false);
                    break;
                }
                case EVACUATOR:
                {
                    bid.setNeedEvacuator(true);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(false);
                    break;
                }
                case MOUNTING:
                {
                    bid.setNeedEvacuator(false);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(needShipment);
                    break;
                }
                case TUNING_ATELIER:
                {
                    bid.setNeedEvacuator(needEvacuator);
                    bid.setOwnParts(false);
                    bid.setNeedShipment(false);
                    break;
                }
            }
//            boolean evacuator = CheckBoxParser.parseCheckBoxValue(params.get("evacuator"));
//            boolean own_parts = CheckBoxParser.parseCheckBoxValue(params.get("own_parts"));
//            Bid bid = new Bid(client, station, BidStatus.WAITING_RESPONSE, car, repairTypes, evacuator, own_parts, date,
//                    comment, quickly, location);
            bid = bidRepository.save(bid);
            sendBidToStationByCategory(bid, location, station, repairTypesIds, stationCategory);
//            sendBidToStation(bid, station, repairTypesIds);
        }

        return "redirect:" + URIPath.BASE;
    }

    private Client registerClient(Map<String, String> params)
    {
        EmailRegistrationForm rf = new EmailRegistrationForm();
        rf.setEmail(params.get("email"));
        rf.setName(params.get("name"));
        rf.setPhone(params.get("phone"));
        String pass = PasswordUtil.generatePassword();
        rf.setPassword(pass);
        rf.setRepass(pass);
        rf.setRole(Role.CLIENT.name());
        if (saveUser.saveUser(rf, null))
        {
            notificationService.addInfoMessage("Инструкция по завершению регистрации направлена на вашу почту");
            AuthUser au = saveUser.authUserRepository.findOneByEmail(rf.getEmail());
            return clientRepository.findOne(au.getId());
        }
        return null;
    }

    private void sendBidToStationByCategory(Bid bid, String location, Station station, List<Integer> repairTypesIds, StationCategoryEnum stationCategoryEnum)
    {
        if (stationCategoryEnum == null)
        {
            sendBidToStation(bid, station, repairTypesIds);
            return;
        }
        if (station == null)
        {
            List<Station> allStationsByCityName;
            if (location.isEmpty())
            {
                allStationsByCityName = stationRepository.findAll();
            }
            else
            {
                allStationsByCityName = stationRepository.findAllStationsByCityName(location);
                if(allStationsByCityName.isEmpty())
                {
                    notificationService.addErrorMessage("Нет зарегистрированных станций для выбранного города");
                    return;
                }
            }
            boolean send = false;
            for (Station s : allStationsByCityName)
            {
                List<StationCategory> thisStationCategories = s.getStationCategories();
                for (StationCategory sc : thisStationCategories)
                {
                    if (sc.getStationCategory().equals(stationCategoryEnum))
                    {
                        send = true;
                        sendBidToStation(bid, s, repairTypesIds);
                    }
                }
            }
            if(!send)
            {
                notificationService.addErrorMessage("В выбранном городе нет станций следующей категории: "+stationCategoryEnum.toString());
            }else{
                notificationService.addInfoMessage("Заявка успешно отправлена");
            }
        }
    }

    private void sendBidToStation(Bid bid, Station station, List<Integer> repairTypesIds)
    {
        if (station == null)
        {
            List<Station> stations = new ArrayList<Station>();
            if (repairTypesIds != null && repairTypesIds.size() > 0)
            {
                stations = stationRepository.findStationByRepairTypes(repairTypesIds, Long.valueOf(repairTypesIds.size()));
            }
            else
            {
                stations = stationRepository.findAll();
            }
            for (Station sto : stations)
            {
                StationBids stationBid = new StationBids(bid, sto, BidStatus.WAITING_RESPONSE);
                stationBidsRepository.save(stationBid);
            }
            notificationService.addInfoMessage("Заявка успешно отправлена во все станции");
        }
        else
        {
            StationBids stationBid = new StationBids(bid, station, BidStatus.WAITING_RESPONSE);
            stationBidsRepository.save(stationBid);
            notificationService.addInfoMessage("Заявка успешно отправлена в " + station.getName());
        }
    }

    private Date parseDate(Map<String, String> params) throws ParseException
    {
        String desired_date = params.get("desired_date");
        if (desired_date != null && !desired_date.isEmpty())
            return sdf.parse(params.get("desired_date"));
        return null;
    }

    private Station findStation(Map<String, String> params)
    {
        String stationId = params.get("station");
        if (stationId != null)
        {
            return stationRepository.findOne(Integer.valueOf(stationId));
        }
        return null;
    }

    private Car findOrAddCar(Client client, Map<String, String> params)
    {
        Car car = null;
        if (params.get("car") != null)
        {
            car = carRepository.findOne(Integer.valueOf(params.get("car")));
        }
        else
        {
            Integer year = Integer.valueOf(params.get("year"));
            CarMark carMark = carMarkRepository.findOne(Integer.valueOf(params.get("mark")));
            CarModel carModel = carModelRepository.findOne(Integer.valueOf(params.get("model")));
            CarYear carYear = carYearRepository.findByModelAndYear(carModel.getId(), year);
            if (carYear == null)
            {
                carYear = new CarYear(year, carModel);
                carYear = carYearRepository.save(carYear);
            }
            car = carRepository.findByClientAndMarkAndModelAndYear(client, carMark, carModel, carYear);
            if (car == null)
            {
                car = new Car(carMark, carModel, carYear, client);
                car = carRepository.save(car);
            }
        }
        return car;
    }

    private boolean isCheckbox(String checkbox)
    {
        return checkbox != null ?
                (checkbox.toLowerCase().equals("on") ? true : false)
                : false;
    }

}
