package com.ss.remont.controller;

import com.ss.remont.captcha.CaptchaService;
import com.ss.remont.captcha.ReCaptchaInvalidException;
import com.ss.remont.entity.Feedback;
import com.ss.remont.repository.FeedbackRepository;
import com.ss.remont.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Tensky on 16.12.2016.
 */
@Controller
public class FeedbackController
{
    @Autowired
    FeedbackRepository feedbackRepository;
    @Autowired
    CaptchaService captchaService;
    @Autowired
    private NotificationService notificationService;

    @RequestMapping(value = URIPath.FEEDBACK, method = RequestMethod.GET)
    public String getPage()
    {

        return "feedback";
    }

    @RequestMapping(URIPath.FEEDBACK_ERR)
    public String err()
    {
        return "feedbackerror";
    }

    @RequestMapping(value = URIPath.FEEDBACK, method = RequestMethod.POST)
    public String Submit(@RequestParam Map<String,String> requestParams)
    {
        String recaptcha = requestParams.get("g-recaptcha-response");
        try {
            captchaService.processResponse(recaptcha);
        } catch (ReCaptchaInvalidException e) {
            notificationService.addErrorMessage("Невозможно подтвердить капчу. " + e.getMessage());
            return "redirect:/feedback";
        }
        String name = requestParams.get("name");
        String email = requestParams.get("email");
        String comment = requestParams.get("comment");
        String phone = requestParams.get("phone");

        Feedback feedback = new Feedback(name, email, comment, phone);
        feedback.setActive(true);
        feedbackRepository.save(feedback);
        notificationService.addInfoMessage("Ваше обращение успешно добавлено!");
        return "redirect:/feedback";
    }


}
