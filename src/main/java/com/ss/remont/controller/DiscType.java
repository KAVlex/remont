package com.ss.remont.controller;
/**
 * Created by Tensky on 27.03.2017.
 */
public enum DiscType
{
    FORGED, STAMPED, CASTED;

    @Override
    public String toString()
    {
        switch (this)
        {
            case CASTED:
                return "Литой";
            case FORGED:
                return "Кованый";
            case STAMPED:
                return "Штампованный";
            default:
                throw new IllegalArgumentException();
        }
    }

    public static DiscType findByString(String type)
    {
        for (DiscType discType : values())
        {
            if (type.equals(discType.toString()))
                return discType;
        }
        throw new EnumConstantNotPresentException(DiscType.class, type);
    }
}
