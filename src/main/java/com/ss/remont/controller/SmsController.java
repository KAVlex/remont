package com.ss.remont.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ss.remont.config.SmsConfig;
import com.ss.remont.service.NotificationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ss.remont.entity.Sms;
import com.ss.remont.entity.SmsStatus;
import com.ss.remont.repository.SmsRepository;
import com.ss.remont.sms.HashGenerator;
import com.ss.remont.sms.SmsDTO;
import com.ss.remont.sms.SmsPackage;

@Controller
public class SmsController
{

    static Logger log = Logger.getLogger(SmsController.class.getName());

    @Autowired
    private SmsRepository smsRepository;

    @Autowired
    SmsConfig smsConfig;

    @Autowired
    NotificationService notificationService;

    /**
     * Запрос списка заявок, находящихся в очереди
     *
     * @param key
     * @param hash
     * @return
     * @throws RestException
     */
    @RequestMapping(value = URIPath.SMS_QUEUE, method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    Map<String, Object> getSmsInQueue(@RequestParam(value = "key", required = true) String key,
                                      @RequestParam(value = "hash", required = true) String hash,
                                      @RequestParam(value = "limit", required = false) String limit)
            throws RestException
    {
        log.info(String.format("QUEUE - key = %s, hash = %s, limit = %s", key, hash, limit));
        try
        {
            if (HashGenerator.checkRequestHash(key, hash))
            {
                Integer size = (limit != null) ? Integer.valueOf(limit) : Integer.MAX_VALUE;
                PageRequest page =
                        new PageRequest(0, size, Sort.Direction.ASC, "id");
                Date before = new Date(Long.valueOf(key));
                List<Sms> smsList = smsRepository.findByStatusAndTimestampBefore(page, SmsStatus.QUEUE, before);
                if (smsList == null || smsList.size() == 0)
                {
                    return Ajax.emptyResponse();
                }
                SmsPackage responsePackage = buildResponsePackage(smsList);
                smsRepository.updateStatus(SmsStatus.PROCESS, smsList);
                log.info("successResponse" + responsePackage);
                return Ajax.successResponse(responsePackage);
            }
            log.error("errorResponse");
            return Ajax.errorResponse("Unauthorized request");
        } catch (Exception e)
        {
            throw new RestException(e);
        }
    }

    @RequestMapping(value = "/admin/sms/add", method = RequestMethod.POST)
    public String addSms(@RequestParam Map<String, String> requestParams) throws ParseException
    {
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String message = requestParams.getOrDefault("message", "");
        String phone = requestParams.getOrDefault("phone", "");
//        String date = requestParams.getOrDefault("date", "");
//        String time = requestParams.getOrDefault("time", "");
        String status = requestParams.getOrDefault("status", "");
//        String timestamp = "";
        Date timestamp = new Date();
//        if (!time.isEmpty())
//        {
//            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//            timestamp = date + " " + time;
//        }
//        else
//        {
//            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            timestamp = date;
//        }
//        if (message.isEmpty() || phone.isEmpty() || date.isEmpty() || status.isEmpty())
        if (message.isEmpty() || phone.isEmpty() ||  status.isEmpty())
        {
            notificationService.addErrorMessage("Вы должны заполнить все поля, прежде чем добавлять запись в базу данных");
            return URIPath.REDIRECT + URIPath.ADMIN + "/sms";
        }
        Sms save = smsRepository.save(new Sms(phone, message, SmsStatus.findByString(status)));
        if (save != null)
            notificationService.addInfoMessage("Запись сохранена");
        else notificationService.addErrorMessage("Не удалось сохранить запись");
        return URIPath.REDIRECT + URIPath.ADMIN + "/sms";
    }

    @RequestMapping(value = "/admin/sms/{page}/filter", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity smsPage(@PathVariable("page") Integer page, @RequestParam Map<String, String> requestParams) throws ParseException
    {
        String phone = requestParams.getOrDefault("phone", null);
        if (phone != null)
            if (phone.isEmpty())
                phone = null;
        String sStatus = requestParams.getOrDefault("status", null);
        SmsStatus status;
        if (sStatus == null)
            status = null;
        else if (sStatus.isEmpty())
            status = null;
        else status = SmsStatus.findByString(sStatus);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String sds = requestParams.getOrDefault("date_start", "");
        String sde = requestParams.getOrDefault("date_end", "");
        Date date_start = new Date(0);
        Date date_end = new Date();
        if (!sds.isEmpty())
            date_start = simpleDateFormat.parse(sds);
        if (!sde.isEmpty())
            date_end = simpleDateFormat.parse(sde);


        Page<Sms> filter = smsRepository.filter(phone, status, date_start, date_end, new PageRequest(page, smsConfig.getSmsInPage()));
        return ResponseEntity.status(HttpStatus.OK).body(filter);
//        return smsRepository.getPage(new PageRequest(page, 5));
    }

    @RequestMapping(value = "/admin/sms/changeStatus", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity changeSmsStatus(@RequestParam("status") String sStatus, @RequestParam("ids") Long[] ids)
    {
        if (sStatus.isEmpty())
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("status is empty");
        SmsStatus status = SmsStatus.findByString(sStatus);

        List<Sms> smsList = smsRepository.findByIds(ids);
        smsList.forEach(sms -> {sms.setStatus(status);});
        smsList = (List<Sms>) smsRepository.save(smsList);
        return ResponseEntity.status(HttpStatus.OK).body(smsList);
    }

    @RequestMapping(value = "/admin/sms", method = RequestMethod.GET)
    public String smsPage(Model model)
    {
        model.addAttribute("Status", SmsStatus.class);
        model.addAttribute("phones", smsRepository.findAllPhones());
        model.addAttribute("listSms", smsRepository.getPage(new PageRequest(0, smsConfig.getSmsInPage())));
        return "admin-sms";
    }

    @RequestMapping(value = URIPath.SMS_MARK, method = RequestMethod.POST, consumes = {"application/json"})
    public
    @ResponseBody
    Map<String, Object> markSms(@RequestBody(required = true) SmsPackage smsPackage) throws RestException
    {
        log.info(smsPackage);
        try
        {
            if (HashGenerator.checkRequestHash(smsPackage.getKey(), smsPackage.getHash()))
            {
                List<SmsDTO> smsDTOs = smsPackage.getSmsDTOs();
                List<Sms> sucessList = new ArrayList<Sms>();
                List<Sms> errorList = new ArrayList<Sms>();
                for (SmsDTO smsDTO : smsDTOs)
                {
                    Sms sms = smsRepository.findByIdAndStatus(smsDTO.getId(), SmsStatus.PROCESS);
                    if (sms != null)
                    {
                        if (smsDTO.getStatus() == null || smsDTO.getStatus().equals(SmsStatus.SUCCESS))
                        {
                            sucessList.add(sms);
                        }
                        else if (smsDTO.getStatus().equals(SmsStatus.ERROR))
                        {
                            errorList.add(sms);
                        }
                    }
                }
                if (sucessList.size() > 0)
                    smsRepository.updateStatus(SmsStatus.SUCCESS, sucessList);
                if (errorList.size() > 0)
                    smsRepository.updateStatus(SmsStatus.ERROR, errorList);
                return Ajax.successResponse(buildResponsePackage());
            }
            return Ajax.errorResponse("Unauthorized request");
        } catch (Exception e)
        {
            throw new RestException(e);
        }
    }

    /**
     * Изменить статус заявки, находящейся в обработке
     *
     * @param key    - ключ(текущее время в миллисекундах)
     * @param hash   - хеш доступа
     * @param status - статус, который необходимо присвоить (SUCCESS или ERROR)
     * @param smsId  - id sms
     * @return
     * @throws RestException
     */
    @RequestMapping(value = URIPath.SMS_MARK_ID, method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, Object> markSmsId(@RequestParam(value = "key", required = true) String key,
                                  @RequestParam(value = "hash", required = true) String hash,
                                  @RequestParam(value = "status", required = false) String status,
                                  @PathVariable("smsId") String smsId) throws RestException
    {
        log.info(String.format("MARK - key = %s, hash = %s, status = %s, smsId = %s", key, hash, status, smsId));
        try
        {
            if (HashGenerator.checkRequestHash(key, hash))
            {
                Long id = Long.valueOf(smsId);

                Sms sms = smsRepository.findByIdAndStatus(id, SmsStatus.PROCESS);
                if (sms != null)
                {
                    if (status == null || status.equals(SmsStatus.SUCCESS.name()))
                    {
                        sms.setStatus(SmsStatus.SUCCESS);
                    }
                    else if (status.equals(SmsStatus.ERROR.name()))
                    {
                        sms.setStatus(SmsStatus.ERROR);
                    }
                    smsRepository.save(sms);
                }
                log.info("successResponse");
                return Ajax.successResponse(buildResponsePackage());
            }
            log.error("errorResponse");
            return Ajax.errorResponse("Unauthorized request");
        } catch (Exception e)
        {
            throw new RestException(e);
        }
    }

    private SmsPackage buildResponsePackage()
    {
        SmsPackage responsePackage = new SmsPackage();
        String responseKey = String.valueOf(Calendar.getInstance().getTimeInMillis());
        responsePackage.setKey(responseKey);
        responsePackage.setHash(HashGenerator.generateResponseHash(responseKey));
        return responsePackage;
    }

    private SmsPackage buildResponsePackage(List<Sms> smsList)
    {
        SmsPackage responsePackage = buildResponsePackage();
        List<SmsDTO> smsDTOs = new ArrayList<SmsDTO>();
        for (Sms sms : smsList)
        {
            SmsDTO smsDTO = new SmsDTO(sms);
            smsDTOs.add(smsDTO);
        }
        responsePackage.setSmsDTOs(smsDTOs);
        return responsePackage;
    }
}
