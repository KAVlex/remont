package com.ss.remont.controller;

public class ViewPath {

	public static final String HOME = "home";
	public static final String REGISTRATION = "registration";
	public static final String WAIT_NOTIFICATION = "waitNotification";
	public static final String REGERROR = "regerror";
	public static final String HELLO = "hello";
	public static final String LOGIN = "login";
	public static final String MESSAGES = "messages";
	public static final String NEWS = "news";
	public static final String FILES = "files";
	public static final String MAP = "map";
	public static final String STATIONS_CARD = "stations-card";
	public static final String OPEN_STATION = "open-station";
	public static final String STATION_BALANCE = "station-balance";
	public static final String CLIENTS_CARD = "clients-card";
	public static final String ADMIN_FEEDBACK = "admin-feedback";
	public static final String NEW_BID = "new-bid";
	public static final String REPAIR_PASSWORD = "repair-password";
	public static final String NEW_PASSWORD = "new-password";

}
