package com.ss.remont.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss.remont.entity.Discount;
import com.ss.remont.entity.Station;
import com.ss.remont.repository.DiscountRepository;
import com.ss.remont.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

/**
 * Created by Tensky on 23.12.2016.
 */
@Controller
public class DiscountController
{
    @Autowired
    StationRepository stationRepository;

    @Autowired
    DiscountRepository discountRepository;


    @RequestMapping(URIPath.DISCOUNTS_STATION_ID)
    @ResponseBody
    public String getListOfStationDiscounts(@PathVariable("stationId") String sStationId) throws JsonProcessingException
    {
        Integer stationId;
        List<Discount> discounts;
        ObjectMapper objectMapper = new ObjectMapper();
        if (!sStationId.equals("") && sStationId != null && !sStationId.equals("-1"))
        {
            stationId = Integer.valueOf(sStationId);
            discounts = discountRepository.getThisStationActiveDiscounts(stationId);
        }
        else
        {
//            discounts = discountRepository.getDiscountsByStatus(DiscountStatus.ACTIVE);
            discounts=discountRepository.getActiveDiscounts();
        }
        return objectMapper.writeValueAsString(discounts);
    }

    @RequestMapping(URIPath.DISCOUNTS)
    public String discounts(Model model)
    {
        List<Station> stations = (List<Station>) stationRepository.findAll();
        List<Discount> discounts = (List<Discount>) discountRepository.findAll();
        model.addAttribute("stations", stations);
        model.addAttribute("discounts", discounts);
        return "discounts";
    }

    @RequestMapping(value = URIPath.DISCOUNTS+"/{discountId}",method = RequestMethod.GET)
    public String showDiscountPage(@PathVariable("discountId") String discountId, Model model)
    {
        Long id = Long.valueOf(discountId);
        Discount discount = discountRepository.findOne(id);
        if(discount==null){
            return "redirect:"+URIPath.DISCOUNTS;
        }
        model.addAttribute("discount",discount);
        return "discount-page";
    }



}
