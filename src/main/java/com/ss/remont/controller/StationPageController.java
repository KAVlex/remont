package com.ss.remont.controller;

import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.*;
import com.ss.remont.repository.*;
import com.ss.remont.service.NotificationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tensky on 15.12.2016.
 */
@Controller()
@RequestMapping(value = URIPath.STATION, method = RequestMethod.GET)
public class StationPageController
{
    @Autowired
    private StationRepository stationRepository;
    
    @Autowired
    private OrderCallRepository orderCallRepository;
    
    @Autowired
    private CarMarkRepository carMarkRepository;
    
    @Autowired
    private CarModelRepository carModelRepository;
    
    @Autowired
    private CityRepository cityRepository;
    
    @Autowired
    private DiscountRepository discountRepository;
    
    @Autowired
	private NotificationService notificationService;

    @Autowired
    RepairTypePriceRepository repairTypePriceRepository;

    @Autowired OrderEmailRepository orderEmailRepository;

    @Autowired
    CurrentUser currentUser;

	@RequestMapping(value = URIPath.STATION_ID)
	public String showStationPage(@PathVariable(value = "stationID", required = true) String stringID, Model model) {
		Integer stationID = Integer.parseInt(stringID);
		Station station = stationRepository.findOne(stationID);
		if (station == null) {
			return "redirect:/";
		}
		addStationAttributesToModel(station, model);
		return "station";
	}
    
    private void addStationAttributesToModel(Station station, Model model){
        model.addAttribute("station", station);
        List<CarMark> carMarks = (List<CarMark>) carMarkRepository.findAll();
        List<City> cities = (List<City>) cityRepository.findAll();
        List<RepairType> repairTypes = stationRepository.findRepairTypesByStation(station);
        List<CarModel> carModels = carModelRepository.findCarModelByMarkId(carMarks.get(0).getId());
//        List<Discount> discounts = discountRepository.getThisStationDiscounts(station, DiscountStatus.ACTIVE);
        List<Discount> discounts = discountRepository.getThisStationActiveDiscounts(station);
        List<RepairTypePrice> repairTypePrices = repairTypePriceRepository.getStationPriceList(station);
        List<Recall> recalls = station.getRecalls();
        model.addAttribute("carMark",carMarks);
        model.addAttribute("carModels",carModels);
        model.addAttribute("cities",cities);
        model.addAttribute("repairTypes",repairTypes);
        model.addAttribute("repairTypePrices",repairTypePrices);
        model.addAttribute("discounts",discounts);
        model.addAttribute("recalls",recalls);
        model.addAttribute("authUser",currentUser.getUser());
    }

    @RequestMapping(value = URIPath.STATION_ORDER_CALL, method = RequestMethod.POST)
    @ResponseBody
    public String orderCall(@RequestParam Map<String, String> requestParams)
    {
        String name = requestParams.get("name");
        String phone = requestParams.get("phone");
        Integer stationID = Integer.parseInt(requestParams.get("stationID"));

        Station station = stationRepository.findOne(stationID);
        if (station == null)
        {
            return "Невозможно найти станцию с номером " + stationID;
        }
        OrderCall orderCall = new OrderCall(name, phone, station);
        orderCallRepository.save(orderCall);
        return "Заявка на имя \"" + name + "\" успешно добавлена.";
    }

    
	@RequestMapping(value = URIPath.STATION_ADD_ORDER_CALL, method = RequestMethod.POST)
	public String addOrderCall(@RequestParam String name, @RequestParam String phone, @RequestParam Integer stationId) {
		Station station = stationRepository.findOne(stationId);
		if (station == null) {
			notificationService.addErrorMessage("Невозможно найти станцию с номером " + stationId);
		} else {
			OrderCall orderCall = new OrderCall(name, phone, station);
			orderCallRepository.save(orderCall);
			notificationService.addInfoMessage("Заявка на имя \"" + name + "\" успешно добавлена.");
		}
		return "redirect:" + URIPath.STATION + URIPath.PATH_SEPARATOR + stationId.toString();
	}

    @RequestMapping(value = URIPath.STATION_ADD_ORDER_EMAIL,method = RequestMethod.POST)
    public String addOrderEmail(@RequestParam Map<String,String> requestParams)
    {
        String name = requestParams.getOrDefault("name", new String());
        String email = requestParams.getOrDefault("email", new String());
        String comment = requestParams.getOrDefault("comment", new String());
        Integer stationId = Integer.valueOf(requestParams.getOrDefault("stationId", "-1"));
        if(stationId<0)
        {
            return "redirect:"+URIPath.BASE;
        }
        Station station = stationRepository.findOne(stationId);
        if(name.equals("")||email.equals("")||comment.equals("")||station==null)
        {
            return "redirect:"+URIPath.STATION+URIPath.PATH_SEPARATOR+stationId.toString();
        }
        //Проверка правильности ввода электронной почты
        Pattern pattern = Pattern.compile("(.*@.*\\.)"); // Наличие '@' и
        // '.' после @
        Matcher matcher = pattern.matcher(email);
        if (!matcher.find()) {
            notificationService.addErrorMessage("Введен некорректный e-mail адрес");
            return "redirect:"+URIPath.STATION+URIPath.PATH_SEPARATOR+stationId.toString();
        }
        OrderEmail orderEmail = orderEmailRepository.save(new OrderEmail(name, email, comment, station));
        if(orderEmail==null)
            notificationService.addErrorMessage("Из-за ошибки заявка не была сохранена");
        else notificationService.addInfoMessage("Заявка создана");
        return URIPath.REDIRECT+URIPath.STATION+URIPath.PATH_SEPARATOR+stationId.toString();
    }

    @RequestMapping(value = "/getModels",method = RequestMethod.GET)
    public String getModels(@RequestParam Map<String,String> requestParams, RedirectAttributes redirectAttributes)
    {
        redirectAttributes.addAllAttributes(requestParams);
        return "redirect:/getModels";
    }

    @RequestMapping(value = "/submit",method = RequestMethod.POST)
    public String createBid(@RequestParam Map<String,String> requestParams,
                            RedirectAttributes redirectAttributes,
                            @RequestParam("repairTypesids") List<String>repairTypesStrings)
    {
        redirectAttributes.addAllAttributes(requestParams);
        redirectAttributes.addAttribute(repairTypesStrings);
        return "redirect:/new-bid/submit";
    }
}
