package com.ss.remont.controller;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.AuthorityType;
import com.ss.remont.auth.CurrentUser;
import com.ss.remont.auth.Role;
import com.ss.remont.balloon.Balloon;
import com.ss.remont.balloon.BalloonOptions;
import com.ss.remont.balloon.BalloonOptionsKeyWords;
import com.ss.remont.config.GalleryProperties;
import com.ss.remont.entity.*;
import com.ss.remont.mail.Mail;
import com.ss.remont.mail.template.StatusChangedTemplate;
import com.ss.remont.repository.*;
import com.ss.remont.service.DataService;
import com.ss.remont.service.MailService;
import com.ss.remont.service.NotificationService;
import com.ss.remont.service.SaveUser;
import com.ss.remont.storage.StorageService;
import com.ss.remont.utils.CheckBoxParser;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Tensky on 09.12.2016.
 */
@Controller
public class MyCabinetController
{

    @Autowired
    CarMarkRepository carMarkRepository;

    @Autowired
    CurrentUser currentUser;

    @Autowired
    StationRepository stationRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    DiscountRepository discountRepository;

    @Autowired
    StorageService storageService;

    @Autowired
    SimpleAuthUserRepository simpleAuthUserRepository;

    @Autowired
    AuthUserRepository authUserRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    RepairTypePriceRepository repairTypePriceRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    RepairTypeRepository repairTypeRepository;

    @Autowired
    @Qualifier("dataService")
    DataService dataService;

    @Autowired
    OrderCallRepository orderCallRepository;

    @Autowired
    OrderEmailRepository orderEmailRepository;

    @Autowired
    RecallRepository recallRepository;

    @Autowired
    private SaveUser saveUser;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private StationBidsRepository stationBidsRepository;

    @Autowired
    ImagesRepository imagesRepository;

    @Autowired
    GalleryProperties galleryProperties;

    @Autowired
    ResponseRepository responseRepository;

    @Autowired
    BidRepository bidRepository;

    @Autowired
    StationDirectorRepository stationDirectorRepository;

    @Autowired
    MailService mailService;

    @RequestMapping(value = URIPath.MYCABINET, method = {RequestMethod.GET, RequestMethod.POST})
    public String myCabinet(Model model,
                            @RequestParam(name = "activeTab", required = false) String activeTab,
                            @RequestParam(name = "fromDate", required = false) String fromDate,
                            @RequestParam(name = "toDate", required = false) String toDate)
            throws UserPrincipalNotFoundException
    {
        Map<String, String> requestParams = new HashMap<String, String>();
        requestParams.put("fromDate", fromDate);
        requestParams.put("toDate", toDate);
        if (activeTab != null)
        {
            requestParams.put("activeTab", activeTab);
        }
        AuthUser authUser = currentUser.getUser();
        model.addAllAttributes(requestParams);
        if (!requestParams.containsKey("activeTab"))
        {
            model.addAttribute("activeTab", "activebids");
        }
        switch (authUser.getRole())
        {
            case STATION:
            {
                initStationPage(model, requestParams);
                return "mycabinet_station";
            }
            case CLIENT:
            {
                initClientPage(model);
                return "mycabinet_client";
            }
            default:
            {
                throw new UserPrincipalNotFoundException("Unknown authUser.role [" + authUser.getRole() + "]");
            }
        }
    }

    private Model initStationPage(Model model, Map<String, String> requestParams)
    {
        AuthUser authUser = currentUser.getUser();
        Station station = stationRepository.findOne(authUser.getId());
        model.addAttribute("station", station);
        List<Discount> discounts = discountRepository.getThisStationDiscounts(station);
        model.addAttribute("discounts", discounts);

        Integer sId = station.getId();
        List<StationBids> activebids = stationBidsRepository
                .findByStationIdAndStatusAndBidStationIsNull(sId, BidStatus.WAITING_RESPONSE);
        List<StationBids> directbids = stationBidsRepository.findByStationIdAndStatusAndBidStationId(sId,
                BidStatus.WAITING_RESPONSE, sId);
        List<StationBids> mybids = stationBidsRepository.findByStationIdAndStatus(sId, BidStatus.REGISTERED);
        mybids.addAll(stationBidsRepository.findByStationIdAndStatus(sId,BidStatus.PROCESSING));
        mybids.addAll(stationBidsRepository.findByStationIdAndStatus(sId,BidStatus.IN_WORK));
        mybids.sort(new Comparator<StationBids>() {
            @Override
            public int compare(StationBids o1, StationBids o2)
            {
                if(o1.getId()>o2.getId())
                    return 1;
                else return -1;
            }
        });
        List<BidStatus> bidStatuses = new ArrayList<BidStatus>();
        bidStatuses.add(BidStatus.CANCELED_CLIENT);
        bidStatuses.add(BidStatus.CANCELED_STATION);
        bidStatuses.add(BidStatus.COMPLETED);
        bidStatuses.add(BidStatus.REJECTED);
        List<StationBids> archivebids = stationBidsRepository.findByStationIdAndStatusIn(sId, bidStatuses);

        List<Recall> recalls = station.getRecalls();

        model.addAttribute("activebids", activebids);
        model.addAttribute("directbids", directbids);
        model.addAttribute("mybids", mybids);
        model.addAttribute("archivebids", archivebids);
        model.addAttribute("recalls", recalls);
        model.addAttribute("BidStatus", BidStatus.class);

        String fromDate = requestParams.get("fromDate");
        String toDate = requestParams.get("toDate");
        if (fromDate != null && toDate != null)
        {
            List<BalanceHistory> list = dataService.findBalanceHistoryByStationIdAndModifyDateBetween(station.getId(),
                    fromDate, toDate);
            model.addAttribute("fromDate", fromDate);
            model.addAttribute("toDate", toDate);
            model.addAttribute("balances", list);
        }
        model.addAttribute("balanceAction", URIPath.MYCABINET);

        List<RepairTypePrice> repairTypePrices = repairTypePriceRepository.getStationPriceList(station);
        model.addAttribute("repairTypePrices", repairTypePrices);
        List<RepairType> repairTypes = (List<RepairType>) repairTypeRepository.findAll();
        model.addAttribute("repairTypes", repairTypes);

        List<OrderCall> orderCalls = orderCallRepository.findAndSortStationOrderCalls(station);
        model.addAttribute("orderCalls", orderCalls);
        List<OrderEmail> orderEmails = orderEmailRepository.findAndSortStationOrderEmails(station);
        model.addAttribute("orderEmails", orderEmails);
        model.addAttribute("authUser", authUser);

        return model;
    }

    private Model initClientPage(Model model)
    {
        AuthUser authUser = currentUser.getUser();
        Client client = clientRepository.findOne(authUser.getId());
        List<Car> cars = carRepository.findUserCars(authUser.getId());
        Iterable<CarMark> carMarks = carMarkRepository.findAll();
        List<Bid> bids = client.getBids();
        model.addAttribute("client", client);
        model.addAttribute("cars", cars);
        model.addAttribute("carMarks", carMarks);
        model.addAttribute("authUser", authUser);
        model.addAttribute("bids", bids);
        model.addAttribute("BidStatus", BidStatus.class);
        return model;
    }


    @RequestMapping(value = URIPath.MYCABINET_EDIT_DISCOUNT, method = RequestMethod.POST, params = "action=edit")
    public
//    @ResponseBody
    String editDiscount(@RequestParam Map<String, String> requestParams, @RequestParam(value = "photo", required = false) MultipartFile multipartFile) throws ParseException
    {
        Station station = stationRepository.findOne(currentUser.getUser().getId());
        if (station == null)//err
        {
            // TODO: 11.01.2017 Вместо ошибки (временно)
            return "redirect:" + URIPath.MYCABINET;
        }
        List<Discount> discounts = station.getDiscounts();
        Long discountId = Long.valueOf(requestParams.getOrDefault("discountId", "-1"));

        if (discountId < 0)//err
        {
            return "redirect:" + URIPath.MYCABINET;
        }

        Discount discount = discountRepository.findOne(discountId);
        if (discount == null)
        {
            return "redirect:" + URIPath.MYCABINET;
        }
        else if (!discounts.contains(discount))
        {
            return "redirect:" + URIPath.MYCABINET;
        }

        String title = requestParams.getOrDefault("title", discount.getTitle());
        String info = requestParams.getOrDefault("info", discount.getInfo());

        Images photo;
        if (!multipartFile.isEmpty())
        {
            try
            {
                photo = storageService.store(currentUser.getUser(), multipartFile, ImagesType.ICON);
                photo = imagesRepository.save(photo);
            } catch (IOException e)
            {
                e.printStackTrace();
                notificationService.addErrorMessage("Не удалось загрузить новое изображение");
                photo = discount.getPhoto();
            }
        }
        else
        {
            photo = discount.getPhoto();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date_start = simpleDateFormat.parse(requestParams.getOrDefault("date_start", simpleDateFormat.format(discount.getDate_start())));
        Date date_end = simpleDateFormat.parse(requestParams.getOrDefault("date_end", simpleDateFormat.format(discount.getDate_end())));
        discount.setTitle(title);
        discount.setInfo(info);
        discount.setDate_start(date_start);
        discount.setDate_end(date_end);
        discount.setPhoto(photo);
        discountRepository.save(discount);
//        discountRepository.edit(discount, title, info, date_start, date_end, photo);
//        entityManager.clear();
        return "redirect:" + URIPath.MYCABINET;
    }

    @RequestMapping(value = URIPath.MYCABINET_EDIT_DISCOUNT, method = RequestMethod.POST, params = "action=delete")
    @ResponseBody
    public String deleteDiscount(@RequestParam("discountId") Long discountId)
            throws UserPrincipalNotFoundException
    {
        Discount discount = discountRepository.findOne(discountId);
        AuthUser authUser = currentUser.getRegistrationAuth();
        Station station = stationRepository.findOne(authUser.getId());
        if (station.getId() == discount.getStation().getId())
        {
            discountRepository.delete(discount);
        }
        return "done";
    }

    @RequestMapping(value = URIPath.MYCABINET + URIPath.PATH_SEPARATOR + "addToGallery")
    public String addToStationGallery(@RequestParam("photo") List<MultipartFile> multipartFiles)
    {
//        GalleryProperties galleryProperties = new GalleryProperties(); // FIXME: 13.01.2017 ВЫНЕСТИ В PROPERTIES
        if (multipartFiles.size() < 1)
        {
            notificationService.addErrorMessage("Вы не выбрали файл для загрузки");
            return URIPath.REDIRECT + URIPath.MYCABINET + "#gallery";
        }
        AuthUser authUser = currentUser.getUser();
        Station station = stationRepository.findOne(authUser.getId());
        int currentGallerySize = station.getGallery().size();


        if (currentGallerySize + multipartFiles.size() > galleryProperties.getMaxNumberOfImages())
        {
            notificationService.addErrorMessage("В вашей галерее не может быть более "
                    + galleryProperties.getMaxNumberOfImages() + " фотографий." +
                    "\nСейчас:[" + currentGallerySize + "]" +
                    "\nПопытка загрузить " + multipartFiles.size() + (multipartFiles.size() == 1 ? " файл" : " файлов"));
            return URIPath.REDIRECT + URIPath.MYCABINET + "#gallery";

        }

        try
        {
            List<Images> imagesList = storageService.store(authUser, multipartFiles, ImagesType.GALLERY);
            station.getGallery().addAll(imagesList);
            stationRepository.save(station);
            notificationService.addInfoMessage("Фотографии загружены в галерею");
            return URIPath.REDIRECT + URIPath.MYCABINET + "#gallery";

        } catch (IOException e)
        {
//            e.printStackTrace();
            notificationService.addErrorMessage("Невозможно загрузить выбранны" + (multipartFiles.size() > 1 ? "е файлы" : "й файл"));
            return URIPath.REDIRECT + URIPath.MYCABINET + "#gallery";

        }
    }

    @RequestMapping(value = URIPath.MYCABINET_DELETE_FROM_GALLERY,
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> deleteFromGallery(@RequestParam Map<String, String> requestParams)
    {
        Integer photoId = Integer.valueOf(requestParams.getOrDefault("photoId", "-1"));
        if (photoId < 0)
        {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Невозможно получить параметр photoId [" + requestParams.get(photoId) + "]");
        }
        AuthUser authUser = currentUser.getUser();
        Images image = imagesRepository.findOne(photoId);
        if (image == null)
        {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("В базе данных нет изображения с таким идентификатором [" + photoId + "]");
        }
        Station station = stationRepository.findOne(authUser.getId());
        if (!station.getGallery().contains(image))
        {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Изображение с идентификатором [" + photoId + "] не принадлежит этому пользователю");
        }
//        if (storageService.deleteImageFromDisk(image))
//        {
        station.getGallery().remove(image);
        stationRepository.save(station);
        storageService.deleteImageFromDisk(image);
        imagesRepository.delete(photoId);
//        entityManager.clear();
        return ResponseEntity.status(HttpStatus.OK).body("Изображение удалено");
//        }
//        else
//        {
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Не удается удалить изображение с жесткого диска");
//        }
    }

    @RequestMapping(value = URIPath.MYCABINET_CHANGE_AVATAR,
            method = RequestMethod.POST)
    public String changeAvatar(@RequestParam("photo") MultipartFile multipartFile)
    {
        AuthUser authUser = currentUser.getUser();
        try
        {
            Images image = storageService.store(authUser, multipartFile, ImagesType.AVATAR);
            Images avatar = imagesRepository.save(image);
            if (avatar != null)
            {

//                return ResponseEntity.status(HttpStatus.OK).body("Изображение профиля изменено");
                authUser.setPhoto(avatar);
                authUserRepository.save(authUser);
                notificationService.addInfoMessage("Изображение профиля изменено");
                return URIPath.REDIRECT + URIPath.MYCABINET + "#profile";

            }
            else
            {
                notificationService.addErrorMessage("Не удалось загрузить изображение");
                return URIPath.REDIRECT + URIPath.MYCABINET + "#profile";

//                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Не удалось загрузить изображение");
            }
        } catch (IOException e)
        {
            notificationService.addErrorMessage("Не удалось загрузить изображение");
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Не удалось загрузить изображение");
            return URIPath.REDIRECT + URIPath.MYCABINET + "#profile";
        }
    }

    @RequestMapping(value = URIPath.MYCABINET_ADD_DISCOUNT, method = RequestMethod.POST)
    public String addDiscount(@RequestParam Map<String, String> requestParams,
                              @RequestParam("photo") MultipartFile multipartFile) throws ParseException, UserPrincipalNotFoundException
    {
        AuthUser authUser = currentUser.getRegistrationAuth();
        Images photo;
        if (!multipartFile.isEmpty() && multipartFile != null && !multipartFile.getOriginalFilename().isEmpty())
        {
            try
            {
                photo = storageService.store(authUser, multipartFile, ImagesType.ICON);
                photo = imagesRepository.save(photo);
            } catch (IOException e)
            {
                e.printStackTrace();
                photo = null;
                notificationService.addErrorMessage("Невозможно сохранить изображение");
            }
        }
        else
        {
            photo = null;
        }
        String title = requestParams.get("title");
        String info = requestParams.get("info");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date_start = simpleDateFormat.parse(requestParams.get("date_start"));
        Date date_end = simpleDateFormat.parse(requestParams.get("date_end"));
//        Date currentDate = new Date();
        // TODO: 09.12.2016
        Station station = stationRepository.findOne(2);
        Discount discount = new Discount(title, info, photo, date_start, date_end, station);
        discount = discountRepository.save(discount);
        if (discount == null)
            notificationService.addErrorMessage("Не удалось сохранить изменения");
        else
        {
            notificationService.addInfoMessage("Изменения акции сохранены");
        }
        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = URIPath.MYCABINET_SAVE_MY_INFO, method = RequestMethod.POST)
    public String saveMyInfo(@RequestParam Map<String, String> requestParams, Model model)
            throws UserPrincipalNotFoundException
    {
        AuthUser authUser = currentUser.getRegistrationAuth();
        boolean res = updateUser(currentUser.getRegistrationAuth(), requestParams);
        switch (authUser.getRole())
        {
            case STATION:
            {
                res &= updateStation((Station) currentUser.getUser(), requestParams);
                break;
            }
            case CLIENT:
            {
                break;
            }
            default:
                break;
        }
        if (res)
        {
            notificationService.addInfoMessage("Данные успешно сохранены");
        }
//        return myCabinet(model, null, null, null);
        return URIPath.REDIRECT + URIPath.MYCABINET + "#profile";
    }

    private boolean updateStation(Station station, Map<String, String> requestParams)
    {
        station.setInfo(requestParams.get("myinfo"));
        station.setSite(requestParams.get("site"));
        station.getAddress().setAddr(requestParams.get("address"));
        Double latitude = Double.valueOf(requestParams.get("latitude"));
        Double longitude = Double.valueOf(requestParams.get("longitude"));
        String clCom = requestParams.getOrDefault("clientCommunication", "");
        station.setClientCommunication(!clCom.isEmpty());
        station.getAddress().setLatitude(latitude);
        station.getAddress().setLongitude(longitude);
        stationRepository.save(station);
        return true;
    }

    private boolean updateUser(AuthUser user, Map<String, String> requestParams)
    {
        user.setName(requestParams.get("name"));
        user.setEmail(requestParams.get("email"));
        user.setPhone(requestParams.get("phone"));
        boolean emailNotice = CheckBoxParser.parseCheckBoxValue(requestParams.get("emailNotice"));
        user.setEmailNotice(emailNotice);
        boolean updatePassword = CheckBoxParser.parseCheckBoxValue(requestParams.get("updatePassword"));
        if (updatePassword && user.getType().equals(AuthorityType.SIMPLE))
        {
            if (!saveUser.updateSimpleUserPassword(user, requestParams.get("oldPassword"),
                    requestParams.get("password"), requestParams.get("repass")))
            {
                return false;
            }
        }
        authUserRepository.save(user);
        return true;
    }

    @RequestMapping(value = URIPath.MYCABINET_PRICE_LIST, method = RequestMethod.POST, params = "action=add")
//    @ResponseBody
    public String addPriceListRow(@RequestParam Map<String, String> requestParams)
    {
        Integer stationId = currentUser.getUser().getId();
        Station station = stationRepository.findOne(stationId);
        if (station == null)
        {
            return "redirect:" + URIPath.MYCABINET; // TODO: 10.01.2017 Потом, когда будет ajax - сделать через ResponseEntity
        }
        Integer repairTypeId = Integer.valueOf(requestParams.getOrDefault("repairType", "-1"));
        Double price = Double.parseDouble(requestParams.getOrDefault("price", "-1"));
        if (repairTypeId < 0 || price < 0)
        {
            return "redirect:" + URIPath.MYCABINET; //Тоже, что и выше в туду
        }
        RepairType repairType = repairTypeRepository.findOne(repairTypeId);
        if (repairType == null)
        {
            return "redirect:" + URIPath.MYCABINET; //Тоже, что и выше в туду
        }
        RepairTypePrice repairTypePrice = new RepairTypePrice(station, repairType, price);
        repairTypePriceRepository.save(repairTypePrice);
        return "redirect:" + URIPath.MYCABINET + "#price";
    }

    @RequestMapping(value = URIPath.MYCABINET_PRICE_LIST, method = RequestMethod.POST, params = "action=delete")
//    @ResponseBody
    public String deletePriceListRow(@RequestParam Map<String, String> requestParams)
    {
        Integer repairTypePriceId = Integer.valueOf(requestParams.getOrDefault("repairTypePriceId", "-1"));
        if (repairTypePriceId < 0)
        {
            return "redirect:" + URIPath.MYCABINET + "#price"; // TODO: 10.01.2017 Потом, когда будет ajax - сделать через ResponseEntity
        }
        Station station = repairTypePriceRepository.getStationByRepairTypePrice(repairTypePriceId);

        if (station == null)
        {
            return "redirect:" + URIPath.MYCABINET + "#price";//Если станция не найдена
        }

        //Если этой станции не принадлежит эта запись (ну, мало ли)
        if (!station.getRepairTypePrices()
                .contains(repairTypePriceRepository.findOne(repairTypePriceId)))
        {
            return "redirect:" + URIPath.MYCABINET + "#price"; //Тоже, что и выше
        }

        repairTypePriceRepository.delete(repairTypePriceId);
//        entityManager.clear();
        return "redirect:" + URIPath.MYCABINET + "#price";
    }

    @RequestMapping(value = URIPath.MYCABINET + URIPath.PATH_SEPARATOR + "changeOrderState",
            params = "table=orderCall", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> changeCallOrderState(@RequestParam Map<String, String> requestParams)
    {
        if (!currentUser.getUser().getRole().equals(Role.STATION))
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Нет прав для использования метода");
        }
        Integer callOrderId = Integer.valueOf(requestParams.getOrDefault("callOrderId", "-1"));
        if (callOrderId < 0)
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Невозможно правильно идентификатор заказа; callOrderId=" + callOrderId);
        }
        OrderCall orderCall = orderCallRepository.findOne(callOrderId);
        orderCall.setAnswered(!orderCall.getAnswered());
        orderCallRepository.save(orderCall);
//        orderCallRepository.toggleAnswer(callOrderId);
//        entityManager.clear();
        return ResponseEntity.status(HttpStatus.OK).body("Статус заказа id=" + callOrderId + " изменен");
    }

    @RequestMapping(value = URIPath.MYCABINET_CHANGE_ORDER_STATE,
            params = "table=orderEmail", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> changeEmailOrderState(@RequestParam Map<String, String> requestParams)
    {
        if (!currentUser.getUser().getRole().equals(Role.STATION))
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Нет прав для использования метода");
        }
        Integer emailOrderId = Integer.valueOf(requestParams.getOrDefault("emailOrderId", "-1"));
        if (emailOrderId < 0)
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Невозможно правильно идентификатор заказа; callOrderId=" + emailOrderId);
        }
        OrderEmail orderEmail = orderEmailRepository.findOne(emailOrderId);
        orderEmail.setAnswered(!orderEmail.getAnswered());
//        orderEmailRepository.toggleAnswer(emailOrderId);
//        entityManager.clear();
        return ResponseEntity.status(HttpStatus.OK).body("Статус заказа id=" + emailOrderId + " изменен");
    }

    @RequestMapping(value = URIPath.MYCABINET_GET_RESPONSES_ON_MAP, method = RequestMethod.GET)
    @ResponseBody
    public Object[] getBalloonsForResponse(@PathVariable("bidId") String id, Model model) throws IOException, TemplateException
    {
        Integer bidId;
        try
        {
            bidId = Integer.valueOf(id);
        } catch (NumberFormatException e)
        {
            bidId = -1;
        }
        if (bidId < 0)
        {
            return null;
        }
        Client client = clientRepository.findOne(currentUser.getUser().getId());
        if (client == null)
        {
            return null;
        }
        Bid bid = bidRepository.findOne(bidId);
        if (bid == null)
        {
            return null;
        }
        if (!client.getBids().contains(bid))
        {
            return null;
        }
        List<Response> responses = responseRepository.findResponsesByBid(bid);
        if (responses == null)
        {
            return null;
        }
        else if (responses.size() < 1)
        {
            return null;
        }
        return new Object[]{new Balloon(responses), new BalloonOptions(BalloonOptionsKeyWords.ISLANDS_GREEN_STRETCHY_ICON)};

    }

    @RequestMapping(value = URIPath.MYCABINET_SHOW_MY_RECALL, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> showMyRecall(@RequestParam("recallId") String rId) throws IOException, TemplateException
    {
        Client client = (Client) currentUser.getUser();
        Integer recallId = Integer.valueOf(rId);
        ModelAndView modelAndView = new ModelAndView();
        Map<String, Object> model = modelAndView.getModel();
        Recall recall = recallRepository.findOne(recallId);
        if (recall == null)
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Не удалось найти отзыв #" + recallId);
        }
        if (!client.equals(recall.getClient()))
        {
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body("Отзыв #" + recall.getId() + " не принадлежит клиенту #" + client.getId());
        }
        model.put("recall", recall);
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_25);
        configuration.setDirectoryForTemplateLoading(new File("src/main/resources/templates/recall"));
        Template template = configuration.getTemplate("myRecall.ftl", "UTF-8");
        String body = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        return ResponseEntity.status(HttpStatus.OK).body(body);
    }

    @RequestMapping(value = URIPath.MYCABINET_ADD_STATION_DIRECTOR, method = RequestMethod.POST)
    public String stationAddDirector(@RequestParam Map<String, String> requestParams)
    {
        String firstName = requestParams.getOrDefault("firstName", "");
        String patronymic = requestParams.getOrDefault("patronymic", "");
        String lastName = requestParams.getOrDefault("lastName", "");
        String phoneNumber = requestParams.getOrDefault("phoneNumber", "");
        String email = requestParams.getOrDefault("email", "");
        Integer stationId = Integer.valueOf(requestParams.getOrDefault("stationId", "-1"));
        Boolean hasErr = Boolean.FALSE;
        AuthUser user = currentUser.getUser();
        if (firstName.isEmpty())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Не указано имя");
        }
        if (patronymic.isEmpty())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Не указано отчество");
        }
        if (lastName.isEmpty())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Не указана фамилия");
        }
        if (stationId < 0 || stationId != user.getId())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Не удалось определить станцию");
        }
        if (phoneNumber.isEmpty())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Не указан номер телефона");
        }
        if (email.isEmpty())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Не указан адрес электронной почты");
        }
        Pattern pattern = Pattern.compile("[0-9\\-\\\\!@#:;\",/$%\\^&\\*\\(\\)\\\\[\\\\]]");
        Matcher matcher = pattern.matcher(firstName);
        if (matcher.find())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("В имени могут содержаться только буквы и '-'");
        }
        matcher = pattern.matcher(patronymic);
        if (matcher.find())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("В отчестве могут содержаться только буквы и '-'");
        }
        matcher = pattern.matcher(lastName);
        if (matcher.find())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("В фамилии могут содержаться только буквы и '-'");
        }
        pattern = Pattern.compile("[^0-9+\\s]");
        matcher = pattern.matcher(phoneNumber);
        if (matcher.find())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Номер телефона может состоять только из цифр и '+'");
        }
        pattern = Pattern.compile("[^a-z0-9@a-z0-9.a-z\\s]");
        matcher = pattern.matcher(email);
        if (matcher.find())
        {
            hasErr = Boolean.TRUE;
            notificationService.addErrorMessage("Неверный электронный адрес почты");
        }
        if (hasErr)
            return URIPath.REDIRECT + URIPath.MYCABINET;
        Station station = stationRepository.findOne(stationId);
        StationDirector stationDirector = station.getStationDirector();
        if (stationDirector == null)
            stationDirector = stationDirectorRepository.save(new StationDirector(firstName, patronymic, lastName, email, phoneNumber, station));
        else
        {
            stationDirector.setFirstName(firstName);
            stationDirector.setPatronymic(patronymic);
            stationDirector.setLastName(lastName);
            stationDirector.setEmail(email);
            stationDirector.setPhoneNumber(phoneNumber);
            stationDirector = stationDirectorRepository.save(stationDirector);
        }
        if (stationDirector == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить управляющего");
        }
        station.setStationDirector(stationDirector);
        station = stationRepository.save(station);
        notificationService.addInfoMessage("Изменения сохранены");
        return URIPath.REDIRECT + URIPath.MYCABINET + "#profile";
    }

    private void sendStatusChanged(AuthUser user, Response response)
    {
        if(!user.isEmailNotice())
            return;
        Message message = new Message();
        message.setMessage("");
        message.setRecipient(user);
//        response.setBid(response.getBid());
        message.setResponse(response);
        StatusChangedTemplate statusChangedTemplate = new StatusChangedTemplate(message);
        mailService.sendEmail(statusChangedTemplate);
    }

    @RequestMapping(value = "/mycabinet/client/cancelBid", method = RequestMethod.POST)
    public String cancelClientBid(@RequestParam("bidId") Integer bidId)
    {
        Bid bid = bidRepository.findOne(bidId);
        bid.setStatus(BidStatus.CANCELED_CLIENT);
        bid.setClosed(true);
        bid = bidRepository.save(bid);
        List<StationBids> stationBids = stationBidsRepository.findByBidId(bid.getId());
        if (stationBids != null)
        {
            for (StationBids sb :
                    stationBids)
            {
                sb.setStatus(BidStatus.CANCELED_CLIENT);
                Response response = responseRepository.findByStationIdAndBidId(sb.getStation().getId(), sb.getBid().getId());
                AuthUser stationUser = authUserRepository.findOne(sb.getStation().getId());
                if (stationUser != null)
                {
                    if (response != null)
                        sendStatusChanged(stationUser,
                                response);
                }
            }
            stationBidsRepository.save(stationBids);
        }
        notificationService.addInfoMessage("Заявка помещена в архив");
        AuthUser user = currentUser.getUser();
//        if (user.isEmailNotice())
//        {
            Response response = new Response();
            response.setBid(bid);
            sendStatusChanged(user, response);
//        }
        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = "/mycabinet/client/restoreBid", method = RequestMethod.POST)
    public String clientRestoreBid(@RequestParam("bidId") Integer bidId)
    {
        Bid bid = bidRepository.findOne(bidId);
        List<StationBids> stationBids = stationBidsRepository.findByBidId(bid.getId());
        for (StationBids sb :
                stationBids)
        {
            sb.setStatus(BidStatus.WAITING_RESPONSE);
        }
        stationBids = stationBidsRepository.save(stationBids);
        if (stationBids == null)
        {
            notificationService.addErrorMessage("Невозможно изменить статусы заявок для станций");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        Client client = clientRepository.findOne(currentUser.getUser().getId());
        if (!bid.getClient().equals(client))
        {
            notificationService.addErrorMessage("Данная заявка Вам не принадлежит");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        BidStatus status = bid.getStatus();
        if (!status.equals(BidStatus.CANCELED_CLIENT))
        {
            notificationService.addErrorMessage("Вы не можете восстановить заявку, которую не отменяли");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        bid.setStatus(BidStatus.WAITING_RESPONSE);
        bid.setClosed(false);
        bid = bidRepository.save(bid);
        if (bid == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить изменения");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        notificationService.addInfoMessage("Заявка восстановлена");
        Response response = new Response();
        response.setBid(bid);
        sendStatusChanged(authUserRepository.findOne(client.getId()), response);
        List<Response> responsesByBid = responseRepository.findResponsesByBid(bid.getId());
        if (responsesByBid != null)
        {
            for (Response resp :
                    responsesByBid)
            {
                AuthUser station = authUserRepository.findOne(resp.getStation().getId());
                if (station != null)
                    sendStatusChanged(station,response);
            }
        }

        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = "/mycabinet/station/closeBid", method = RequestMethod.POST)
    public String stationCloseBid(@RequestParam("sBidId") Integer sBidId, @RequestParam("status") String statusName)
    {
        AuthUser user = currentUser.getUser();
        Station station = stationRepository.findOne(user.getId());
        BidStatus status = BidStatus.findByString(statusName);
        if (!status.equals(BidStatus.REJECTED) && !status.equals(BidStatus.COMPLETED))
        {
            notificationService.addErrorMessage("Невозможно присвоить статус: " + status);
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        StationBids stationBid = stationBidsRepository.findOne(Long.valueOf(sBidId));
        if (!station.equals(stationBid.getStation()))
        {
            notificationService.addErrorMessage("Невозможно изменить статус, " +
                    "т.к. заявка принадлежит другой станции (Ваш номер #" + station.getId() + ", " +
                    "номер станции у заявки #" + stationBid.getStation().getId());
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        stationBid.setStatus(status);
        stationBid = stationBidsRepository.save(stationBid);
        if (stationBid == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить изменение заявки для станции");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        Bid bid = stationBid.getBid();
        bid.setStatus(status);
        bid.setClosed(true);
        bid = bidRepository.save(bid);
        if (bid == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить изменение заявки");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        notificationService.addInfoMessage("Заявка помещена в архив со статусом \"" + status + "\"");
        Response response = responseRepository.findByStationIdAndBidId(user.getId(), bid.getId());
//        if (user.isEmailNotice() && response != null)
//        {
            sendStatusChanged(user, response);
//        }
        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = "/mycabinet/station/cancelDirectBid", method = RequestMethod.POST)
    public String stationCancelDirectBid(@RequestParam("sBidId") Long sBidId)
    {
        StationBids stationBid = stationBidsRepository.findOne(sBidId);
        if (stationBid == null)
        {
            notificationService.addErrorMessage("Невозможно определить заявку станции");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        stationBid.setStatus(BidStatus.CANCELED_STATION);
        stationBid = stationBidsRepository.save(stationBid);
        if (stationBid == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить изменение статуса прямой заявки");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        Bid bid = stationBid.getBid();
        bid.setStatus(BidStatus.CANCELED_STATION);
        bid = bidRepository.save(bid);
        if (bid == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить изменение статуса заявки");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        notificationService.addInfoMessage("Заявка помещена в архив");
        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = "/mycabinet/station/cancelActiveBid", method = RequestMethod.POST)
    public String stationCancelActiveBid(@RequestParam("sBidId") Long sBidId)
    {
        StationBids stationBid = stationBidsRepository.findOne(sBidId);
        stationBid.setStatus(BidStatus.CANCELED_STATION);
        stationBid = stationBidsRepository.save(stationBid);
        if (stationBid == null)
        {
            notificationService.addErrorMessage("Невозможно сохранить изменения статуса заявки станции");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        ////////////////////////////////////////////////////////////////////////////////
        Response response = new Response();
        Bid bid = stationBid.getBid();
        response.setBid(bid);
        sendStatusChanged(currentUser.getUser(), response);
        sendStatusChanged(authUserRepository.findOne(bid.getClient().getId()),response);
        ////////////////////////////////////////////////////////////////////////////////
        notificationService.addInfoMessage("Заявка помещена в архив");
        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = "/mycabinet/station/restoreBid", method = RequestMethod.POST)
    public String stationRestoreBid(@RequestParam("sBidId") Long sBidId)
    {
        StationBids stationBid = stationBidsRepository.findOne(sBidId);
        Bid bid = stationBid.getBid();
        if (!stationBid.getStatus().equals(BidStatus.CANCELED_STATION))
        {
            notificationService.addErrorMessage("Невозможно восстановить заявку, которую Вы не отменяли");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        BidStatus bidStatus = bid.getStatus();
        if (!bidStatus.equals(BidStatus.WAITING_RESPONSE) && !bidStatus.equals(BidStatus.CANCELED_STATION))
        {
            notificationService.addErrorMessage("Невозможно восстановить заявку, т.к. она уже присвоена другой станцией");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        stationBid.setStatus(BidStatus.WAITING_RESPONSE);
        stationBid = stationBidsRepository.save(stationBid);
        bid = stationBid.getBid();
        bid.setStatus(BidStatus.WAITING_RESPONSE);
        bid = bidRepository.save(bid);
        if (bid == null)
        {
            notificationService.addErrorMessage("Невозможно изменить статус заявки");
            return URIPath.REDIRECT + URIPath.MYCABINET;
        }
        notificationService.addInfoMessage("Заявка восстановлена");
        Response response = new Response();
        response.setBid(bid);
        sendStatusChanged(currentUser.getUser(),response);
        sendStatusChanged(authUserRepository.findOne(bid.getClient().getId()),response);
        return URIPath.REDIRECT + URIPath.MYCABINET;
    }

    @RequestMapping(value = URIPath.MYCABINET+"/client/acceptStation",method = RequestMethod.POST)
    public String acceptStation(@RequestParam("stationLogin")String stationLogin, @RequestParam("bidId")Integer bidId)
    {
        Bid bid = bidRepository.findOne(bidId);
        if(bid==null)
        {
            notificationService.addErrorMessage("Невозможно определить заявку");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
        if(bid.getStation()!=null)
        {
            notificationService.addErrorMessage("Заявка уже отдана станции "+bid.getStation().getName());
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
        AuthUser stationUser = authUserRepository.findOneByPhone(stationLogin);
        if(stationUser==null)
        {
            notificationService.addErrorMessage("Невозможно определить станцию");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
        Station station = stationRepository.findOne(stationUser.getId());
        if(station==null)
        {
            notificationService.addErrorMessage("Невозможно определить станцию");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
        bid.setStation(station);
        bid.setStatus(BidStatus.PROCESSING);
        StationBids stationBid = stationBidsRepository.findByBidIdAndStationId(bid.getId(), station.getId());
        stationBid.setStatus(BidStatus.PROCESSING);
        List<StationBids> byBidId = stationBidsRepository.findByBidId(bid.getId());
        byBidId.remove(stationBid);
        for (StationBids sbid :
                byBidId)
        {
            sbid.setStatus(BidStatus.CANCELED_CLIENT);
        }
        stationBidsRepository.save(byBidId);
        notificationService.addInfoMessage("Заявка отдана на выполнение станции \""+station.getName()+"\"");
        return URIPath.REDIRECT+URIPath.MYCABINET;
    }

//    @RequestMapping(value = URIPath.MYCABINET+"/toggle/clientCommunication",method = RequestMethod.POST)
//    @ResponseBody
//    public ResponseEntity toggleClientCommunication()
//    {
//        Role role = currentUser.getUser().getRole();
//        if(!role.equals(Role.STATION))
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Данная функция доступна только станциям.");
//        Station station = stationRepository.findOne(currentUser.getUser().getId());
//        station.setClientCommunication(!station.getClientCommunication());
//        station= stationRepository.save(station);
//        if(station==null)
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Не удалось сохранить изменения");
//        return ResponseEntity
//                .status(HttpStatus.OK)
//                .body("Теперь клиенты "+
//                        (station.getClientCommunication()?"могут ":"не могут ")+
//                        "присылать Вам сообщения");
//    }
}
