package com.ss.remont.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ss.remont.entity.Client;
import com.ss.remont.repository.ClientRepository;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChangeUserStatusController {
	
	@Autowired
	ClientRepository clientRepository;
	
	@RequestMapping(URIPath.ADMIN_SAVE_CLIENT)
	public Client saveClient(@RequestParam(name="id",required=false)Integer id,
			                 Model model){
		Client client = clientRepository.findOne(id);
		client.setLocked(!client.getLocked());
		clientRepository.save(client);
		return client;
	}
	
}
