package com.ss.remont.controller;

import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.Images;
import com.ss.remont.entity.ImagesType;
import com.ss.remont.repository.ImagesRepository;
import com.ss.remont.utils.ConsoleColor;
import com.ss.remont.storage.StorageFileNotFoundException;
import com.ss.remont.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by Tensky on 09.11.2016.
 */
@Controller
public class FileUploaderController {
	private final StorageService storageService;

    @Autowired
    CurrentUser currentUser;

    @Autowired
    ImagesRepository imagesRepository;

	@Autowired
	public FileUploaderController(StorageService storageService) {
		this.storageService = storageService;
	}

	@RequestMapping(value = URIPath.TEST_UPLOAD, method = RequestMethod.POST)
	public @ResponseBody String doUpload(@RequestParam("file") MultipartFile multipartFile, @PathVariable("path") String path) {
		return "Успешно загружено " + multipartFile.getSize() + " байт.";
	}

	@RequestMapping(value = URIPath.TEST, method = RequestMethod.POST)
	public @ResponseBody String test(@PathVariable("path") String path) {
		System.err.println(
				"[test.Line(" + Thread.currentThread().getStackTrace()[1].getLineNumber() + ")]: Test method called.");
		return "Test method called.";
	}

	@RequestMapping(value = URIPath.UPLOAD_FILE, method = RequestMethod.POST)
	@ResponseBody
	public List<Images> handleFileUpload(@RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                         @RequestParam("objectID") int objectID, @PathVariable("path") String path) {
		if (multipartFiles.size() < 1) {
			throw new StorageFileNotFoundException("There is no any file to upload.");
		}
		System.out.println("[handleFileUpload]: " + ConsoleColor.ANSI_GREEN + "User uploaded " + multipartFiles.size()
				+ " file(-s)." + ConsoleColor.ANSI_RESET);
        List<Images> images;
        try
        {
            images = storageService.store(currentUser.getUser(),multipartFiles, ImagesType.ICON);
        } catch (IOException e)
        {
            images=null;
        }
        return images;
	}

	@GetMapping(URIPath.FILES_FILENAME)
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable(name = "filename") String filename) {
		System.out.println("[serveFile]: SAMBADY WANTS FILE");
		filename = "pu.png";
		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}


    @Deprecated //Некорректное отображение в FireFox
	@GetMapping(URIPath.IMAGES)
	public @ResponseBody HttpEntity<byte[]> getImage(@RequestParam("filename") String filename) {
		File file = new File(filename);
		try {
			byte[] bytes = Files.readAllBytes(file.toPath());
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.IMAGE_JPEG);
			headers.setContentLength(bytes.length);
//			System.out.println("[getImage]: " + ConsoleColor.ANSI_PURPLE + "returning Image: " + filename
//					+ ConsoleColor.ANSI_RESET);
			return new HttpEntity<>(bytes, headers);
		} catch (IOException e) {
			System.err.println("[getImage.Line(" + Thread.currentThread().getStackTrace()[1].getLineNumber()
					+ ")]: no such file: " + filename);
			// e.printStackTrace();
			return null;
		}
	}

    /**
     * Получет изображение по пути <code>/image/${image.path{small/medium/large}}</code>
     * @return
     */
    @RequestMapping(value = "/img/upload-dir/{type}/{year}/{month}/{dayOfMonth}/{base}/{filename:.+}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getImage(@PathVariable("type")String type,
                                           @PathVariable("year") String year,
                                           @PathVariable("month") String month,
                                           @PathVariable("dayOfMonth") String dayOfMonth,
                                           @PathVariable("base") String base,
                                           @PathVariable("filename") String name)
    {
        String separator = "/";
        String filename = type + separator + year + separator + month + separator + dayOfMonth + separator + base + separator + name;
        Path imagePath = storageService.load(filename);
        byte[] image;
        try
        {
            image = Files.readAllBytes(imagePath);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.IMAGE_JPEG);
            return new ResponseEntity<byte[]>(image,httpHeaders, HttpStatus.OK);
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/image/{id}/{size}", method = RequestMethod.GET)
    @ResponseBody HttpEntity<byte[]> getImageFirefoxTest(@PathVariable("id")Integer id, @PathVariable("size") String size) throws IOException
    {
        Images images = imagesRepository.findOne(id);
        if(images==null)
            throw new StorageFileNotFoundException("no image found by id");
        String path;
        switch (size)
        {
            case "s":
            {
                path=images.getPathSmall();
                break;
            }
            case "m":
            {
                path=images.getPathMedium();
                break;
            }
            case "l":
            {
                path=images.getPathLarge();
                break;
            }
            default:
            {
                throw  new StorageFileNotFoundException("unknown size");
            }
        }
        File f = new File(path);
        byte[] image = Files.readAllBytes(f.toPath());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(image,httpHeaders, HttpStatus.OK);
    }

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException e) {
		return ResponseEntity.notFound().build();
	}
}
