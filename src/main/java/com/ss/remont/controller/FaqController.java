package com.ss.remont.controller;

import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.Faq;
import com.ss.remont.repository.FaqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Tensky on 30.12.2016.
 */
@Controller
public class FaqController {
	
	@Autowired
	FaqRepository faqRepository;
	
	@Autowired
	CurrentUser currentUser;

	@RequestMapping(URIPath.FAQ)
	public String faqPage(Model model) {
		List<Faq> faqs = faqRepository.findAll();
		model.addAttribute("faqs", faqs);
		return "faq/view";
	}

	@RequestMapping(URIPath.FAQ_EDIT)
	public String faqEditPage(Model model) {
		List<Faq> faqs = faqRepository.findAll();
		model.addAttribute("remove", true);
		model.addAttribute("faqs", faqs);
		return "faq/edit";
	}

	@RequestMapping(value = URIPath.FAQ_EDIT, method = RequestMethod.POST, params = "action=add")
	public String faqAdd(@RequestParam(value = "question", required = true) String question,
			@RequestParam(value = "answer", required = true) String answer) {
		faqRepository.save(new Faq(question, answer));
		return "redirect:/faq/edit";
	}

	@RequestMapping(value = URIPath.FAQ_EDIT_FAQ_ID, params = "action=delete")
	public String faqDelete(@PathVariable("faqId") String sFaqId) {
		Integer faqId = Integer.valueOf(sFaqId);
		faqRepository.delete(faqId);
		return "redirect:/faq/edit";
	}

}
