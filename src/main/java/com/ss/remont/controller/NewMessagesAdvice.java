package com.ss.remont.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.ss.remont.auth.CurrentUser;
import com.ss.remont.repository.MessageRepository;

@ControllerAdvice
public class NewMessagesAdvice {

	@Autowired
	private CurrentUser currentUser;

	@Autowired
	MessageRepository messageRepository;

	@ModelAttribute
	public void addNewMessagesSizeToModel(Model model) {
		if (currentUser.isAuthenticated()) {
			Integer newMessagesSize = messageRepository
					.countByRecipientIdAndRead(currentUser.getRegistrationAuth().getId(), false);
			model.addAttribute("newMessagesSize", newMessagesSize);
		}
	}
}
