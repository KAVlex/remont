package com.ss.remont.controller;

public class URIPath {
	
	public static final String PATH_SEPARATOR = "/";
	public static final String ALL_CHARACTERS = "*";
	public static final String ALL_DIRECTORIES = "**";
	public static final String PATH_ALL_CHARACTERS = PATH_SEPARATOR + ALL_CHARACTERS;
	public static final String PATH_ALL_DIRECTORIES = PATH_SEPARATOR + ALL_DIRECTORIES;
    public static final String REDIRECT = "redirect:";

	
	public static final String ACTIVE_USERS = "/activeUsers";
	public static final String TOPIC_ACTIVE = "/topic/active";
	
	
	public static final String AUTOSERVICE = "/autoservice";
	public static final String ADMIN = "/admin";
	public static final String ADMIN_OPEN_CONFIGURATION = "/admin/open-configuration"; 
	public static final String ADMIN_SAVE_CONFIGURATION = "/admin/save-configuration";
	public static final String ADMIN_REPORT_STATION_BALANCE = "/admin/report/station-balance";
	public static final String ADMIN_CLIENTS_CARD = "/admin/clients-card";
	public static final String ADMIN_SEARCH_CLIENT = "/admin/search-client";
	public static final String ADMIN_SAVE_CLIENT  = "/admin/save-client";
	public static final String ADMIN_SEARCH_STATION = "/admin/search-station";
	

	public static final String ADMIN_FEEDBACK  = "/admin/feedback";
	public static final String ADMIN_SHOW_FEEDBACK  = "/admin/show-feedback";
	public static final String ADMIN_ANSWER_FEEDBACK  = "/admin/answer-feedback";
	
	public static final String ADMIN_SEARCH_MARKS  = "/admin/search-marks";
	public static final String ADMIN_MENU_MARK  = "/admin/menu-mark";
	public static final String ADMIN_MENU_MODEL  = "/admin/menu-model";
	public static final String ADMIN_UPDATE_MARK  = "/admin/update-mark";
	public static final String ADMIN_DELETE_MARK  = "/admin/delete-mark";
	
	public static final String ADMIN_SEARCH_MODEL  = "/admin/search_models";
	public static final String ADMIN_UPDATE_MODEL  = "/admin/update-model";
	
	public static final String NEW_BID = "/new-bid";
	public static final String NEW_BID_STATION = "/new-bid/{station}";
	public static final String NEW_BID_GET_CAR_MARKS = "/new-bid/getCarMarks";
	public static final String NEW_BID_GET_CITIES= "/new-bid/getCities";
	public static final String NEW_BID_GET_CAR_MODELS_BY_MARK = "/new-bid/getCarModelsByMark";
	public static final String NEW_BID_SUBMIT = "/new-bid/submit";
	public static final String NEW_BID_CREATE = "/new-bid/createBid";
	
	public static final String FEEDBACK = "/feedback";
	public static final String FEEDBACK_ERR = "feedback/err";
	
	public static final String TEST_UPLOAD = "/testUpload";
	public static final String TEST = "/test";
	public static final String UPLOAD_FILE = "/upload-file";
	public static final String FILES = "/files";
	public static final String FILES_FILENAME = "/files/{filename}";
	public static final String IMAGES = "/images/";
	
	public static final String BASE = "/";
	public static final String GET_MODELS = "/getModels";
	
	public static final String JS = "/js/";
	
	public static final String MAP = "/map";
	public static final String MAP_GET_ALL_BALLOONS = "/map/getAllStationBalloons";
	public static final String MAP_SEND_FILTER = "/map/sendFilter";
	public static final String MAP_GET_FILTER = "/map/getFilter";
	
	public static final String CHAT = "/chat";
	public static final String QUEUE_MESSAGES = "/queue/messages";
	public static final String MESSAGES = "/messages";
	public static final String MESSAGE_MARK = "/message/mark";
	public static final String USER_QUEUE_MESSAGES = "/user/queue/messages";		//???
	
	public static final String MYCABINET = "/mycabinet";
	public static final String MYCABINET_EDIT_DISCOUNT = "/mycabinet/editDiscount";
	public static final String MYCABINET_ADD_DISCOUNT = "/mycabinet/addDiscount";
	public static final String MYCABINET_SAVE_MY_INFO = "/mycabinet/saveMyInfo";
    public static final String MYCABINET_DELETE_FROM_GALLERY ="/mycabinet/deleteFromGallery";
    public static final String MYCABINET_PRICE_LIST="/mycabinet/priceList";
    public static final String MYCABINET_CHANGE_ORDER_STATE="/mycabinet/changeOrderState";
    public static final String MYCABINET_GET_RESPONSES_ON_MAP = "/mycabinet/map/responses/{bidId}";
    public static final String MYCABINET_SHOW_MY_RECALL = "/mycabinet/showMyRecall";
    public static final String MYCABINET_CHANGE_AVATAR = "/mycabinet/changeAvatar";
    public static final String MYCABINET_ADD_STATION_DIRECTOR = MYCABINET+"/stationDirector/add";
	
	public static final String MYCARS = "/mycars";
	
	public static final String NEWS = "/news";
	public static final String NEWS_CREATE = "/news/create";
	public static final String NEWS_VIEW = "/news/view";
	public static final String NEWS_VIEW_ID = "/news/view/{id}";
    public static final String NEWS_NEXTPORTION = "/news/nextportion/{pageNumber}";
    public static final String NEWS_INDEX = "/news/index";
	
	public static final String STATION = "/station";
	public static final String STATION_ID = "/{stationID}";
	public static final String STATION_ORDER_CALL = "/order_call";
	public static final String STATION_ADD_ORDER_CALL = "/addOrderCall";
    public static final String STATION_ORDER_EMAIL= "/order_email";
    public static final String STATION_ADD_ORDER_EMAIL ="/addOrderEmail";
	
	public static final String STATIONS_CARD = "/stations-card";
	public static final String STATIONS_CARD_GET_PAGE = "/stations-card/get-page";
	public static final String STATIONS_CARD_OPEN_STATION = "/stations-card/open-station";
	public static final String STATIONS_CARD_CLOSE_STATION = "/stations-card/close-station";
	
	public static final String REGISTRATION = "/registration/";
	public static final String REGISTRATION_SAVE_USER = "/registration/saveUser";
	public static final String REGISTRATION_ACTIVATE_USER = "/registration/activateUser";
	public static final String REGISTRATION_REPAIR_PASSWORD = "/registration/repair-password";
	public static final String REGISTRATION_NEW_PASSWORD = "/registration/new-password";
	public static final String REGISTRATION_REGERROR = "/registration/regerror";
	public static final String REGISTRATION_WAIT_NOTIFICATION = "/registration/waitNotification";
	
	public static final String REPAIR_PASSWORD = "/repair-password";
	public static final String HELLO = "/hello";
	public static final String LOGIN = "/login";
	public static final String LOGOUT = "/logout";
	public static final String COMMON = "/common";
	public static final String MAIL = "/mail";
	public static final String WEBJARS = "/webjars";
	public static final String RESOURCES = "/resources";
	public static final String API = "/api";
	public static final String APP = "/app";
	
	public static final String RESPONSE_MESSAGES = "/response/messages";
	public static final String BID_RESPONSES = "/bid/responses";
	public static final String STATION_RESPONSES = "/station/responses";
	public static final String RESPONSE_ADD = "/response/add";
	
	public static final String DISCOUNTS = "/discounts";
	public static final String DISCOUNTS_STATION_ID = "/discounts/{stationId}";
	
	public static final String SEARCH = "/search";
	public static final String FAQ = "/faq";
	public static final String FAQ_EDIT = "/faq/edit";
	public static final String FAQ_EDIT_FAQ_ID = "/faq/edit/{faqId}";
	
	public static final String SMS_QUEUE = "/sms/queue";
	public static final String SMS_MARK = "/sms/mark";
	public static final String SMS_MARK_ID = "/sms/mark/{smsId}";
}
