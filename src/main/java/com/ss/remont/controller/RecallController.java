package com.ss.remont.controller;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.Bid;
import com.ss.remont.entity.Client;
import com.ss.remont.entity.Recall;
import com.ss.remont.entity.Station;
import com.ss.remont.repository.BidRepository;
import com.ss.remont.repository.ClientRepository;
import com.ss.remont.repository.RecallRepository;
import com.ss.remont.repository.StationRepository;
import com.ss.remont.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

/**
 * Created by Tensky on 23.12.2016.
 */
@Controller
public class RecallController {

	@Autowired
	private CurrentUser currentUser;

	@Autowired
	private RecallRepository recallRepository;

	@Autowired
	private StationRepository stationRepository;

	@Autowired
	private ClientRepository clientRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    BidRepository bidRepository;

    @Autowired
    NotificationService notificationService;

    private ResponseEntity<?> generateResponse(HttpStatus status, Object o) {
		return ResponseEntity.status(status).body(o);
	}


	@RequestMapping(value = "recall/add", method = RequestMethod.POST)
//	@ResponseBody
	public String rateStation(@RequestParam Map<String, String> requestParams) {
		AuthUser authUser;
		try {
			authUser = currentUser.getUser();
		} catch (Exception e) {
            notificationService.addErrorMessage("Неавторизированный пользователь не может изменять рейтинг станции");
//			return generateResponse(HttpStatus.FORBIDDEN,
//					"Неавторизированный пользователь не может изменять рейтинг станции");
            return URIPath.REDIRECT+URIPath.MYCABINET;
		}

        Integer bidId = Integer.valueOf(requestParams.getOrDefault("bidId", "-1"));
        if(bidId<0)
        {
            notificationService.addErrorMessage("Не удалось распознать идентификатор заявки");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
//            return generateResponse(HttpStatus.FORBIDDEN, "Не удалось распознать идентификатор заявки");

        Integer rating = Integer.valueOf(requestParams.getOrDefault("rating", "-1"));
        if(rating<1||rating>5)
        {
            notificationService.addErrorMessage("Невозможно определить рейтинг - "+rating);
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
//            return generateResponse(HttpStatus.FORBIDDEN,"Невозможно определить рейтинг - "+rating);

        String comment = requestParams.getOrDefault("comment", "");
        if(comment.equals(""))
        {
            notificationService.addErrorMessage("Комментарий не указан");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
//            return generateResponse(HttpStatus.FORBIDDEN, "Комментарий не указан");

        Bid bid = bidRepository.findOne(bidId);
        if(bid==null)
        {
            notificationService.addErrorMessage("Заявка #"+bidId+"не найдена в базе данных");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
        if(bid.getRecall()!=null)
        {
            notificationService.addErrorMessage("Вы уже оставляли отзыв по заявке #"+bid.getId());
        }
//            return generateResponse(HttpStatus.FORBIDDEN, "Заявка #"+bidId+"не найдена в базе данных");
        Station station = bid.getStation();
        if(station==null)
        {
            notificationService.addErrorMessage("Невозможно определить станцию по заявке #"+bidId);
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
//            return generateResponse(HttpStatus.FORBIDDEN,"Невозможно определить станцию по заявке #"+bidId);

        Client client = clientRepository.findOne(authUser.getId());
        if(client==null)
        {
            notificationService.addErrorMessage("Не удается определить клиента с идентификатором #"+authUser.getId());
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
//            return generateResponse(HttpStatus.FORBIDDEN, "Не удается определить клиента с идентификатором #"+authUser.getId());

        Recall recall = recallRepository.save(new Recall(client, station, bid ,rating, comment));
        if(recall==null)
        {
            notificationService.addErrorMessage("Невозможно сохранить отзыв в базе данных");
            return URIPath.REDIRECT+URIPath.MYCABINET;
        }
//            generateResponse(HttpStatus.FORBIDDEN,"Невозможно сохранить отзыв в базе данных");
        bid.setRecall(recall);
        bidRepository.save(bid);

        stationRepository.updateStationRating(station.getId());
        entityManager.clear();
        return URIPath.REDIRECT+URIPath.MYCABINET;
//        return generateResponse(HttpStatus.OK, "Рейтинг станции обновлен");
	}
}
