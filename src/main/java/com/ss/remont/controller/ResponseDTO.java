package com.ss.remont.controller;

import java.util.Date;

import com.ss.remont.entity.Response;

public class ResponseDTO {

	private Integer id;
	private Date time;
	private Double cost;
	private String comment;
	private String stationLogin;
	private String stationPhoto;
	private String stationName;
	private String stationAddress;
	private Integer stationRating;
	private String bidContent;
	private String clientLogin;
	private String clientPhoto;
	private String clientName;
	
	public ResponseDTO(Response response){
		setComment(response.getComment());
		setCost(response.getCost());
		setId(response.getId());
		setTime(response.getTime());
		setStationLogin(response.getStation().getPhone());		//<-- нужно как-то по другому
		setStationName(response.getStation().getName());
		if (response.getStation().getPhoto() != null)
			setStationPhoto(response.getStation().getPhoto().getPathSmall());
		setStationRating(response.getStation().getRating());
		setStationAddress(response.getStation().getAddress().getAddr());
		setBidContent(response.getBid().getContent());
		setClientLogin(response.getBid().getClient().getPhone());
		setClientName(response.getBid().getClient().getName());
		if (response.getBid().getClient().getPhoto() != null)
			setClientPhoto(response.getBid().getClient().getPhoto().getPathSmall());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getStationLogin() {
		return stationLogin;
	}

	public void setStationLogin(String stationLogin) {
		this.stationLogin = stationLogin;
	}

	public String getStationPhoto() {
		return stationPhoto;
	}

	public void setStationPhoto(String stationPhoto) {
		this.stationPhoto = stationPhoto;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getStationAddress() {
		return stationAddress;
	}

	public void setStationAddress(String stationAddress) {
		this.stationAddress = stationAddress;
	}

	public Integer getStationRating() {
		return stationRating;
	}

	public void setStationRating(Integer stationRating) {
		this.stationRating = stationRating;
	}

	public String getBidContent() {
		return bidContent;
	}

	public void setBidContent(String bidContent) {
		this.bidContent = bidContent;
	}

	public String getClientLogin() {
		return clientLogin;
	}

	public void setClientLogin(String clientLogin) {
		this.clientLogin = clientLogin;
	}

	public String getClientPhoto() {
		return clientPhoto;
	}

	public void setClientPhoto(String clientPhoto) {
		this.clientPhoto = clientPhoto;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

}
