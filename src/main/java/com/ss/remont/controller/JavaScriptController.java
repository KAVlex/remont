package com.ss.remont.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Tensky on 18.11.2016.
 * Контроллер, который позволяет возвращать JavaScript'ы
 */
@Controller
public class JavaScriptController
{
    /**
     * Путь до папки с JavaScript'ами
     */
    final String path = "..\\remont\\src\\main\\resources\\static\\common\\js\\";

    /**
     * Метод, возвращающий JavaScript по запросу
     * (чтобы не писать их каждый раз в тегах {@literal <script>***</script>})
     * <p><b/>Пример вызова в *.HTML:</p>
     * <pre>
     *  {@literal
     *  <html>
     *      <head>
     *          <!--ВЫЗОВ СКРИПТА-->
     *          <script src="/js/?script=map_init.js"/>
     *      </head>
     *      <body>
     *          ***
     *      </body>
     *  </html>
     * }
     * </pre>
     * @param script - название скрипта
     * @return указанный в запросе JavaScript
     * @throws FileNotFoundException Файл с указанным именем не найден
     */
    @RequestMapping(value = URIPath.JS, method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
    @ResponseBody
    public byte[] getScript(@RequestParam("script") String script) throws FileNotFoundException
    {
        script = path + script;
        //Если такой файл существует
        if (new File(script).exists())
        {
            try
            {
                //Пробуем считать из него все байты и вернуть их клиенту
                @SuppressWarnings("resource")
				FileInputStream fileInputStream = new FileInputStream(script);
                byte[] b = new byte[fileInputStream.available()];
                fileInputStream.read(b);
                return b;
            } catch (FileNotFoundException e)
            //Если файл не найден, чего не должно быть, т.к. проверяем выше, но все же
            {
                e.printStackTrace();
                System.err.println("[getScript.Line(" +
                        Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
                        ")]: Can't read file: " + script);
            } catch (IOException e)
            //Если из него нельзя считать байты
            {
                e.printStackTrace();
                System.err.println("[getScript.Line(" +
                        Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
                        ")]: Can't get available from: " + script);
            }
        }
        //Если файл не найден
        throw new FileNotFoundException("Can't find this JavaScript: " + script);

    }
}
