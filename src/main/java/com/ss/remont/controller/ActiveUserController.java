package com.ss.remont.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
public class ActiveUserController {
  
  private ActiveUserService activeUserService;

  @Autowired
  public ActiveUserController(ActiveUserService activeUserService) {
    this.activeUserService = activeUserService;
  }
  
  @MessageMapping(URIPath.ACTIVE_USERS)
  public void activeUsers(Message<Object> message) {
    Principal user = message.getHeaders().get(SimpMessageHeaderAccessor.USER_HEADER, Principal.class);
    activeUserService.mark(user.getName());
  }

}
