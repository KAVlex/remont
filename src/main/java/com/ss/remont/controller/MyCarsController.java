package com.ss.remont.controller;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.CurrentUser;
import com.ss.remont.auth.SimpleAuthUser;
import com.ss.remont.entity.*;
import com.ss.remont.repository.*;
import com.ss.remont.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.persistence.EntityManager;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * Created by Tensky on 06.12.2016.
 */
@Controller
public class MyCarsController
{
    @Autowired
    CarRepository carRepository;
    @Autowired
    SimpleAuthUserRepository simpleAuthUserRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    BidRepository bidRepository;
    @Autowired
    CarMarkRepository carMarkRepository;
    @Autowired
    CarModelRepository carModelRepository;
    @Autowired
    CarYearRepository carYearRepository;
    @Autowired
    CurrentUser currentUser;
    @Autowired
    NotificationService notificationService;

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<?> handleException(DataIntegrityViolationException e)
    {
        return ResponseEntity
//                .status(HttpStatus.I_AM_A_TEAPOT) лол
                .status(HttpStatus.FORBIDDEN)
                .body("Невозможно выполнить запрос, по причине:\n"
                        + e.getMostSpecificCause().toString());
    }

    @RequestMapping(URIPath.MYCARS)
    public String myCars(Model pageModel) throws FileNotFoundException
    {
        AuthUser authUser = currentUser.getRegistrationAuth();
        Client client = clientRepository.findOne(authUser.getId());
        if (client == null)
        {
            throw new FileNotFoundException("client not found");
        }
        List<Car> cars = client.getCars();
        List<CarMark> carMarks = (List<CarMark>) carMarkRepository.findAll();

        pageModel.addAttribute("cars", cars);
        pageModel.addAttribute("carMarks", carMarks);
        return "mycars";
    }

    @RequestMapping(value = URIPath.MYCARS, method = RequestMethod.POST, params = "action=edit")
    @ResponseBody
    public ResponseEntity<?> editCar(@RequestParam Map<String, String> requestParams)
    {
        AuthUser authUser = currentUser.getRegistrationAuth();
        Integer carID = Integer.valueOf(requestParams.getOrDefault("carID", "-1"));
        if (carID < 0)
        {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("wrong carID parameter: " + requestParams.get("carID"));
        }
//            return "wrong carID parameter: "+requestParams.get("carID");
        Car car = carRepository.findOne(carID);
        if (car == null)
        {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("car not found in database");
        }
        Client client = clientRepository.findOne(authUser.getId());
        List<Car> clientCars = client.getCars();
        String color = requestParams.getOrDefault("color", "");
        String regNumber = requestParams.getOrDefault("regNumber", "");
        String mileageString = requestParams.getOrDefault("mileage", "");
        Integer mileage = null;
        if (!mileageString.isEmpty())
            if (!Pattern.matches("[0-9]+", mileageString))
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Неверное число в поле 'Пробег'");
            else
                mileage = Integer.valueOf(mileageString);

        if (clientCars.contains(car))
        {
            if (!color.equals("")) car.setColor(color);
            if (!regNumber.equals("")) car.setRegNumber(regNumber);
            if (mileage != null) car.setMileage(mileage);
            carRepository.save(car);
//            carRepository.updateCar(car.getId(), car.getClient(), car.getMark(), car.getModel(), car.getYear(),
//                    (!color.equals("") ? color : car.getColor()),
//                    (!regNumber.equals("") ? regNumber : car.getRegNumber()));
//            entityManager.clear();
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Изменения сохранены");
        }
        else
        {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("no such car in " + authUser.getName() + "'s Cars list");
        }

    }

    @RequestMapping(value = URIPath.MYCARS, method = RequestMethod.POST, params = "action=delete")
    @ResponseBody
    public String deleteCar(@RequestParam Map<String, String> requestParams)
    {
        AuthUser authUser = currentUser.getRegistrationAuth();
        Car car = carRepository.findOne(Integer.parseInt(requestParams.get("carID")));
        Client client = clientRepository.findOne(authUser.getId());
        List<Car> clientCars = client.getCars();
        if (clientCars.contains(car))
        {
            carRepository.nullClient(car.getId());
            return "done";
        }
        return "no such car in " + authUser.getName() + "'s Cars list";
    }

    @RequestMapping(value = URIPath.MYCARS, method = RequestMethod.POST, params = "action=add")
//    @ResponseBody
    public ResponseEntity<?> addCar(@RequestParam Map<String, String> requestParam, Model model)
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SimpleAuthUser simpleAuthUser = (SimpleAuthUser) authentication.getPrincipal();

        Integer carMarkID = Integer.parseInt(requestParam.get("carMarkID"));
        Integer carModelID = Integer.parseInt(requestParam.get("carModelID"));
        Integer year = Integer.parseInt(requestParam.get("year"));
        String color = requestParam.get("color");
        String regNumber = requestParam.get("regNumber");
        String mileageString = requestParam.getOrDefault("mileage", "");
        Integer mileage = null;
        if (!mileageString.isEmpty())
            if (Pattern.matches("[0-9]+", mileageString))
                mileage = Integer.valueOf(mileageString);
            else
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Неверное число в поле 'Пробег'");


        Car car = carRepository.findByRegNumber(regNumber);
        CarMark carMark = carMarkRepository.findOne(carMarkID);
        CarModel carModel = carModelRepository.findOne(carModelID);
        CarYear carYear = carYearRepository.findByModelAndYear(carModel.getId(), year);
        Client client = clientRepository.findOne(simpleAuthUser.getAuthUser().getId());
        if (carYear == null)
        {
            carYear = new CarYear(year, carModel);
            carYearRepository.save(carYear);
            carYear = carYearRepository.findByModelAndYear(carModel.getId(), year);
        }
        List<Car> userCars = carRepository.findUserCars(client.getId());
        if (car == null)
        {
            car = new Car(carMark,carModel,carYear,color,regNumber,mileage,client);
            carRepository.save(car);
            model.addAttribute("cars", userCars);

//            return "Транспортное средство с номером "+regNumber+" закреплено за Вами.";
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(car);
        }
        else
        {
            if (car.getClient() == null)
            {
                Integer id = car.getId();
                car = new Car(carMark, carModel, carYear, color, regNumber, mileage, client);
                car.setId(id);
                carRepository.save(car);
//                carRepository.updateCar(car.getId(), client, carMark, carModel, carYear, color, regNumber);
//                entityManager.clear();
//                car = carRepository.findOne(car.getId());
                return ResponseEntity.status(HttpStatus.OK).body(car);
            }
            else
            {
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body("Автомобиль с таким номером принадлежит другому пользователю. " +
                                "Если это действительно Ваш автомобиль, " +
                                "напишите администрации для решения данной ситауации.");
            }
        }
    }
}
