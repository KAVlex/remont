package com.ss.remont.controller;

import java.util.*;

import com.ss.remont.auth.CurrentUser;
import com.ss.remont.entity.*;
import com.ss.remont.repository.*;
import com.ss.remont.utils.Sublister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {
	
	@Autowired
	private CarMarkRepository carMarkRepository;
	
	@Autowired
	private CarModelRepository carModelRepository;
	
	@Autowired
	private RepairTypeRepository repairTypeRepository;
	
	@Autowired
	private StationRepository stationRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private BidRepository bidRepository;
	
	@Autowired
	private DiscountRepository discountRepository;
	
	@Autowired
	private NewsRepository newsRepository;
	
	@Autowired
	private CurrentUser currentUser;

    @Autowired
    CityRepository cityRepository;
	
	@RequestMapping(URIPath.GET_MODELS)
	public @ResponseBody List<CarModel> getModelsByMark(@RequestParam Map<String, String> map) {
		String markID = map.getOrDefault("markID", "");
		if (markID.equals("") || markID == null) {
			return null;
		}
		List<CarModel> carModels = carModelRepository.findCarModelByMarkId(Integer.parseInt(markID));
		return carModels;
	}

	@RequestMapping(URIPath.BASE)
	public String testDesign(Model model, @RequestParam(required = false) RedirectAttributes redirectAttributes) {
		List<CarMark> carMarks = (List<CarMark>) carMarkRepository.findAll();
		ArrayList<Integer> years = new ArrayList<>();
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		for (int i = 1950; i < currentYear; i++) {
			years.add(i);
		}
		
		Car car = addLastCarAttributes(model);
		List<CarModel> carModels = new ArrayList<CarModel>(); 
		if (car != null){
			carModels = carModelRepository.findCarModelByMarkId(car.getMark().getId());	
		}
		
		List<RepairType> repairTypes = (List<RepairType>) repairTypeRepository.findAll();

		Integer count_stations = Math.toIntExact(stationRepository.count());
		Integer count_clients = Math.toIntExact(clientRepository.count());
		Integer count_bids = Math.toIntExact(bidRepository.count());
		List<Bid> last10Bids = bidRepository.findTop10ByOrderByIdDesc();
		List<News> news = newsRepository.findTop4ByOrderByIdDesc();
//		List<Discount> discounts = discountRepository.getDiscountsByStatus(DiscountStatus.ACTIVE);
        List<Discount> discounts = discountRepository.getActiveDiscounts();
        List<News> newsRow1 = Sublister.sublistThis(news, 2, 0);
		List<News> newsRow2 = Sublister.sublistThis(news, 2, 1);
		model.addAttribute("carMarks", carMarks);
		model.addAttribute("years", years);
		model.addAttribute("carModels", carModels);
		model.addAttribute("repairTypes", repairTypes);
		model.addAttribute("count_stations", count_stations);
		model.addAttribute("count_clients", count_clients);
		model.addAttribute("count_bids", count_bids);
		model.addAttribute("last10Bids", last10Bids);
		model.addAttribute("newsRow1", newsRow1);
		model.addAttribute("newsRow2", newsRow2);
		model.addAttribute("discounts", discounts);
        model.addAttribute("cities",cityRepository.findAll());
        model.addAttribute("stationCategories", StationCategoryEnum.values());
		return "index";
	}
	
	private Car addLastCarAttributes(Model model){
		Car lastCar = null;
		if (currentUser.isAuthenticated() && currentUser.isClient()) {
			Client client = (Client) currentUser.getUser();
			if (client.getCars() != null && client.getCars().size() > 0) {
				lastCar = client.getCars().get(0);
				model.addAttribute("markId", lastCar.getMark().getId());
				model.addAttribute("modelId", lastCar.getModel().getId());
				model.addAttribute("yearId", lastCar.getYear().getYear());
			}
		}		
		return lastCar;
	}

}