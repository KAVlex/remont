package com.ss.remont.mail;

import java.util.Map;

import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.ss.remont.RemontApplicationContext;
import com.ss.remont.config.SiteProperties;

import freemarker.template.Configuration;
import freemarker.template.Template;


public abstract class Mail implements  MimeMessagePreparator {

    protected SiteProperties siteProperties; 
	
    protected Configuration configuration;
    
	public Mail() {
		this.siteProperties = (SiteProperties) RemontApplicationContext.getBean(SiteProperties.class);
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
	
	public abstract String getTemplateName();
	
    public String createTextFromTemplate(Map<String, Object> model){
        try{
        	System.out.println("******************************");
        	
        	Template template = configuration.getTemplate(getTemplateName(), "UTF-8");
        	template.setOutputEncoding("UTF-8");
        	String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        	return text;
        }catch(Exception e){
            System.out.println("Exception occured while processing fmtemplate1:" + e.getMessage());
        }
        return "";
    }
	 
}
