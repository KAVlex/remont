package com.ss.remont.mail.template;
import com.ss.remont.auth.AuthUser;
import com.ss.remont.entity.Message;
import com.ss.remont.entity.Response;
import com.ss.remont.mail.Mail;
import com.ss.remont.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Tensky on 16.03.2017.
 */
public class StatusChangedTemplate extends Mail
{
    @Autowired
    MailService mailService;


    private static final String TEMPLATE_NAME = "bid-status-changed.ftl";

    private Message message;

    @Override
    public String getTemplateName()
    {
        return TEMPLATE_NAME;
    }

    public StatusChangedTemplate(Message message) {
        super();
        this.message = message;
    }

    @Override
    public void prepare(MimeMessage mimeMessage) throws Exception
    {
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        helper.setSubject(siteProperties.getName());
        helper.setTo(message.getRecipient().getEmail());
        Map<String, Object> model = new HashMap<>();
        model.put("message", message.getMessage());
        model.put("user", message.getRecipient());
        model.put("bid", message.getResponse().getBid());
        model.put("link", siteProperties.getUrl());
        String text = createTextFromTemplate(model);
        helper.setText(text, true);
    }

}
