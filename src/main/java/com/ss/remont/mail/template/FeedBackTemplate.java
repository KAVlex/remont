package com.ss.remont.mail.template;

import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

import com.ss.remont.entity.Feedback;
import com.ss.remont.mail.Mail;

public class FeedBackTemplate extends Mail {

	private static final String TEMPLATE_NAME="feedback-admin.ftl";
	
	private Feedback feedback;
	
	private String answer;
	
	public FeedBackTemplate(Feedback feedback,String answer){
		this.feedback = feedback;
		this.answer   = answer;
	}
	
	@Override
	public void prepare(MimeMessage mimeMessage) throws Exception {
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true,"UTF-8") ;
        helper.setSubject("Ремонт на ваших условиях");
        helper.setTo(feedback.getEmail());
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("feedback", feedback);
        model.put("answer", answer);        
        String text = createTextFromTemplate(model);
        System.out.println("Template content : " + text);
        helper.setText(text, true);
	}

	@Override
	public String getTemplateName() {
		return TEMPLATE_NAME;
	}

}
