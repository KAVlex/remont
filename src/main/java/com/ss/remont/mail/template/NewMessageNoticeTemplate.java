package com.ss.remont.mail.template;

import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

import com.ss.remont.entity.Message;
import com.ss.remont.mail.Mail;

public class NewMessageNoticeTemplate extends Mail {

	private static final String TEMPLATE_NAME = "new-message.ftl";

	private Message message;

	public NewMessageNoticeTemplate(Message message) {
		super();
		this.message = message;
	}

	@Override
	public void prepare(MimeMessage mimeMessage) throws Exception {
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
		helper.setSubject(siteProperties.getName());
		helper.setTo(message.getRecipient().getEmail());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("message", message.getMessage());
		model.put("user", message.getRecipient());
		model.put("bid", message.getResponse().getBid());
		model.put("link", siteProperties.getUrl());
		String text = createTextFromTemplate(model);
		helper.setText(text, true);
	}

	@Override
	public String getTemplateName() {
		return TEMPLATE_NAME;
	}

}
