package com.ss.remont.mail.template;


import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

import com.ss.remont.mail.Confirm;
import com.ss.remont.mail.Mail;

public class RegistrationTemplate extends Mail {
    	
	private static final String TEMPLATE_NAME="confirm.ftl";
	
	private Confirm confirm;
	
	public  RegistrationTemplate(Confirm confirm){
		this.confirm = confirm;
	}
	
	
	@Override
	public void prepare(MimeMessage mimeMessage) throws Exception {
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true,"UTF-8");
        helper.setSubject("Ремонт на ваших условиях");
        helper.setTo(confirm.getEmail());
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("link", confirm.getActivationLink());
        model.put("password", confirm.getPassword());
        model.put("site", siteProperties);
        String text = createTextFromTemplate(model);
        System.out.println("Template content : " + text);
        helper.setText(text, true);
	}
	
	@Override
	public String getTemplateName() {
		return TEMPLATE_NAME;
	}
	
    
    
}
