package com.ss.remont.utils;

import java.security.MessageDigest;

import sun.misc.BASE64Encoder;

public class DigestUtils {

	public static String md5(String text){
		String hex = "";
		try{
			byte[] bytesOfMessage = text.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);		
			BASE64Encoder encode = new BASE64Encoder();
			hex = encode.encode(thedigest);
		}catch (Exception e) {
			e.printStackTrace();
		}
        return hex;
	} 
}
