package com.ss.remont.utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Tensky on 22.12.2016.
 */
public class Sublister
{
    /**
     * Получает строку элементов исходного списка <code>thisList</code>. Например, если нужно, чтобы
     * разбить список из 6 элементов на 2 списка (строки), то передается исходный список <code>thisList</code>,
     * количество строк <code>numberOfRows</code>, на которое разбивается этот список разбивается и
     * порядковый номер строки необходимой строки <code>currentRowIndex</code>. При нехватке элементов
     * (например для последней строки, когда в строке должно быть 4 элемента, а в списке осталось только 2) дополняет
     * список <code>null</code>'ами до тех пор, пока размерность возвращаемого списка не совпадет с количеством
     * элементов в строке.
     * @param thisList исходный список
     * @param numberOfRows количество строк (списков), на который список необходимо разбить
     * @param currentRowIndex порядковый номер необходимой строки (начиная с 0)
     * @param <T> Тип списка
     * @return строку исходного списка
     */
    public static <T> List<T> sublistThis(final List<T> thisList, int numberOfRows, int currentRowIndex){
        int originalSize = thisList.size();
        int odd = (originalSize%2==0 ? 0:1);
        int inOneRow = originalSize/numberOfRows+odd;
        int startIndex=inOneRow*currentRowIndex;
        int lastIndex=startIndex+inOneRow;
        ArrayList<T> sublist = new ArrayList<T>(inOneRow);
        try
        {
            for(int i=startIndex;i<lastIndex;i++){
                sublist.add(thisList.get(i));
            }
        }catch (IndexOutOfBoundsException e){
            while(sublist.size()<inOneRow){
                sublist.add(null);
            }
        }
        return sublist;
    }

}
