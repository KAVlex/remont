package com.ss.remont.utils;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Tensky on 15.11.2016.
 */
public class CurrentDate
{
    int year=0;
    int month=0;
    int day=0;
    String[] monthNames = {"January","February",
            "March","April","May",
            "June","July","August",
            "September","October","November",
            "December"};

    /**
     * Возвращает текущий год
     * @return
     */
    public int getYear()
    {
        return this.year;
    }

    /**
     * Возвращает номер текущего месяца
     */
    public int getMonthNumber()
    {
        return this.month+1;
    }

    /**
     * Возвращает название текущего месяца
     * @return
     */
    public String getMonthName()
    {
        return this.monthNames[this.month];
    }

    /**
     * Возвращает текущий день месяца
     * @return
     */
    public int getDayOfMonth()
    {
        return this.day;
    }

    /**
     * Заполняет поля <code>year</code>,<code>month</code>,<code>day</code> значениями, соответствующими
     * году, месяцу, дню на момент вызова
     */
    public CurrentDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
        this.year = calendar.get(Calendar.YEAR);
        this.month = calendar.get(Calendar.MONTH);
        this.day = calendar.get(Calendar.DAY_OF_MONTH);
    }

}
