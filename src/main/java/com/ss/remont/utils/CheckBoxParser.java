package com.ss.remont.utils;

/**
 * Created by Tensky on 20.12.2016.
 */
public class CheckBoxParser {

	static public boolean parseCheckBoxValue(String val) {

		if (val == null) {
			val = "off";
		}
		switch (val) {
		case "on":
			return true;
		default:
			return false;
		}
	}

}
