package com.ss.remont.utils;

import java.io.Serializable;

import org.springframework.data.repository.Repository;

public abstract interface ReadOnlyRepository<T, ID extends Serializable> extends Repository<T, ID> {

	T findOne(ID id);

	Iterable<T> findAll();
}
