package com.ss.remont.storage;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.entity.Images;
import com.ss.remont.entity.ImagesType;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface StorageService {

    void init();

//    String[] store(List<MultipartFile> files, ImagesType imagesType, int objectID);

    List<Images> store(AuthUser authUser, List<MultipartFile> multipartFiles, ImagesType imagesType) throws IOException;
    Images store(AuthUser authUser, MultipartFile multipartFile, ImagesType imagesType) throws IOException;

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteImageFromDisk(Images image);

    void deleteAll();

//	Stream<Path> loadAll();

}
