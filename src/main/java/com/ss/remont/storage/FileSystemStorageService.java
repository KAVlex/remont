package com.ss.remont.storage;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.config.ImageConverterProperties;
import com.ss.remont.config.StorageProperties;
import com.ss.remont.entity.Images;
import com.ss.remont.entity.ImagesType;
import com.ss.remont.utils.ConsoleColor;
import com.ss.remont.utils.CurrentDate;
import com.ss.remont.repository.ImagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class FileSystemStorageService implements StorageService
{

    private final Path rootLocation;

    @Autowired
    private ImagesRepository imagesRepository;

    @Autowired
    private ImageConverterProperties converterSettings;

    @Autowired
    public FileSystemStorageService(StorageProperties properties)
    {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public Path load(String filename)
    {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename)
    {
        try
        {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable())
            {
                return resource;
            }
            else
            {
                throw new StorageFileNotFoundException("Не возможно прочитать файл: " + filename);
            }
        } catch (MalformedURLException e)
        {
            throw new StorageFileNotFoundException("Не возможно прочитать файл: " + filename, e);
        }
    }

    @Override
    public void deleteAll()
    {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init()
    {
        try
        {
            Files.createDirectory(rootLocation);
        } catch (IOException e)
        {
            throw new StorageException("Невозможно создать хранилище", e);
        }
    }

    /**
     * Для каждого переданного изображения выполняет {@link #store(AuthUser, MultipartFile, ImagesType)}
     * @param authUser {@link AuthUser владелец файла}
     * @param multipartFiles изображение, переданное владельцем из браузера
     * @param imagesType {@link ImagesType тип изображения}
     * @return Список объектов класса {@link Images}
     * @throws IOException
     * @see #store(AuthUser, MultipartFile, ImagesType)
     */
    public List<Images> store(AuthUser authUser, List<MultipartFile> multipartFiles, ImagesType imagesType) throws IOException
    {
        //Если файлов нет
        if (multipartFiles.size() < 1)
        {
            throw new StorageException("No files to save");
        }

        //Обрабатывает и сохраняет в БД каждый файл
        ArrayList<Images> images = new ArrayList<>();
        for (MultipartFile multipartFile :
                multipartFiles)
        {
            images.add(store(authUser, multipartFile, imagesType));
        }
        return images;
    }

    /**
     * Выполняет сохранение отдельного изображения, его конвертацию и последующее удаление оригинального изображения
     *
     * @param authUser {@link AuthUser владелец изображение}
     * @param multipartFile изображение, переданное владельцем из браузера
     * @param imagesType {@link ImagesType тип изображения}
     * @return Объект класса {@link Images}
     * @throws IOException не удалось получить доступ к входному потоку <code>multipartFile</code>
     */
    public Images store(AuthUser authUser, MultipartFile multipartFile, ImagesType imagesType) throws IOException
    {
        if (multipartFile == null)
        {
            throw new StorageException("No file to save");
        }

        String separator = "\\";
        CurrentDate currentDate = new CurrentDate();
        String name = UUID.randomUUID().toString();

        //Куда сохранять (D:/comProjects/remontSVN/upload-dir/2017/01/12)
        //Абсолютный путь нужен для того, чтобы запустить magick (конвертор) из cmd
        String saveDirectory = this.rootLocation.toAbsolutePath().toString() + separator
                + imagesType.toString() + separator
                + currentDate.getYear() + separator
                + currentDate.getMonthName() + separator
                + currentDate.getDayOfMonth() + separator
                + name;

        //Создает и проверяет наличие папок
        if (!new File(saveDirectory).mkdirs())
        {
            if (!new File(saveDirectory).exists())
            {
                throw new StorageException("Can't create directory [" + saveDirectory + "]");
            }
        }

        //Было "/upload-dir/2017/01/12/{name}" стало "/upload-dir/2017/01/12/{name}/"
        saveDirectory += separator;

        //Абсолютные пути для каждого из новых файлов
        String absolutePathSmall = saveDirectory + name + converterSettings.getPostfixSmall();
        String absolutePathMedium = saveDirectory + name + converterSettings.getPostfixMedium();
        String absolutePathLarge = saveDirectory + name + converterSettings.getPostfixLarge();

        //Запоминаем имя загружаемого файла, заменяя все пробелы на '_'
        String formattedName = multipartFile.getOriginalFilename().replace(" ", "_");

        //Загружаем файл и присваиваем ему отформатированное выше имя
        Files.copy(multipartFile.getInputStream(), this.rootLocation.resolve(formattedName));

        //Здесь формируются и исполняются команды для cmd
        String command =
                "magick convert "
                        //Что конверируем
                        + this.rootLocation.toAbsolutePath().toString() + separator + formattedName
                        //Качество
                        + " -quality " + converterSettings.getImageQuality()
                        //Новый размер
                        + " -resize " + converterSettings.getImageSizeSmall()
                        //Куда сохраняем
                        + " " + absolutePathSmall;

        //Запускает командную строчку с этой командой и, прежде чем выполнять следующую команду,
        //ждет, пока не выполнится процесс конвертирования
        convertThis(command);
        command = "magick convert " + this.rootLocation.toAbsolutePath().toString() + separator + formattedName
                + " -quality " + converterSettings.getImageQuality()
                + " -resize " + converterSettings.getImageSizeMedium()
                + " " + absolutePathMedium;
        convertThis(command);

        command = "magick convert " + this.rootLocation.toAbsolutePath().toString() + separator + formattedName
                + " -quality " + converterSettings.getImageQuality()
                + " -resize " + converterSettings.getImageSizeLarge()
                + " " + absolutePathLarge;
        convertThis(command);

        //Удаляет оригинальный файл, чтобы не занимать место на диске
        if (!new File(this.rootLocation.resolve(multipartFile.getOriginalFilename()).toString()).delete())
        {
            System.err.println("[store.Line(" +
                    Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
                    ")]: Nothing to delete");
        }

        //У нас уже имеются абсолютные пути к файлам, но это небезопасно, поэтому необходимо убрать часть строки
        //до "upload-dir" (this.rootLocation)
        //Для этого получаем положение this.rootLocation в строке с абсолютным путем и формируем новую строку, начиная
        //с первого символа this.rootLocation и до конца строки, содержащей абсолютный путь
        String securedPathBase = saveDirectory.substring(saveDirectory.indexOf(this.rootLocation.toString()), saveDirectory.length() - 1);
        String securedPathSmall = absolutePathSmall.substring(absolutePathSmall.indexOf(this.rootLocation.toString()));
        String securedPathMedium = absolutePathMedium.substring(absolutePathMedium.indexOf(this.rootLocation.toString()));
        String securedPathLarge = absolutePathLarge.substring(absolutePathLarge.indexOf(this.rootLocation.toString()));

        //Если удалось сохранить эти файлы, то
        if(new File(absolutePathSmall).exists()
            && new File(absolutePathMedium).exists()
            && new File(absolutePathLarge).exists())
        {
            //Возвращаем объект класса Images
            return new Images(authUser,name,imagesType,securedPathBase,securedPathSmall,securedPathMedium,securedPathLarge);
        }
        else{
            //Иначе удаляем все файлы, которые удалось сохранить
            new File(absolutePathSmall).delete();
            new File(absolutePathMedium).delete();
            new File(absolutePathSmall).delete();

            //и возвращаем null
            return null;
        }
    }

    public void deleteImageFromDisk(Images image)
    {
        File file = new File(image.getPathBase());
        deleteAllFilesInDirectory(file);
        return;
//        return file.delete();
    }

    private void deleteAllFilesInDirectory(File file)
    {
        if(!file.exists())
        {
            System.out.println("[deleteAllFilesInDirectory]: Directory ["
                    + file.getAbsolutePath() +
                    "] is not exists. Nothing to delete.");
            return;
        }
        for (File f :
                file.listFiles())
        {
            if (f.isDirectory())
                deleteAllFilesInDirectory(f);
            else f.delete();
        }
    }

    private static boolean convertThis(String command)
    {
        Process exec = null;
        try
        {
            exec = Runtime.getRuntime().exec(command);
            while (exec.isAlive())
            {
                try
                {
                    Thread.sleep(10);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                    System.err.println("[convertThis.Line(" +
                            Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
                            ")]: Can't do [Thread.sleep(10)].");
                }
            }
            return true;
        } catch (IOException e)
        {
            e.printStackTrace();
            System.err.println("[convertThis.Line(" +
                    Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
                    ")]: Can't run this command: " + command);
            return false;
        }
    }

    /**
     * Проверяет наличие файла по указанному пути
     *
     * @param path путь
     * @return <code>true</code> - файл существует;
     * <code>false</code> - не удалось найти файл
     */
    @SuppressWarnings("unused")
    private static boolean isImageCreated(String path)
    {
        if (new File(path).exists())
        {
            System.out.println("[isImageCreated]: " + "File [" + ConsoleColor.ANSI_GREEN + path + ConsoleColor.ANSI_RESET + "] exists.");
            return true;
        }
        else
        {
            System.err.println("[isImageCreated.Line(" +
                    Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
                    ")]: File [" + path + "] not found.");
            return false;
        }
    }


//        if (files.size() < 1)
//        {
//            throw new StorageException("No files to convert");
//        }
//        String[] returningPaths = new String[files.size()];
//        CurrentDate currentDate = new CurrentDate();
//        String separator = "/";
//        String saveDirectory = this.rootLocation.toString() + separator +
//                imagesType.toString() + separator +
//                objectID + separator +
//                currentDate.getYear() + separator +
//                currentDate.getMonthName() + separator +
//                currentDate.getDayOfMonth();
//        //Создание директории типа upload-dir/AVATAR/666/2016/November/15
//        if (!new File(saveDirectory).mkdirs())
//        {
//            if (!new File(saveDirectory).exists())
//            {
//                throw new StorageException("Can't create directory: " + saveDirectory);
//            }
//        }
//        saveDirectory += separator;
//        int i = 0;
//        //Обработка каждого файла
//        for (MultipartFile file : files)
//        {
//            //Если файл "пуст", то ошибка
//            if (file.isEmpty())
//            {
//                System.err.println("[store.Line(" +
//                        Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
//                        ")]: Current file is empty.");
//                break;
//            }
//
//
//            String uuid = UUID.randomUUID().toString().replace("-", "");
//            String newDirectory = saveDirectory + uuid;
//            if (!new File(newDirectory).mkdir())
//            {
//                if (!new File(newDirectory).exists())
//                {
//                    System.err.println("[store.Line(" +
//                            Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
//                            ")]: Can't create directory named [" + uuid + "] in [" + saveDirectory + "].");
//                }
//            }
//            String formattedName = file.getOriginalFilename().replace(" ", "_");
//            try
//            {
//                Files.copy(file.getInputStream(), this.rootLocation.resolve(formattedName));
//            } catch (IOException e)
//            {
//                System.err.println("[store.Line(" +
//                        Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
//                        ")]: Can't copy new file from " + file.getOriginalFilename() + "'s stream to " + this.rootLocation + separator + formattedName);
//            }
//            returningPaths[i] = convertAndSave(formattedName, newDirectory, uuid, objectID, imagesType);
//            i++;
//            if (new File(this.rootLocation + separator + formattedName).delete())
//            {
//                System.out.println("[store]: " + ConsoleColor.ANSI_PURPLE + "Original file [" + formattedName + "] deleted" + ConsoleColor.ANSI_RESET);
//            }
//            else
//            {
//                System.err.println("[store.Line(" +
//                        Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
//                        ")]: Can't delete file [" + formattedName + "]");
//            }
//        }
//        return returningPaths;
//    }

//    private String convertAndSave(String originalFileName, String directory, String filename, int ObjectID, ImagesType imagesType)
//    {
//        String command;
//        String from = this.rootLocation.toString() + "/" + originalFileName;
//        String lastChar = directory.substring(directory.length() - 1);
//        if (!lastChar.equals("/"))
//        {
//            directory += "/";
//        }
//        String[] savePaths = {directory + filename + converterSettings.getPostfixSmall(), directory + filename + converterSettings.getPostfixMedium(), directory + filename + converterSettings.getPostfixLarge()};
//        String[] sizes = {converterSettings.getImageSizeSmall(), converterSettings.getImageSizeMedium(), converterSettings.getImageSizeLarge()};
//        boolean fail = false;
//        for (int i = 0; i < savePaths.length; i++)
//        {
//            command = "magick convert " + from +
//                    " -quality "+converterSettings.getImageQuality()+" " +
//                    "-resize " + sizes[i] + " " + savePaths[i];
//            System.out.println("[convertAndSave]: Running command: " + ConsoleColor.ANSI_CYAN + command + ConsoleColor.ANSI_RESET);
//            if (!convertThis(command))
//            {
//                fail = true;
//                break;
//            }
//            Images images = new Images();
//            images.setObjectID(ObjectID);
//            images.setPath(savePaths[i]);
//            images.setType(imagesType.toString());
//            imagesRepository.save(images);
//        }
//        return fail ? null : savePaths[0];
//    }
//    /**
//     * Конвертрует, сохраняет изображение, делает запись в базе данных
//     * @param directory директория с только что загруженным изображением
//     * @param newDirectory директория, куда помещать сконвертированные изображение
//     * @param fileName имя файла
//     * @param objectID
//     * @param imagesType
//     * @return
//     */
//    private String convertAndSave(String directory, String newDirectory, String fileName,String newFileName, int objectID, ImagesType imagesType)
//    {
//        //600x500
//        //250х250
//        //50х50
//        directory += "/";
////        String uuid = UUID.randomUUID().toString().replace("-", "");
//
////        String newFileName = UUID.randomUUID().toString().replace("-", "");
//        String command;
//        String[] savePaths = {newFileName + largePostfix, newFileName + mediumPostfix, newFileName + smallPostfix};
//
//        command = "magick convert " + directory + fileName + " -quality 90 -resize 600x500! " + newDirectory + savePaths[0];
//        System.out.println("[convertAndSave]: Running command: " + ConsoleColor.ANSI_CYAN + command + ConsoleColor.ANSI_RESET);
//        System.out.println((convertThis(command)) ? "[convertAndSave]: Process Done." : "[convertAndSave]: Process Fail.");
//        if (!isImageCreated(newDirectory + savePaths[0]))
//        {
//            return null;
//        }
//
//        command = "magick convert " + directory + fileName + " -quality 90 -resize 250x250! " + newDirectory + savePaths[1];
//        System.out.println("[convertAndSave]: Running command: " + ConsoleColor.ANSI_CYAN + command + ConsoleColor.ANSI_RESET);
//        System.out.println((convertThis(command)) ? "[convertAndSave]: Done." : "[convertAndSave]: Fail.");
//        if (!isImageCreated(newDirectory + savePaths[1]))
//        {
//            return null;
//        }
//
//        command = "magick convert " + directory + fileName + " -quality 90 -resize 50x50! " + newDirectory + savePaths[2];
//        System.out.println("[convertAndSave]: Running command: " + ConsoleColor.ANSI_CYAN + command + ConsoleColor.ANSI_RESET);
//        System.out.println((convertThis(command)) ? "[convertAndSave]: Done." : "[convertAndSave]: Fail.");
//        if (!isImageCreated(newDirectory + savePaths[2]))
//        {
//            return null;
//        }
//
//        File originalImage = new File(directory + fileName);
//        if (originalImage.delete())
//        {
//            System.out.println("[convertAndSave]: Original image: " + ConsoleColor.ANSI_PURPLE + directory + fileName + ConsoleColor.ANSI_RESET + " deleted.");
//        }
//        else
//        {
//            System.err.println("[convertAndSave.Line(" +
//                    Thread.currentThread().getStackTrace()[1].getLineNumber() +//Error line number
//                    ")]: Can't delete original image: " + directory + fileName + ".");
//        }
//
//        for (String path : savePaths)
//        {
//            Images currentImage = new Images();
//            currentImage.setObjectID(objectID);
//            currentImage.setPath(newDirectory+path);
//            currentImage.setType(imagesType.toString());
//            imagesRepository.save(currentImage);
//        }
//
//        newFileName += smallPostfix;
//        return newDirectory + newFileName;
//    }


}
