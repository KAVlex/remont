package com.ss.remont.repository;

import com.ss.remont.entity.OrderCall;
import com.ss.remont.entity.Station;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Tensky on 15.12.2016.
 */
@RepositoryRestResource
public interface OrderCallRepository extends CrudRepository<OrderCall,Integer>
{
    @Query(value = "SELECT oc FROM OrderCall oc where oc.station.id=:id")
    List<OrderCall> findStationOrderCalls(@Param("id")Integer stationID);

    @Query("select oc from OrderCall oc where oc.station=:station")
    List<OrderCall> findStationOrderCalls(@Param("station")Station station);

    @Query("select oc from OrderCall oc where oc.station.id=:id order by oc.answered asc, oc.timestamp desc")
    List<OrderCall> findAndSortStationOrderCalls(@Param("id")Integer stationId);

    @Query("select oc from OrderCall oc where oc.station=:station order by oc.answered asc, oc.timestamp desc")
    List<OrderCall> findAndSortStationOrderCalls(@Param("station")Station station);

    @Modifying
    @Transactional
    @Query("update OrderCall oc set oc.answered = case oc.answered when true then false when false then true end where oc.id=:id")
    void toggleAnswer(@Param("id")Integer orderId);

    @Modifying
    @Transactional
    @Query("update OrderCall oc set oc.answered = case oc.answered when true then false when false then true end where oc=:orderCall")
    void toggleAnswer(@Param("orderCall")OrderCall orderCall);
}
