package com.ss.remont.repository;

import com.ss.remont.entity.BalanceHistory;
import com.ss.remont.entity.Station;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import org.springframework.data.jpa.repository.Query;

@RepositoryRestResource
public interface BalanceHistoryRepository  extends CrudRepository<BalanceHistory, Integer>{
	
		
	@Query("select bh from BalanceHistory bh where bh.station=?1 and bh.modifyDate between ?2 and ?3")
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetween(Station station,
			                                                                      Date from,
			                                                                      Date to);
	
	
	@Query("select bh from BalanceHistory bh where bh.station=?1 and (bh.modifyDate between ?2 and ?3) and bh.balance < 0")
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceLessThan(Station station, 
			Date from,
            Date to);

	@Query("select bh from BalanceHistory bh where bh.station=?1 and (bh.modifyDate between ?2 and ?3) and bh.balance > 0")
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceGreaterThan(Station station, 
			Date from,
            Date to);
}
