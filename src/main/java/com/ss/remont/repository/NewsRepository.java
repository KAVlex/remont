package com.ss.remont.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ss.remont.entity.News;

import java.util.Date;
import java.util.List;

@RepositoryRestResource
public interface NewsRepository extends PagingAndSortingRepository<News, Integer>{

    List<News> findTop4ByOrderByIdDesc();

//    @Query("select n from News n where n.id=:id")
//    Slice<News> getPage(@Param("id")Integer id, Pageable pageable);

    @Query("select max(n) from News n where n.id<:id")
    News getPreviousFrom(@Param("id") Integer id);

    @Query("select min(n) from News n where n.id>:id")
    News getNextFrom(@Param("id")Integer id);

    @Query("select n from News n where lower(n.title ) like lower(concat('%',:request,'%'))")
    List<News> searchFor(@Param("request")String string);

    @Query("select n from News n")
    Page<News> getPage(Pageable pageable);

    @Query("select max(n) from News n where n.timestamp<:date")
    News getNextByRelevance(@Param("date")Date date);

    @Query("select min(n) from News n where n.timestamp>:date")
    News getPreviousByRelevance(@Param("date")Date date);

}
