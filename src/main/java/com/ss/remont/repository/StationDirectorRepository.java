package com.ss.remont.repository;

import com.ss.remont.entity.StationDirector;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Tensky on 18.02.2017.
 */
@RepositoryRestResource
public interface StationDirectorRepository extends CrudRepository<StationDirector,Integer>
{
}
