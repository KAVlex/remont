package com.ss.remont.repository;

import com.ss.remont.entity.RepairType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Tensky on 22.11.2016.
 */
@RepositoryRestResource
public interface RepairTypeRepository extends CrudRepository<RepairType, Integer> {

	@Query(value = "select rt from RepairType rt where rt.id in :ids")
	List<RepairType> listOfRepairTypesByIds(@Param("ids") List<Integer> ids);

}
