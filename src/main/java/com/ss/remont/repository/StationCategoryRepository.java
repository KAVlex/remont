package com.ss.remont.repository;
import com.ss.remont.entity.StationCategory;
import com.ss.remont.entity.StationCategoryEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


/**
 * Created by Tensky on 30.03.2017.
 */
@RepositoryRestResource
public interface StationCategoryRepository extends CrudRepository<StationCategory,Integer>
{
    @Query("select s.stationCategory from StationCategory s where s.stationCategory=:statCat")
    StationCategoryEnum findStationCategoryEnumByEnum(@Param("statCat")StationCategoryEnum stationCategoryEnum);

    @Query("select s from StationCategory s where s.stationCategory=:statCat")
    StationCategory findStationCategoryByEnum(@Param("statCat")StationCategoryEnum stationCategoryEnum);
}
