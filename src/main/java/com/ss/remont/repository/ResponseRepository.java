package com.ss.remont.repository;

import java.util.List;

import com.ss.remont.entity.Bid;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.ss.remont.entity.Response;
import org.springframework.data.repository.query.Param;

public interface ResponseRepository extends PagingAndSortingRepository<Response, Integer>{

	List<Response> findByBidIdAndBidClientId(Integer bid, Integer cid, Pageable pageable);
	
	List<Response> findByStationId(Integer station, Pageable pageable);
	
	Response findByStationIdAndBidId(Integer stationId, Integer bidId);

    @Query("select r from Response r where r.bid=:bid order by r.time desc")
    List<Response> findResponsesByBid(@Param("bid")Bid bid);

    @Query("select r from Response r where r.bid.id=:id order by r.time desc")
    List<Response> findResponsesByBid(@Param("id")Integer bidId);

    @Query("select count(r) from Response r where r.bid=:bid")
    Integer BidResponsesCount(@Param("bid")Bid bid);

    @Query("select count(r) from Response r where r.bid.id=:id")
    Integer BidResponsesCount(@Param("id")Integer bidId);
}
