package com.ss.remont.repository;

import com.ss.remont.entity.RepairType;
import com.ss.remont.entity.Station;

import com.ss.remont.entity.StationCategoryEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Tensky on 16.11.2016.
 */
//@RepositoryRestResource		//-используется???
public interface StationRepository extends JpaRepository<Station, Integer> {

	Page<Station> findAll(Pageable pageable);

	@Query(value = "SELECT s FROM Station s LEFT JOIN s.repairTypePrices rtp WHERE rtp.repairType.id IN :ids "
			+ "group by s.id,s.name,s.phone,s.photo,s.email,s.locked,s.role,s.type,s.emailNotice "
			+ "HAVING COUNT (DISTINCT rtp.id) = :sizeOfList")
	List<Station> findStationByRepairTypes(@Param("ids") List<Integer> ids, @Param("sizeOfList") Long sizeOfList);

	@Modifying
	@Transactional
	@Query(value = "update Station s set s.info=:info where s.id=:id")
	void updateStationInfo(@Param("id") Integer id, @Param("info") String newInfo);

    @Modifying
    @Transactional
    @Query(value = "update Station s set s.info=:info where s=:station")
    void updateStationInfo(@Param("station")Station station, @Param("info")String newInfo);

	@Query(value = "select rtp.repairType from RepairTypePrice rtp where rtp.station.id=:id")
	List<RepairType> findRepairTypesByStationId(@Param("id") Integer stationID);

	@Query(value = "select rtp.repairType from RepairTypePrice rtp where rtp.station=:station")
	List<RepairType> findRepairTypesByStation(@Param("station") Station station);

	@Query(value = "select s from Station s where lower(s.name) like lower(concat('%',:request,'%'))")
	List<Station> searchFor(@Param("request") String string);
	
	Page<Station> findAllStationsByNameContainsOrId(Pageable pageable,String search,Integer id);

//    @Query(value = "select s from Station s join StationCategory sc where :cat = sc.stationCategory and s in sc.stations")
//    List<Station> findAllStationsByStationCategory(@Param("cat") StationCategoryEnum stationCategory);

    @Query(value = "select s from Station s where lower(s.city.name) like lower(concat('%',:city,'%'))")
    List<Station> findAllStationsByCityName(@Param("city")String city);

    @Modifying
    @Transactional
    @Query("update Station s set s.rating=(select round(avg(r.rating),0) from Recall r where r.station.id=:id) where s.id=:id")
    void updateStationRating(@Param("id")Integer stationId);

//    @Modifying
//    @Transactional
//    @Query("update Station s set s.rating=(select round(avg(r.rating),0) from Recall r where r.station=:station) where s=:station")
//    void updateStationRating(@Param("station")Station station);
}
