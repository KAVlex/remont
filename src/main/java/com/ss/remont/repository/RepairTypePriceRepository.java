package com.ss.remont.repository;

import com.ss.remont.entity.RepairTypePrice;
import com.ss.remont.entity.Station;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Tensky on 10.01.2017.
 */
public interface RepairTypePriceRepository extends CrudRepository<RepairTypePrice, Integer>
{
    @Query("select rtp from RepairTypePrice rtp where rtp.station.id=:id")
    List<RepairTypePrice> getStationPriceList(@Param("id")Integer stationId);

    @Query("select rtp from RepairTypePrice rtp where rtp.station=:station")
    List<RepairTypePrice> getStationPriceList(@Param("station")Station station);

    @Query("select rtp.station from RepairTypePrice rtp where rtp=:rtp")
    Station getStationByRepairTypePrice(@Param("rtp")RepairTypePrice repairTypePrice);

    @Query("select rtp.station from RepairTypePrice rtp where rtp.id=:id")
    Station getStationByRepairTypePrice(@Param("id")Integer repairTypePriceId);

    @Modifying
    @Transactional
    @Query("delete from RepairTypePrice rtp where rtp.id=:id")
    void delete(@Param("id")Integer repairTypePriceId);

    @Modifying
    @Transactional
    @Query("delete from RepairTypePrice rtp where rtp=:rtp")
    void delete(@Param("rtp")RepairTypePrice repairTypePrice);
}
