package com.ss.remont.repository;

import com.ss.remont.entity.CarModel;
import com.ss.remont.entity.CarYear;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Tensky on 05.12.2016.
 */
@RepositoryRestResource
public interface CarYearRepository extends CrudRepository<CarYear,Integer>
{

    @Query(value = "select cy from CarYear cy where cy.model=:carModel and cy.year=:year")
    CarYear findByModelAndYear(@Param("carModel") CarModel carModel,@Param("year") Integer year);

    @Query(value = "select cy from CarYear cy where cy.model.id=:cmid and cy.year=:year")
    CarYear findByModelAndYear(@Param("cmid") Integer carModelId, @Param("year") Integer year);
}
