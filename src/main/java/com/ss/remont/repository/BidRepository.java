package com.ss.remont.repository;

import com.ss.remont.entity.Bid;
import com.ss.remont.entity.BidStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tensky on 29.11.2016.
 */
public interface BidRepository extends CrudRepository<Bid,Integer>
{
    @Query(value="select b from Bid b where b.status=:status order by b.timestamp desc")
    List<Bid> getBidsByStatus(@Param("status")BidStatus status);

    @Query(value = "select b from Bid b where b.status in :status order by b.timestamp desc")
    List<Bid> getBidByStatus();

    List<Bid>findTop10ByOrderByIdDesc();

}
