package com.ss.remont.repository;

import com.ss.remont.entity.CarModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by Tensky on 28.11.2016.
 */
@RepositoryRestResource
public interface CarModelRepository extends PagingAndSortingRepository <CarModel, Integer>
{
    @Query("select carModel from CarModel carModel where carModel.mark.id=:modelId")
    List<CarModel> findCarModelByMarkId(@Param("modelId") Integer modelId);
}
