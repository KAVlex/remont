package com.ss.remont.repository;

import com.ss.remont.entity.Feedback;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Tensky on 16.12.2016.
 */
@RepositoryRestResource
public interface FeedbackRepository extends PagingAndSortingRepository<Feedback,Integer>
{
	public Page<Feedback> findAllFeedbackByActive(Pageable pageable,Boolean active);
	
}
