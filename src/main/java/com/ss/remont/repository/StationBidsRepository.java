package com.ss.remont.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ss.remont.entity.BidStatus;
import com.ss.remont.entity.StationBids;

public interface StationBidsRepository extends JpaRepository<StationBids, Long>{

	//Активные заявки
	public List<StationBids> findByStationIdAndStatusAndBidStationIsNull(Integer stationId, BidStatus bidStatus);
	
	//Прямые записи
	public List<StationBids> findByStationIdAndStatusAndBidStationId(Integer stationId, BidStatus bidStatus,
			Integer bidStationId);
	
	//В работе
	public List<StationBids> findByStationIdAndStatus(Integer stationId, BidStatus bidStatus);
	
	//Архив заявок
	public List<StationBids> findByStationIdAndStatusIn(Integer stationId, Collection<BidStatus> bidStatuses);

    //Поиск по идентификатору заявки
    public List<StationBids> findByBidId(Integer bidId);

    //Поиск по идентификаторам заявки и станции
    StationBids findByBidIdAndStationId(Integer bidId, Integer stationId);
	
}
