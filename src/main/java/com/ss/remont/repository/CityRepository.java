package com.ss.remont.repository;

import com.ss.remont.entity.City;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by Tensky on 28.11.2016.
 */
public interface CityRepository extends CrudRepository<City,Integer>
{

    @Query(value = "select c from City c where c.name=:city")
    City findByName(@Param("city")String cityName);
}
