package com.ss.remont.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ss.remont.entity.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

public interface ClientRepository extends PagingAndSortingRepository<Client,Integer> {
	 
	 @Query("select c from Client c where c.name LIKE %?1% or c.email LIKE %?1% or c.phone LIKE %?1%")
	 Page<Client> findAllClientBySearchWordContains(Pageable pageable,String search);
	 
	 @Query("select c from Client c where (c.name LIKE %?1% or c.email LIKE %?1% or c.phone LIKE %?1% ) and c.locked=?2")
	 Page<Client> findAllClientBySearchWordContainsAndLocked(Pageable pageable,String search,Boolean locked);

}
