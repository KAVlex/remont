package com.ss.remont.repository;

import com.ss.remont.entity.Images;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.Role;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AuthUserRepository extends CrudRepository<AuthUser,Integer> {
	
	public AuthUser findOneByEmail(String email);
	
	public AuthUser findOneByPhone(String phone); 
	

    @Query(value = "select count(sau) from AuthUser sau where sau.role=:role")
    Integer getCountByRole(@Param("role")Role role);

    @Modifying
    @Transactional
    @Query("update AuthUser au set au.photo=:ava where au.id=:id")
    void setUserAvatar(@Param("id")Integer authUserId, @Param("ava")Images photo);

    @Modifying
    @Transactional
    @Query("update AuthUser au set au.photo=:ava where au=:au")
    void setUserAvatar(@Param("au")AuthUser authUser,@Param("ava")Images photo);
}
