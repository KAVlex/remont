package com.ss.remont.repository;

import com.ss.remont.entity.OrderEmail;
import com.ss.remont.entity.Station;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Tensky on 10.01.2017.
 */
public interface OrderEmailRepository extends CrudRepository<OrderEmail,Integer>
{
    @Query(value = "SELECT oe FROM OrderEmail oe where oe.station.id=:id")
    List<OrderEmail> findStationOrderEmails(@Param("id")Integer stationID);

    @Query("select oe from OrderEmail oe where oe.station=:station")
    List<OrderEmail> findStationOrderEmails(@Param("station")Station station);

    @Query("select oe from OrderEmail oe where oe.station.id=:id order by oe.answered asc, oe.timestamp desc")
    List<OrderEmail> findAndSortStationOrderEmails(@Param("id")Integer stationId);

    @Query("select oe from OrderEmail oe where oe.station=:station order by oe.answered asc, oe.timestamp desc")
    List<OrderEmail> findAndSortStationOrderEmails(@Param("station")Station station);

    @Modifying
    @Transactional
    @Query("update OrderEmail oe set oe.answered = case oe.answered when true then false when false then true end where oe.id=:id")
    void toggleAnswer(@Param("id")Integer orderId);

    @Modifying
    @Transactional
    @Query("update OrderEmail oe set oe.answered = case oe.answered when true then false when false then true end where oe=:orderEmail")
    void toggleAnswer(@Param("orderEmail")OrderEmail orderEmail);
}
