package com.ss.remont.repository;

import com.ss.remont.entity.Recall;
import com.ss.remont.entity.Station;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Tensky on 23.12.2016.
 */
@RepositoryRestResource
public interface RecallRepository extends CrudRepository<Recall, Integer>
{
    @Query(value = "select count(rc) from Recall rc where rc.station.id=:id")
    Integer countOfRecordsByStation(@Param("id") Integer stationId);

    @Query(value = "select count (rc) from  Recall rc where rc.station=:station")
    Integer countOfRecordsByStation(@Param("station") Station station);

    @Query(value = "select rc from Recall rc where rc.station.id=:id")
    List<Recall> findAllRecallsByStation(@Param("id")Integer stationId);

    @Query(value = "select rc from Recall rc where rc.station=:station")
    List<Recall> findAllRecallsByStation(@Param("station")Station station);
}
