package com.ss.remont.repository;

import com.ss.remont.entity.Discount;
import com.ss.remont.entity.Images;
import com.ss.remont.entity.Station;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Tensky on 09.12.2016.
 */
@RepositoryRestResource
public interface DiscountRepository extends CrudRepository<Discount,Integer>
{
//    @Query(value = "select d from Discount d where d.status=:status order by d.date_start")
//    List<Discount> getDiscountsByStatus(@Param("status")DiscountStatus status);

    @Query(value = "select d from Discount d where d.station.id=:id")
    List<Discount> getThisStationDiscounts(@Param("id")Integer stationID);

    @Query(value = "select d from Discount d where d.station=:station order by d.id asc")
    List<Discount> getThisStationDiscounts(@Param("station")Station station);

    @Query("select d from Discount d where (current_date between d.date_start and d.date_end) and d.station.id=:id")
    List<Discount> getThisStationActiveDiscounts(@Param("id")Integer stationId);

    @Query("select d from Discount d where (current_date between d.date_start and d.date_end) and d.station=:station")
    List<Discount> getThisStationActiveDiscounts(@Param("station")Station station);

    @Query(value = "select d from Discount d where current_date between d.date_start and d.date_end")
    List<Discount> getActiveDiscounts();

    @Query(value = "select d from Discount d where current_date not between d.date_start and d.date_end")
    List<Discount> getInactiveDiscounts();

//    @Query(value = "select d from Discount d where d.station.id=:id and d.status=:status")
//    List<Discount> getThisStationDiscounts(@Param("id")Integer stationID,@Param("status") DiscountStatus status);
//
//    @Query(value = "select d from Discount d where d.station=:station and d.status=:status")
//    List<Discount> getThisStationDiscounts(@Param("station")Station station, @Param("status")DiscountStatus status);

    @Query(value = "select d from Discount d where d.id=:id")
    Discount findOne(@Param("id") Long discountID);

    @Query(value = "select d from Discount d where lower(d.title) like lower(concat('%',:request,'%')) or lower(d.info) like lower(concat('%',:request,'%')) group by d.id")
    List<Discount> searchFor(@Param("request")String string);

    @Modifying
    @Transactional
    @Query("update Discount d " +
            "set d.title=:title, d.info=:info, d.date_start=:dstart, d.date_end=:dend," +
            " d.photo=(case :photo when null then d.photo else :photo end) " +
            "where d.id=:id")
    void edit(@Param("id")Long discountId, @Param("title")String title, @Param("info")String info,
              @Param("dstart")Date date_start, @Param("dend")Date date_end, @Param("photo")Images photo);

    @Modifying
    @Transactional
    @Query("update Discount d " +
            "set d.title=:title, d.info=:info, d.date_start=:dstart, d.date_end=:dend," +
            " d.photo=(case :photo when null then d.photo else :photo end) " +
            "where d=:discount")
    void edit(@Param("discount")Discount discount, @Param("title")String title, @Param("info")String info,
              @Param("dstart")Date date_start, @Param("dend")Date date_end, @Param("photo")Images photo);
}
