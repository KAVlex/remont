package com.ss.remont.repository;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;
import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ss.remont.entity.Sms;
import com.ss.remont.entity.SmsStatus;

public interface SmsRepository extends CrudRepository<Sms, Long>
{

    Sms findByIdAndStatus(Long id, SmsStatus status);

    List<Sms> findByStatusAndTimestampBefore(Pageable pageable, SmsStatus status, Date before);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Sms s SET s.status = :status WHERE s IN :smsList")
    int updateStatus(@Param("status") SmsStatus status, @Param("smsList") List<Sms> smsList);

    @Query("select s from Sms s order by timestamp desc")
    Page<Sms> getPage(Pageable pageable);

    @Query("select s.phone from Sms s group by s.phone")
    List<String> findAllPhones();

    @Query("select s from Sms s where s.id in :ids")
    List<Sms> findByIds(@Param("ids") Long[] ids);

    @Query("select s from Sms s where " +
            "(:phone is null or s.phone=:phone) and " +
            "(:status is null or s.status=:status) " +
            "and s.timestamp between :date_start and :date_end " +
            "order by s.timestamp desc")
    Page<Sms> filter(@Nullable @Param("phone") String phone,
                     @Nullable @Param("status") SmsStatus status,
                     @Nullable @Param("date_start") Date date_start,
                     @Nullable @Param("date_end") Date date_end,
                     Pageable pageable);
}
