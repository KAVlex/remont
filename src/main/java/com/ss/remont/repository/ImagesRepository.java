package com.ss.remont.repository;

import com.ss.remont.entity.Images;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Tensky on 14.11.2016.
 */
public interface ImagesRepository extends CrudRepository<Images, Integer>
{

}
