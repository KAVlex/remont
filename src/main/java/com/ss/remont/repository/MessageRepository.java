package com.ss.remont.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.ss.remont.entity.Message;

public interface MessageRepository extends PagingAndSortingRepository<Message, Long> {

	// Для станций
	public List<Message> findByResponseIdAndResponseStationId(Integer rId, Integer sId, Pageable pageable);

	// Для клиентов
	public List<Message> findByResponseIdAndResponseBidClientId(Integer rId, Integer cId, Pageable pageable);
	
	public Integer countByRecipientIdAndRead(Integer id, Boolean read);	
	
	public Message findByIdAndRecipientId(Long id, Integer rId);
}
