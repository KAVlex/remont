package com.ss.remont.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ss.remont.entity.Article;

@RepositoryRestResource
public interface ArticleRepository extends PagingAndSortingRepository<Article, Integer>{

}
