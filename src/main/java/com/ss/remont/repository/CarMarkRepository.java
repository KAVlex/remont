package com.ss.remont.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ss.remont.entity.CarMark;

@RepositoryRestResource
public interface CarMarkRepository extends PagingAndSortingRepository<CarMark, Integer>{

	 Page<CarMark> findAllByNameContains(Pageable pageable,String name);
	 
	 CarMark findOneByName(String name);
	
}
