package com.ss.remont.repository;

import com.ss.remont.entity.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Tensky on 05.12.2016.
 */
@RepositoryRestResource
public interface CarRepository extends CrudRepository<Car,Integer>
{
    @Query("select car from Car car where car.client.id=:userID")
    List<Car> findUserCars(@Param("userID") Integer userID);

    @Deprecated
    @Modifying
    @Transactional
    @Query("delete from Car car where car.id=:id")
    void delete(@Param("id") Integer id);

    @Deprecated
    @Modifying
    @Transactional
    @Query("delete from Car car where car=:car")
    void delete(@Param("car") Car car);

    @Modifying
    @Transactional
    @Query("update Car car set car.client=null where car.id=:id")
    void nullClient(@Param("id") Integer carID);

    /**
     * Изменяет цвет для ТС с идентификатором <code>id</code>
     *
     * @param carID id транспортного средства
     * @param color новый цвет
     */
    @Modifying
    @Transactional
    @Query("update Car car set car.color=:color where car.id=:id")
    void updateColor(@Param("id") Integer carID, @Param("color") String color);

    /**
     * Изменяет регистрационный номер для ТС с идентификатором <code>id</code>
     *
     * @param carID     id транспортного средства
     * @param regNumber новый регистрационный номер
     */
    @Modifying
    @Transactional
    @Query("update Car car set car.regNumber=:reg where car.id=:id")
    void updateRegNumber(@Param("id") Integer carID, @Param("reg") String regNumber);

    @Query("select car from Car car where car.regNumber=:num")
    Car findByRegNumber(@Param("num") String regNumber);

    @Deprecated
    @Modifying
    @Transactional
    @Query("update Car car set car.client=:client where car.id=:id")
    void setNewClient(@Param("id") Integer carID, @Param("client") Client client);

    @Modifying
    @Transactional
    @Query("update Car car set car.client=:client, car.mark=:mark, car.model=:model, car.year=:year, car.color=:color,car.regNumber=:num where car.id=:id")
    void updateCar(@Param("id")Integer carID,
                   @Param("client")Client client,
                   @Param("mark")CarMark carMark,
                   @Param("model")CarModel model,
                   @Param("year")CarYear carYear,
                   @Param("color")String color,
                   @Param("num")String regNumber);
    
    
	public Car findByClientAndMarkAndModelAndYear(Client client, CarMark carMark, CarModel carModel,
			CarYear carYear);
}
