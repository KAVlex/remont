package com.ss.remont.repository;

import org.springframework.data.repository.CrudRepository;

import com.ss.remont.auth.SimpleAuthUser;

public interface SimpleAuthUserRepository  extends CrudRepository<SimpleAuthUser,Integer> {

	
	SimpleAuthUser findOneByAuthUserId(Integer id);
	
	
}
