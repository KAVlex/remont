package com.ss.remont.repository;

import com.ss.remont.entity.Faq;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Tensky on 30.12.2016.
 */
@RepositoryRestResource
public interface FaqRepository extends CrudRepository<Faq,Integer>
{
    @Query(value = "SELECT f FROM Faq f")
    List<Faq> findAll();
}
