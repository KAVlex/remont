package com.ss.remont.service;

public interface NotificationService {
	
	void addInfoMessage(String msg);

	void addErrorMessage(String msg);
}
