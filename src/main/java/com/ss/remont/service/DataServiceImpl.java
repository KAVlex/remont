package com.ss.remont.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ss.remont.entity.BalanceHistory;
import com.ss.remont.entity.Station;
import com.ss.remont.repository.BalanceHistoryRepository;
import com.ss.remont.repository.StationRepository;

@Service("dataService")
public class DataServiceImpl implements DataService {

	@Autowired
	StationRepository stationRepository;

	@Autowired
	BalanceHistoryRepository balanceHistoryRepository;

	@Override
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetween(Integer idStation, String fromDate,
			String toDate) {
		List<BalanceHistory> list = null;
		if (idStation != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date curDate = sdf.parse(fromDate);
				Date from = new Date(curDate.getTime());
				curDate = sdf.parse(toDate);
				Date to = new Date(curDate.getTime());
				Station station = stationRepository.findOne(idStation);
				list = balanceHistoryRepository.findBalanceHistoryByStationIdAndModifyDateBetween(station, from, to);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceLessThan(Integer idStation,
			String fromDate, String toDate) {
		List<BalanceHistory> list = null;
		if (idStation != null) {
			try {
				System.out.println("spisanie=================");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date curDate = sdf.parse(fromDate);
				Date from = new Date(curDate.getTime());
				curDate = sdf.parse(toDate);
				Date to = new Date(curDate.getTime());
				Station station = stationRepository.findOne(idStation);
				list = balanceHistoryRepository.findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceLessThan(station, from, to);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceGreaterThan(
			Integer idStation, String fromDate, String toDate) {
		List<BalanceHistory> list = null;
		System.out.println("zachislenie==================");
		if (idStation != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date curDate = sdf.parse(fromDate);
				Date from = new Date(curDate.getTime());
				curDate = sdf.parse(toDate);
				Date to = new Date(curDate.getTime());
				Station station = stationRepository.findOne(idStation);
				list = balanceHistoryRepository.findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceGreaterThan(station, from, to);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

}
