package com.ss.remont.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ss.remont.entity.Station;
import com.ss.remont.entity.StationCategory;
import com.ss.remont.entity.StationCategoryEnum;
import com.ss.remont.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


import com.ss.remont.auth.Role;
import com.ss.remont.auth.RoleRegistrationFactory;
import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.AuthorityType;
import com.ss.remont.auth.EmailActivateForm;
import com.ss.remont.auth.EmailRegistrationForm;
import com.ss.remont.auth.RegistrationTypeFactory;
import com.ss.remont.auth.SimpleAuthUser;
import com.ss.remont.captcha.ICaptchaService;
import com.ss.remont.captcha.ReCaptchaInvalidException;
import com.ss.remont.utils.DigestUtils;
import com.ss.remont.controller.URIPath;
import com.ss.remont.controller.ViewPath;
import com.ss.remont.mail.Confirm;
import com.ss.remont.mail.template.RegistrationTemplate;


@Controller
public class SaveUser
{

    @Autowired
    public SimpleAuthUserRepository simpleAuthUserRepository;

    @Autowired
    public AuthUserRepository authUserRepository;

    @Autowired
    public ClientRepository clientRepository;

    @Autowired
    public MailService mailService;

    @Autowired
    public StationRepository stationRepository;

    @Autowired
    private ICaptchaService captchaService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private StationCategoryRepository stationCategoryRepository;

    @RequestMapping(URIPath.REGISTRATION_SAVE_USER)
    public String saveSimpleUser(@RequestParam(name = "name", required = true) String name,
                                 @RequestParam(name = "email", required = true) String email,
                                 @RequestParam(name = "phone", required = true) String phone,
                                 @RequestParam(name = "role", required = true) String role,
                                 @RequestParam(name = "password", required = true) String password,
                                 @RequestParam(name = "repass", required = true) String repass,
                                 @RequestParam(name = "address", required = false) String address,
                                 @RequestParam(name = "latitude", required = false) Double latitude,
                                 @RequestParam(name = "longitude", required = false) Double longitude,
                                 @RequestParam(name = "dealer", required = false) Boolean dealer,
                                 @RequestParam(name = "freelancer", required = false) Boolean freelancer,
                                 @RequestParam(name = "g-recaptcha-response", required = false) String recaptcha,
                                 @RequestParam(name = "stationCategories") List<String> statCategories)
    {
        if (recaptcha != null)
        {
            try
            {
                captchaService.processResponse(recaptcha);
            } catch (ReCaptchaInvalidException e)
            {
                notificationService.addErrorMessage("Необходимо подтвердить, что вы не робот - " + e.getMessage());
                return "redirect:" + URIPath.REGISTRATION_REGERROR;
            }
        }

        if (role.equals("ADMIN"))
        {
            return "redirect:" + URIPath.REGISTRATION_REGERROR;
        }

        EmailRegistrationForm registrationForm = new EmailRegistrationForm();
        registrationForm.setName(name);
        registrationForm.setEmail(email);
        registrationForm.setPhone(phone);
        registrationForm.setRole(role);
        registrationForm.setPassword(password);
        registrationForm.setRepass(repass);
        registrationForm.setAddress(address);
        registrationForm.setLatitude(latitude);
        registrationForm.setLongitude(longitude);
        registrationForm.setFreelancer(freelancer);
        if (dealer != null && dealer.booleanValue())
        {
            registrationForm.setDealer(true);
        }

        if (saveUser(registrationForm, statCategories))
        {
            return "redirect:" + URIPath.REGISTRATION_WAIT_NOTIFICATION;
        }
        else
        {
            return "redirect:" + URIPath.REGISTRATION_REGERROR;
        }
    }

    public boolean saveUser(EmailRegistrationForm rf, List<String> statCategories)
    {
        boolean result = false;
        Set<String> reasons = checkCredentials(rf.getName(), rf.getPhone(), rf.getEmail(), rf.getPassword(),
                rf.getRepass());
        if (reasons != null && reasons.size() > 0)
        {
            for (String error : reasons)
            {
                notificationService.addErrorMessage(error);
            }
            return false;
        }
        AuthUser sauPhone = authUserRepository.findOneByPhone(rf.getPhone());
        AuthUser sauEmail = authUserRepository.findOneByPhone(rf.getPhone());

        if (sauPhone == null && sauEmail == null)
        {
            Role theRole = Role.valueOf(rf.getRole());
            RoleRegistrationFactory rrf = RoleRegistrationFactory.getFactory(this, theRole);
            rrf.importRegistrationForm(rf);
            rrf.saveAuthWithRole();
            RegistrationTypeFactory rtf = RegistrationTypeFactory.getFactory(rrf.getAuthUser(), this);
            Object dbObject = rtf.saveUser(rf);
            result = true;
            SimpleAuthUser simpleAuthUser = (SimpleAuthUser) dbObject;
            AuthUser authUser = simpleAuthUser.getAuthUser();
            if (authUser.getRole().equals(Role.STATION))
            {
                if (statCategories == null)
                {
                    notificationService.addErrorMessage("Выберите тип станции");
                    authUserRepository.delete(authUser);
                    return false;
                }
                else if (statCategories.isEmpty())
                {
                    notificationService.addErrorMessage("Выберите тип станции");
                    authUserRepository.delete(authUser);
                    return false;
                }
                boolean success = true;
                try
                {
                    Station station = stationRepository.findOne(authUser.getId());
//                List<StationCategoryEnum> stationCategories = new ArrayList<>();
                    List<StationCategory> existingStationCategories = (List<StationCategory>) stationCategoryRepository.findAll();
                    if (existingStationCategories == null)
                    {
                        existingStationCategories = new ArrayList<>();
                    }
                    for (String sc : statCategories)
                    {
                        StationCategoryEnum scenum = StationCategoryEnum.findByString(sc);
                        StationCategory statCatFromDB = stationCategoryRepository.findStationCategoryByEnum(scenum);
                        if (!existingStationCategories.contains(statCatFromDB))
                        {
                            statCatFromDB = new StationCategory(scenum);
                            statCatFromDB = stationCategoryRepository.save(statCatFromDB);
                            existingStationCategories.add(statCatFromDB);
                        }
                        if (station.getStationCategories()== null)
                        {
                            station.setStationCategories(new ArrayList<>());
                        }
                        station.getStationCategories().add(statCatFromDB);
                    }
                    station = stationRepository.save(station);
                    if(station.getStationCategories().size() != statCategories.size())
                    {
                        success=false;
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                    success = false;
                } finally
                {
                    if (!success)
                    {
                        Integer sauId = simpleAuthUser.getId();
                        Integer auId = simpleAuthUser.getAuthUser().getId();
                        notificationService.addErrorMessage("Ошибка при сохранении станции");
                        simpleAuthUserRepository.delete(sauId);
                        stationRepository.delete(auId);
                        try
                        {
                        authUserRepository.delete(auId);
                        }catch (EmptyResultDataAccessException ignored){}
                    }
                }
            }
            //rtf.afterSave(dbObject,rf);
        }
        else
        {
            notificationService.addErrorMessage("Пользователь с указанным телефоном уже зарегистрирован");
        }
        return result;
    }

    public String getEmailCheck(String email, String password)
    {
        String check = email + '_' + password;
        System.out.println(check);
        check = DigestUtils.md5(check);

        return check;
    }


    @RequestMapping(URIPath.REGISTRATION_ACTIVATE_USER)
    public String activateSimpleUser(@RequestParam(value = "id", required = true) Integer id,
                                     @RequestParam(value = "check", required = true) String check,
                                     Model model)
    {

        EmailActivateForm activationForm = new EmailActivateForm();
        activationForm.setId(id);
        activationForm.setCheck(check);
        boolean result = false;
        SimpleAuthUser sau = simpleAuthUserRepository.findOne(id);
        AuthUser au = sau.getAuthUser();
        RegistrationTypeFactory rrf = RegistrationTypeFactory.getFactory(au, this);
        rrf.activateUser(sau, activationForm);
        result = !sau.getAuthUser().getLocked();
        if (result)
        {
            model.addAttribute("id", sau.getId());
            model.addAttribute("check", check);
            simpleAuthUserRepository.save(sau);
            return ViewPath.NEW_PASSWORD;
        }
        else
        {
            return "redirect:" + URIPath.REGISTRATION_REGERROR;
        }
    }


    @RequestMapping(URIPath.REGISTRATION_REPAIR_PASSWORD)
    public String repairPassword(@RequestParam(value = "email", required = true) String email,
                                 Model model)
    {

        AuthUser au = authUserRepository.findOneByEmail(email);
        if (au.getType().equals(AuthorityType.SIMPLE))
        {
            SimpleAuthUser sau = simpleAuthUserRepository.findOneByAuthUserId(au.getId());
            Confirm confirm = new Confirm();
            confirm.setEmail(email);
            String check = getEmailCheck(au.getEmail(), sau.getPassword());
            String activationLink = "http://localhost:8080/registration/activateUser?id=" +
                    String.valueOf(sau.getId());
            try
            {
                activationLink += "&check=" + URLEncoder.encode(check, "UTF-8");
            } catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }
            confirm.setActivationLink(activationLink);
            RegistrationTemplate tempalte = new RegistrationTemplate(confirm);
            mailService.sendEmail(tempalte);
            au.setLocked(true);
            authUserRepository.save(au);
            return "redirect:" + URIPath.REGISTRATION_WAIT_NOTIFICATION;
        }
        else
        {
            return "redirect:" + URIPath.REGISTRATION_REGERROR;
        }


    }


    @RequestMapping(URIPath.REGISTRATION_NEW_PASSWORD)
    public String newPassword(@RequestParam(value = "id", required = true) Integer id,
                              @RequestParam(value = "password", required = true) String password,
                              @RequestParam(value = "repass", required = true) String repass,
                              @RequestParam(value = "check", required = true) String check,
                              Model model)
    {
        SimpleAuthUser sau = simpleAuthUserRepository.findOne(id);

        if (sau != null)
        {
            String oldCheck = getEmailCheck(sau.getAuthUser().getEmail(), sau.getPassword());
            if (oldCheck.equals(check))
            {
                sau.setPassword(password);
                sau.getAuthUser().setLocked(false);
                simpleAuthUserRepository.save(sau);
                return "redirect:" + URIPath.LOGIN;
            }
            else
            {
                return "redirect:" + URIPath.REGISTRATION_REGERROR;
            }
        }
        else
        {
            return "redirect:" + URIPath.REGISTRATION_REGERROR;
        }
    }


    public boolean updateSimpleUserPassword(AuthUser authUser, String oldPassword, String password, String repass)
    {
        SimpleAuthUser sau = simpleAuthUserRepository.findOneByAuthUserId(authUser.getId());
        if (sau.getPassword().equals(DigestUtils.md5(oldPassword)) && password.equals(repass))
        {
            sau.setPassword(password);
            simpleAuthUserRepository.save(sau);
            return true;
        }
        else
        {
            notificationService
                    .addErrorMessage("Неверно введен старый пароль, либо новый пароль и подтверждение не совпадают");
            return false;
        }
    }

    private Set<String> checkCredentials(String name, String phone, String email, String pass,
                                         String pass2)
    {
        Set<String> denyReason = new HashSet<String>();
        if (name == null)
        {
            denyReason.add("Вы не ввели имя пользователя");
        }
        else
        {
            if (name.length() < 5 || name.length() > 32)
            {
                denyReason.add("Имя пользователя не может состоять из менее чем 5 символов и более чем 32");
            }
            //Если в имени есть спец символы
            Pattern pattern = Pattern.compile("[@#$%^&*(){}<>?!\\[\\],=~\\\\\";:+|]");
            Matcher matcher = pattern.matcher(name);
            if (matcher.find())
            {
                denyReason.add("Имя пользователя не может содержать специальные символы, "
                        + "такие как @#$%^&*[](){}<>?!,=~\\/\";:+|");
            }
        }
        if (email == null)
        {
            denyReason.add("Вы не ввели адрес электронной почты");
        }
        else
        {
            Pattern pattern = Pattern.compile("(.*@.*\\.)"); // Наличие '@' и
            // '.' после @
            Matcher matcher = pattern.matcher(email);
            if (!matcher.find())
            {
                denyReason.add("Вы ввели некорректный адрес электронной почты");
            }
        }

        if (phone == null)
        {
            denyReason.add("Вы не ввели номер телефона");
        }
        else
        {
            if (phone.length() < 5 || phone.length() > 10)
            {
                denyReason.add("Номер телефона должен содержать не менее 5 и не более 10 симоволов");
            }
            Pattern pattern = Pattern.compile("\\D");
            Matcher matcher = pattern.matcher(phone);
            if (matcher.find())
            {
                denyReason.add("Номер телефона должен состоять только из цифр");
            }
        }

        if (!pass.equals(pass2))
        {
            denyReason.add("Пароли не совпадают");
        }
        return denyReason;
    }

}
