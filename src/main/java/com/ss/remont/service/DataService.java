package com.ss.remont.service;

import java.util.List;

import com.ss.remont.entity.BalanceHistory;

public interface DataService {

	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetween(Integer idStation, String fromDate,
			String toDate);
	
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceLessThan(Integer idStation, 
			                                                                                String fromDate,
			                                                                                String toDate);
	
	public List<BalanceHistory> findBalanceHistoryByStationIdAndModifyDateBetweenAndBalanceGreaterThan(Integer idStation, 
            String fromDate,
            String toDate);
	
}
