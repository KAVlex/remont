package com.ss.remont.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ss.remont.auth.AuthUser;
import com.ss.remont.auth.AuthorityType;
import com.ss.remont.repository.AuthUserRepository;
import com.ss.remont.repository.SimpleAuthUserRepository;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;



@Service("userDetailsService")
public class AuthUserDetailsService implements UserDetailsService {


	@Autowired
	SimpleAuthUserRepository simpleAuthUser;
	
	@Autowired
	AuthUserRepository authUserRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		AuthUser authUser =  authUserRepository.findOneByPhone(login);
		if(authUser==null){
			throw new UsernameNotFoundException("Пользователь " + login + " не найден");
		}
		
		UserDetails userDetails = null;
		if(authUser.getType().equals(AuthorityType.SIMPLE)){
			userDetails = simpleAuthUser.findOneByAuthUserId(authUser.getId());
		}
		return userDetails;
		
	}
	
	
	

}
