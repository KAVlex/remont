package com.ss.remont.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import com.ss.remont.mail.Mail;


@Service("mailService")
public class MailServiceImpl implements MailService{

	
    @Autowired
    JavaMailSender mailSender;
    
    @Autowired
    protected FreeMarkerConfigurer freemarkerConfig;
    
     
	@Override
	public void sendEmail(Mail mail) {
	  try {
		    mail.setConfiguration(freemarkerConfig.getConfiguration());
	        mailSender.send(mail);
	        System.out.println("Message has been sent...........................");
	  }catch (MailException ex) {
	        System.err.println(ex.getMessage());
	  }
	}

}
