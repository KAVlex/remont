package com.ss.remont.pages;

import java.util.ArrayList;

public class Pages {

	private String base;
	private Integer page;
	private Integer total;
	
	public Pages(String link,Integer page,Integer total){
		this.base  = link;
		this.page  = page;
		this.total = total;
	}
	
	
	public ArrayList<Link> generatePages(){
		 ArrayList<Link> pages =  new ArrayList<Link>();
         Link link = null;
         if(page.intValue()>0){
        	link = new Link(base,page.intValue() - 1, "<");
        	pages.add(link);
         }
         int count = total.intValue() - 1;
         System.out.println(count);
         int from = page % 10;
         from  = (page - from) / 10;
         int to = from + 10;
         if(to>count){
         	to = count;
         }
	     for(int i=from;i<to;i++){
	    	link = new Link(base,i, String.valueOf(i));
	    	pages.add(link);
	     }
	     if(page<total.intValue() - 1){
	    	link = new Link(base,page.intValue() + 1, ">");
	    	pages.add(link);
	     }
	    return pages;
	}
	
}
