package com.ss.remont;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.ss.remont.config.ImageConverterProperties;
import com.ss.remont.config.StorageProperties;
import com.ss.remont.storage.StorageService;

@SpringBootApplication
@EnableConfigurationProperties({StorageProperties.class, ImageConverterProperties.class})
public class RemontApplication {

	public static void main(String[] args) {
		SpringApplication.run(RemontApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
//            storageService.deleteAll();
//            storageService.init();
		};
	}
}
