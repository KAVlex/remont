function jq( myid ) {
 
    return myid.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" );
 
}

function connect() {
	socket = new SockJS('/chat');
	stompClient = Stomp.over(socket);
	stompClient.debug = null;
	stompClient.connect('', '', function(frame) {
		whoami = frame.headers['user-name'];
		console.log('Connected: ' + frame);
		stompClient.subscribe('/user/queue/messages', function(message) {
			showMessage(JSON.parse(message.body), true);
		});
		stompClient.subscribe('/topic/active', function(activeMembers) {
			showActive(activeMembers);
		});
	});
}

$(document).ready(function() {
	console.log("ready!");
	connect();
	
	$("[name=idBid]").change(function(){
		$('.messages').html('');
		$('.messages').hide();
		$('#responseList').html('');
		if ($(this).val() != ''){
			getMassages($(this).val());	
		}
	});
});

function createItem(response){
	var item = $('<div>', {
		id : 'user-' + response.clientLogin,
		name: 'response-' + response.id,
		class : 'item'
	});
	var border_bot = $('<div>', {
		class : 'border_bot'
	});
	var logo_comp = $('<div>', {
		class : 'logo_comp'
	});
	border_bot.append(logo_comp);
	var img = $('<img/>', {
		src : '/images/?filename=' + ((response.clientPhoto) ? response.clientPhoto : '/common/images/unknown.m.jpg')
	});
	logo_comp.append(img);
	
	var name = $('<div>', {
		id: 'response-client-name-' + response.id,
		text : response.clientName	//'Автолоцман'
	});
	var raiting = $('<div>', {
		class : 'raiting'
	});
	raiting.append(name);
	border_bot.append(raiting);
	
	var under = $('<div>', {
		class : 'under'
	});
	var active = $('<div>', {
		class : 'col-md-8 col-sm-7 col-xs-8 active-status',	
		text : 'online'
	});
	var rub = $('<div>', {
		class : 'col-md-4 col-sm-5 col-xs-4 rub',
		text : 	response.cost
	});
	var contact = $('<div>', {
		class : 'address',
		text : 	response.clientLogin
	});
	under.append(active);
	under.append(rub);
	under.append(contact);
	
	item.append(border_bot);
	item.append(under);
	
	var hidden = $('<div>', {
		id: 'rinfo-' + response.id,
	});
	var textarea = $('<textarea>', {
		type : 'text',
		id: 'response-comment-' + response.id,
		text : 	response.comment
	});
	hidden.append(textarea);
	
	var content = $('<textarea>', {
		type : 'text',
		id: 'response-bid-content-' + response.id,
		text : 	response.bidContent
	});
	hidden.append(content);
	
	var station = $('<input>', {
		type : 'text',
		id: 'response-station-name-' + response.id,
		value : 	response.stationName
	});
	hidden.append(station);
	var stationLogin = $('<input>', {
		type : 'text',
		id: 'response-station-login-' + response.id,
		value : 	response.stationLogin
	});
	hidden.append(stationLogin);
	
	var time = $('<input>', {
		type : 'text',
		id: 'response-time-' + response.id,
		value : 	response.time
	});
	hidden.append(time);
	
	hidden.hide();
	
	item.append(hidden);
	
	return item;
}

function showMessage(message, online) {
	var mId = 'message-' + message.id;
	var item = $("#"+mId); 
	if (item.length){
		if (message.read){
			doRead(message.id, false);
		}
		return item;
	}
	if (message.sender !== whoami && online && !message.read){
		incrementMessageNum();
		incrementBidMessageCount(message.bid);
	}
	var chatWindowTarget = (message.recipient === whoami) ? message.sender
			: message.recipient;
	var chatContainer = getChatWindow(chatWindowTarget, message.response);
	if (chatContainer.length){
		var chatWindow = chatContainer.find('.window_message');
		var isread = message.read ? 'read' : 'unread';
		var item = $('<div>', {
			class : 'item ' + isread,
			id: mId
		});
		
		if (message.sender !== whoami) {
			var sendingUser = $('#user-' + jq(message.sender));
			var rid = sendingUser.attr('name').substring(9);
			if (!sendingUser.hasClass('active')
					&& !sendingUser.hasClass('pending-messages')) {
				sendingUser.append(newMessageIcon());
				sendingUser.addClass('pending-messages');
			}
			if (!message.read){
				markMessage(message.id, message.bid);	
			}
			
			var user_other_name = $('#response-client-name-' + rid);
					
			var user_other = $('<div>', {
				class : 'col-md-3 user_other',
				text: user_other_name.text()
			});
			item.append(user_other);
			var message_other = $('<div>', {
				class : 'col-md-9 message_other'
			});
			item.append(message_other);
			
			var div = $('<div>', {
				class : 'div'
			});
			message_other.append(div);
			var img = $('<img/>', {
				src : '/autoservice/img/treyg.png'
			});
			div.append(img);
			var span = $('<span/>', {
				class: 'message-text',
				text : message.message
			});
			div.append(span);
			var time = $('<div>', {
				class : 'time',
				text: message.timestamp
			});
			div.append(time);
			
			var user_auth = $('<div>', {
				class : 'col-md-3 user_auth'
			});
			item.append(user_auth);	
		}else{
			var recipientUser = $('#user-' + jq(message.recipient));
			var rid = recipientUser.attr('name').substring(9);
			var message_auth = $('<div>', {
				class : 'col-md-9 message_auth'
			});
			var div = $('<div>', {
				class : 'div'
			});
			message_auth.append(div);
			var img = $('<img/>', {
				src : '/autoservice/img/treyg_grey.png'
			});
			div.append(img);
			var span = $('<span/>', {
				text : message.message
			});
			div.append(span);
			var time = $('<div>', {
				class : 'time',
				text: message.timestamp
			});
			div.append(time);
			
			var user_auth_name = $('#response-station-name-' + rid);
			var user_auth = $('<div>', {
				class : 'col-md-3 user_auth',
				text: user_auth_name.val()
			});
			item.append(message_auth);
			item.append(user_auth);
		}
		chatWindow.append(item);
		chatWindow.animate({ scrollTop: chatWindow[0].scrollHeight}, 1);
		return item;
	}else{
		alert("Вам пришло новое сообщение - " + JSON.stringify(message));
	}
}

function incrementBidMessageCount(bId){
	var mNum = $("#message-count-" + bId);
	if (mNum.length){
		mNum.text(parseInt(mNum.text()) + 1);	
	}
}

function decrementBidMessageCount(bId){
	var mNum = $("#message-count-" + bId);
	if (mNum.length){
		mNum.text(parseInt(mNum.text()) - 1);	
	}
}

function incrementMessageNum(){
	var mNum = $(".message_num"); 
	if (mNum.length){
		mNum.text(parseInt(mNum.text()) + 1);	
	}
	
}

function decrementMessageNum(){
	var mNum = $(".message_num");
	if (mNum.length){
		mNum.text(parseInt(mNum.text()) - 1);
	}
}

function showActive(activeMembers) {
	renderActive(activeMembers.body);
	stompClient.send('/app/activeUsers', {}, '');
}

function renderActive(activeMembers) {
	var members = $.parseJSON(activeMembers);
	$('.active-status').addClass('offline').text('offline');
	$.each(members, function(index, value) {
		if (value === whoami) {
			return true;
		}
		var userLine = $('#user-' + jq(value));
		if (userLine.length > 0){
			userLine.find('.active-status').removeClass('offline').addClass('online').text('online');
		}		
	});
}

function newMessageIcon() {
	var newMessage = $('<span>', {
		class : 'newmessage'
	});
	newMessage.html('&#x2709;');
	return newMessage;
}

function sendMessageTo(user) {
	var chatInput = '#input-chat-' + jq(user);
	var message = $(chatInput).val();
	if (!message.length) {
		return;
	}
	var responseId = $("#responseList").find('.active').attr('name').substring(9);
	var bidId = $("[name='idBid']").val();
	stompClient.send("/app/chat", {}, JSON.stringify({
		'response' : responseId,
		'bid':	bidId,
		'recipient' : user,
		'message' : message
	}));
	$(chatInput).val('');
	$(chatInput).focus();
}

function getChatWindow(userName, rId) {
	var elementId = 'chat-' + userName;
	escapeElementId = jq(elementId);
	var containerId = escapeElementId + '-container-' + rId;
	var selector = '#' + containerId;
	return $(selector);
}

function createChatWindow(userName, rId) {
	var existingChats = $('.messages');
	var elementId = 'chat-' + userName;
	escapeElementId = jq(elementId);
	var containerId = escapeElementId + '-container-' + rId;
	var selector = '#' + containerId;
	var inputId = 'input-' + elementId;
	if (!$(selector).length) {
		inputId = 'input-' + elementId;
		var chatContainer = $('<div>', {
			id : containerId,
			class: 'chat-container'
		});
		var window_message = $('<div>', {
			class : 'window_message'
		});
		var window_text = $('<div>', {
			class : 'window_text'
		});
		chatContainer.append(window_message);
		chatContainer.append(window_text);
		
		var form = $('<form/>', {
			class : 'form_message'
		});
		window_text.append(form);
		var box = $('<div>', {
			class : 'box'
		});
		form.append(box);
		var row = $('<div>', {
			class : 'row'
		});
		var text = $('<div>', {
			class : 'col-md-9 col-sm-9'
		});
		row.append(text);
		var textarea = $('<textarea>', {
			placeholder : 'Ваше сообщение',
			id : inputId,
			type : 'text'
		});
		text.append(textarea);
		
		var btn = $('<div>', {
			class : 'col-md-3 col-sm-3'
		});
		row.append(btn);
		var send = $('<button>', {
			class : 'send',
			id : 'submit-' + elementId
		});
		send.click(function(event) {
			event.preventDefault();
			var user = event.currentTarget.id.substring(12); 
			sendMessageTo(user);
		});
		
		
		btn.append(send);
		var file = $('<button>', {
			class : 'file',
			text: 'Прикрепить'
		});
		//btn.append(file);
		
		var row2 = $('<div>', {
			class : 'row',
		});
		box.append(row);
		box.append(row2);
		//chatContainer.hide();
		existingChats.append(chatContainer);
		//var responseId = $('#user-' + jq(userName)).attr('name').substring(9);
		//getMassages(responseId);
	}
	return $(selector);
}

function showBidMessage(rId){
	var message = new Object();
	message.message = $("#response-bid-content-" + rId).text();
	message.timestamp = $("#response-time-" + rId).val();
	message.sender = $("[name='response-"+rId+"']").attr('id').substring(5);	//user-
	message.recipient = $("#response-station-login-" + rId).val();
	message.response = rId;
	message.id = "bid-" + rId;
	message.read = true;
	showMessage(message, false);
}

function showResponseMessage(rId){
	var message = new Object();
	message.message = $("#response-comment-" + rId).text();
	message.timestamp = $("#response-time-" + rId).val();
	message.sender = $("#response-station-login-" + rId).val();
	message.recipient = $("[name='response-"+rId+"']").attr('id').substring(5);	//user-
	message.response = rId;
	message.id = "response-" + rId;
	message.read = true;
	showMessage(message, false);
}

function getMassages(bidId){
	if (bidId != ''){
		var jqxhr = $.get("/response/messages?bidId=" + bidId).done(
				function(rMessages) {
					var item = createItem(rMessages.response);
					item.addClass('active');
					$("#responseList").append(item);
					$('.messages').show();
					createChatWindow(rMessages.response.clientLogin, rMessages.response.id);
					showBidMessage(rMessages.response.id);
					showResponseMessage(rMessages.response.id)
					if (rMessages.messages.length > 0){
						$.each(rMessages.messages, function(index, message) {
							showMessage(message, false);	
						});
					}
				}).fail(
				function() {
					alert("Не удалось получить ответы по выбранной заявке. "
							+ "Попробуйте позже или сообщите администартору");
				}).always(function() {
					// Not need
					});	
	}
}

function doRead(id, isDec){
	$("#message-"+id).removeClass("unread").addClass("read");
	if (isDec){
		decrementMessageNum();	
	}
}

function markMessage(id, bid){
	if (id != ''){
		var jqxhr = $.post("/message/mark", { 'id': id }).done(
				function(data) {
					if (data){
						doRead(id, true);
						decrementBidMessageCount(bid);
					}
					//mark
				}).fail(function() {
					// Not need
				}).always(function() {
					// Not need
				});	
	}
}