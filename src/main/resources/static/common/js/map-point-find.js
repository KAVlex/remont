
 $(document).ready(function() {
	          
	          $('#address').select2().on('change', function() {
	            	var point = $('#address').select2('val');
	            	if(point==null)return;
	           	    var data = point.split(' ');
	           	    $('#latitude').val(data[0]);
	           	    $('#longitude').val(data[1]);
	           	    var text = $("#address :selected").text();
	           	    $('input[name="address"]').val(text);
	          }).trigger('change');
	          
	          window.requestURL = 'https://geocode-maps.yandex.ru/1.x/?format=json&results=5'
	          $("#address").select2({language : 'ru',
	        	    minimumInputLength: 5,
	        	    placeholder: "Адрес",
	        	    allowClear: true,
	        	    ajax: {
	        	        url: window.requestURL,
	        	        dataType: 'json',
	        	        type: "GET",
	        	        quietMillis: 50,
	        	        data: function (term) { 
	        	            return {
	        	            	geocode: term.term
	        	            };
	        	        },
	        	        processResults:generateAddressList
	        	    }
	        	});
	          
	        });

   
   
   function generateAddressList(geoObjects){	   
      var currentObject;
      result = new Object();
      var list = new Array();
      try{
    	  geoObjects = geoObjects.response.GeoObjectCollection.featureMember;
          for(var i=0;i<geoObjects.length;i++){
        	  currentObject = geoObjects[i].GeoObject;
        	  list[i] = {id:currentObject.Point.pos,text:currentObject.metaDataProperty.GeocoderMetaData.text};
          }  
      }catch(e){
    	  
      }
      result.results = list;
      return result;
   }
   
   function fillOptionsForAddress(items){
	   $("#address").html('').select2({data: items}); 
   }
   