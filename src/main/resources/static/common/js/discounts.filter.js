/**
 * Created by Tensky on 23.12.2016.
 */
function stationChanged(form, discountsDivId) {
    var formId = form.id;
    var stationId = $('#' + formId).val();
    if (stationId == "" || stationId == null) {
        stationId = '-1';
    }

    var _csrf = $('body input[name=_csrf]').val();
    var formData = new FormData();

    formData.append("_csrf", _csrf);

    $.ajax({
        url: 'discounts/' + stationId,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            result=JSON.parse(result);
            console.log(result);
            var discountsDiv = document.getElementById(discountsDivId);
            while (discountsDiv.firstChild) {
                discountsDiv.removeChild(discountsDiv.firstChild);
            }

            for (var i = 0; i < result.length; i++) {
                var div = document.createElement('div');
                div.className = "discount";
                discountsDiv.appendChild(div);

                var img = document.createElement('img');
                img.src = '/images/?filename='+result[i].photo;
                img.onerror = function () {
                    this.src = '../common/images/discount.png';
                };
                div.appendChild(img);

                var a = document.createElement('a');
                a.href = "/station/" + result[i].station.id+'#discount'+result[i].id;
                a.innerHTML = result[i].station.name;
                div.appendChild(a);

                var h1 = document.createElement('h1');
                h1.innerHTML = result[i].title;
                div.appendChild(h1);

                var h2 = document.createElement('h2');
                h2.innerHTML = result[i].info;
                div.appendChild(h2);

                var h3 = document.createElement('h3');
                h3.innerHTML = "Акция действует с " + result[i].date_start + " по " + result[i].date_end;
                div.appendChild(h3);
            }
        },
        error: function (result) {
            alert(result);
        }
    });
}