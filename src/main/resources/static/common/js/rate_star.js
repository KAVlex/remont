/**
 * Created by Tensky on 23.12.2016.
 */

var recall_stars;
var srcs = [null,null,null,null,null];
var star_null='/autoservice/img/zvezda_null.png';
var star_1= '/autoservice/img/zvezda_1.png';
var star_over = '/autoservice/img/zvezda_over.png';
$(document).ready(function () {
    var recall_star1 = document.getElementById('recall_star1');
    var recall_star2 = document.getElementById('recall_star2');
    var recall_star3 = document.getElementById('recall_star3');
    var recall_star4 = document.getElementById('recall_star4');
    var recall_star5 = document.getElementById('recall_star5');
    recall_stars = [recall_star1,recall_star2,recall_star3,recall_star4,recall_star5];
    for(var i=0;i<5;i++)
    srcs[i]=recall_stars[i].src
    console.log("[recall_stars]:");
    console.log(recall_stars);
    $('#send-recall').on('hide.bs.modal',function () {
        emptyRecallForm();
    });
    $('#send-recall').on('show.bs.modal',function () {
       $('#recall_form input[name=bidId]') 
    });

    //BUG
    //fixme: ето жесть
    recall_stars[0].onmouseover = function () {
        rateStarOver('1');
    };
    recall_stars[0].onmouseout = function () {
        rateStarOut('1');
    };
    recall_stars[1].onmouseover = function () {
        rateStarOver('2');
    };
    recall_stars[1].onmouseout = function () {
        rateStarOut('2');
    };
    recall_stars[2].onmouseover = function () {
        rateStarOver('3');
    };
    recall_stars[2].onmouseout = function () {
        rateStarOut('3');
    };
    recall_stars[3].onmouseover = function () {
        rateStarOver('4');
    };
    recall_stars[3].onmouseout = function () {
        rateStarOut('4');
    };
    recall_stars[4].onmouseover = function () {
        rateStarOver('5');
    };
    recall_stars[4].onmouseout = function () {
        rateStarOut('5');
    };
});

function emptyRecallForm() {
    for(var i=0;i<5;i++)
    {
        recall_stars[i].src=star_null;
    }
    $('#recall_form input[name=rating]').val(0);
    $('#recall_form textarea[name=comment]').val(null);

}

function rateStarOver(rating) {
    for(var i=0;i<rating;i++)
    {
        srcs[i]=recall_stars[i].src;
        recall_stars[i].src=star_over;
    }
}

function rateStarOut(rating) {
    for(var i=0;i<rating;i++)
    {
        recall_stars[i].src=srcs[i];
    }
}

function rateClick(rating) {
    if (rating >= 1 && rating <= 5)
    {
        $('#recall_form input[name=rating]').val(rating);
        for(var i=0;i<rating;i++)
        {
            recall_stars[i].src=star_1;
            srcs[i]=recall_stars[i].src;
        }
        for(var i=rating;i<5;i++)
        {
            recall_stars[i].src=star_null;
            srcs[i]=recall_stars[i].src;
        }
    }
}
