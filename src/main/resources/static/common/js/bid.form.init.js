/**
 * Created by Tensky on 15.12.2016.
 */
$(document).ready(
    function () {
        $("#bid_carmark").select2({
            placeholder: "Марка",
            allowClear: true,
            width: "40%"
        });
        $("#bid_carmodel").select2({
            placeholder: "Модель",
            allowClear: false,
            width: "40%"
        });
        setMaxYear(document.getElementById('bid_year'));
        $("#bid_repairtypes").select2({
            placeholder: "Выберите виды работ",
            allowClear: true,
            width: "96%"
        });
        $("#bid_city").select2({
            placeholder: "Город",
            allowClear: true,
            width: "96%"
        });
    }
);
function setMaxYear(element) {
    var max=new Date().getFullYear();
    element.setAttribute("max",max);
}