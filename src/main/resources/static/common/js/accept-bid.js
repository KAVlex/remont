/**
 * Created by Tensky on 24.03.2017.
 */
$(document).ready(function () {
    $(document).on("click", ".rec", null, function () {
        var inputs = $('#modalClientAcceptStation').find('input');
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].name == "bidId") {
                inputs[i].value = $('#idBid').val();
            }
            else if (inputs[i].name == "stationLogin") {
                inputs[i].value = $("#responseList .active").attr("id").substring(5);
            }
        }
        $('#modalClientAcceptStation').modal('show');
    })
});

function acceptFromMap(bidId, statLog){
    var inputs = $('#modalClientAcceptStation').find('input');
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].name == "bidId") {
            inputs[i].value = bidId;
        }
        else if (inputs[i].name == "stationLogin") {
            inputs[i].value = statLog;
        }
    }
    $('#modalClientAcceptStation').modal('show');
}