function setTextAreaEditable() {
    var textarea = document.getElementById('infotext');
    var attrRO = textarea.getAttribute("readonly");
    var attrD = textarea.getAttribute("disabled");
    var save = document.createElement("button");
    textarea.appendChild(save);
    if (attrRO == "readonly") {
        textarea.style.border = "1px solid #A4C400";
        textarea.style.resize = "vertical";
        textarea.style.overflow = "scroll";
        textarea.removeAttribute(attrRO);
        textarea.removeAttribute(attrD)
    }
    else {
        textarea.setAttribute("readonly", "readonly");
        textarea.style.border = 'none';
        textarea.style.resize = 'none';
        textarea.style.overflow = 'hidden';
    }
}

function clientCarEdit(carId) {
    var formData = new FormData();
    var _csrf = $('#car' + carId + ' input[name=_csrf').val();
    var color = $('#edit-car-fields-' + carId + ' input[name=color]').val();
    var regNumber = $('#edit-car-fields-' + carId + ' input[name=regNumber]').val();
    var mileage = $('#edit-car-fields-'+carId+' input[name=mileage]').val();
    //
    console.log(color);
    console.log(regNumber);
    console.log(_csrf);
    //
    formData.append('carID', carId);
    formData.append('color', color);
    formData.append('regNumber', regNumber);
    formData.append('mileage',mileage);
    formData.append('action', 'edit');
    formData.append('_csrf', _csrf);

    $.ajax({
        url: '/mycars',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'text',
        success: function (result) {
            location.reload();//временно
        },
        error: function (result) {
            alert(result.textContent);
        }
    });
}

function clientCarDelete(carId) {
    var formData = new FormData();
    var _csrf = $('#car' + carId + ' input[name=_csrf]').val();
    formData.append('carID', carId);
    formData.append('action', 'delete');
    formData.append('_csrf', _csrf);
    //
    console.log(carId);
    console.log(_csrf);
    console.log(formData);

    //
    $.ajax({
        url: '/mycars',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'text',
        success: function (result) {
            var carDiv = document.getElementById('car' + carId);
            carDiv.parentNode.removeChild(carDiv);
        },
        error: function (result) {

        }
    });
}

function carDelete(formID) {
    console.log("[carDelete]: button presed");
    console.log("[carDelete]: formID: " + formID);
    var _csrf = $('#' + formID + ' input[name=_csrf').val();
    var form = document.getElementById(formID);
    console.log("[carDelete]: _csrf: " + _csrf);
    var carID = $('#' + formID + ' input[name=carID').val();
    console.log("[carDelete]: carID: " + carID);
    var formdata = new FormData();
    formdata.append("action", "delete");
    formdata.append("_csrf", _csrf);
    formdata.append("carID", carID);

    $.ajax({
        url: "/mycars",
        type: "post",
        contentType: false,
        processData: false,
        cache: false,
        data: formdata,
        success: function (msg) {
            form.parentNode.removeChild(form);
        },
        error: function (msg) {
            console.log("[error]: " + msg.responseText);
        }
    })
}

function discountEdit(value, formID) {
    console.log("[discountEdit]: button pressed");
    console.log("[discountEdit]: formID: " + formID);
    console.log("[discountEdit]: discount.id (var value): " + value);
    var _csrf = $('#' + formID + ' input[name=_csrf]').val();
    console.log("[discountEdit]: _csrf: " + _csrf);
    var formData = new FormData();
    formData.append('_csrf', _csrf);
    formData.append('action', 'edit');
    formData.append('discountID', value);
    $.ajax(
        {
            url: "/mycabinet/editDiscount",
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            // data:{
            //     "action":"edit",
            //     "_csrf":_csrf,
            //     "discountID":value
            // },
            data: formData,
            success: function (msg) {
                console.log("[discountEdit.success]: message: " + msg);
            }
        }
    )
}

function discountDelete(value, formID) {
    console.log("[discountDelete]: formID: " + formID);
    console.log("[discountDelete]: discount.id (var value): " + value);
    // var _csrf = $('#' + formID + ' input[name=_csrf]').val();
    var _csrf = $('#addDiscount input[name=_csrf]').val();
    console.log("[discountEdit]: _csrf: " + _csrf);
    var formData = new FormData();
    formData.append('action', 'delete');
    formData.append('_csrf', _csrf);
    formData.append('discountId', value);
    $.ajax({
        url: '/mycabinet/editDiscount',
        type: 'post',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        success: function () {
            var element = document.getElementById(formID);
            element.parentNode.removeChild(element);

        }
    })
}

function clientAddCar() {
    var _csrf = $('#newCar input[name=_csrf]').val();
    var markId = $('#bid_carmark').val();
    var modelId = $('#bid_carmodel').val();
    var carYear = $('#newCar input[name=year]').val();
    var color = $('#newCar input[name=color]').val();
    var regNumber = $('#newCar input[name=regNumber]').val();
    var mileage = $('#newCar input[name=mileage]').val();
    //
    // console.log(markId);
    // console.log(modelId);
    // console.log(carYear);
    // console.log(color);
    // console.log(regNumber);
    // console.log(_csrf);
    //
    var formData = new FormData();
    formData.append('carMarkID', markId);
    formData.append('carModelID', modelId);
    formData.append('year', carYear);
    formData.append('color', color);
    formData.append('regNumber', regNumber);
    formData.append('mileage',mileage);
    formData.append('action', 'add');
    formData.append('_csrf', _csrf);
    //
    $.ajax({
        url: '/mycars',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'text',
        success: function (result) {
            location.reload();
        },
        error: function (result) {
            alert(result.responseText);
        }
    });
}

function carAdd(formID, parentID) {
    var _csrf = $('#' + formID + ' input[name=_csrf]').val();
    console.log(formID);

    var formData = new FormData();
    formData.append('action', 'add');
    formData.append('_csrf', _csrf);
    formData.append('carMarkID', $('#' + formID + ' select[name=carMarkID]').val());
    formData.append('carModelID', $('#' + formID + ' select[name=carModelID]').val());
    formData.append('year', $('#' + formID + ' input[name=year]').val());
    formData.append('color', $('#' + formID + ' input[name=color]').val());
    formData.append('regNumber', $('#' + formID + ' input[name=regNumber]').val());
    formData.append('mileage', $('#'+formID + ' input[name=regNumber').val());
    $.ajax({
        url: "/mycars",
        contentType: false,
        processData: false,
        cache: false,
        type: "POST",
        data: formData,
        success: function (car) {
            console.log(car);
            var div = document.getElementById('car' + car.id);
            if (div == null) {
                add(parentID, car);
            }
            else {
                edit(div, car)
            }
        },
        error: function (message) {
            message = message.responseText.toString();
            alert(message);

        }
    });

    function add(parentID, car) {
        var parentForm = document.getElementById(parentID);
        var div = document.createElement('div');
        div.className = "carblock";
        div.id = 'car' + car.id;

        var deleteButton = document.createElement('button');
        deleteButton.value = 'Удалить';
        deleteButton.className = 'deleteButton';
        deleteButton.onclick = function () {
            carDelete(div.id);
        };
        div.appendChild(deleteButton);

        var img = document.createElement('img');
        img.src = '../common/images/' + car.mark.name + '.png';
        img.onerror = function () {
            this.src = 'http://placehold.it/116x116';
        };
        div.appendChild(img);

        var divMark = document.createElement('div');
        divMark.textContent = 'Марка: ' + car.mark.name;
        div.appendChild(divMark);

        var divModel = document.createElement('div');
        divModel.textContent = 'Модель: ' + car.model.name;
        div.appendChild(divModel);

        var divYear = document.createElement('div');
        divYear.textContent = 'Год: ' + car.year.year;
        div.appendChild(divYear);

        var divColor = document.createElement('div');
        divColor.textContent = 'Цвет: ' + car.color;
        div.appendChild(divColor);

        var divRegNumber = document.createElement('div');
        divRegNumber.textContent = 'Регистрационный номер: ' + car.regNumber;
        div.appendChild(divRegNumber);

        var divMileage = document.createElement('div');
        divMileage.textContent = 'Пробег: '+ (car.mileage==null?'Не указан':car.mileage);
        div.appendChild(divMileage);

        var hiddenID = document.createElement('input');
        hiddenID.type = 'hidden';
        hiddenID.name = 'carID';
        hiddenID.value = car.id;
        div.appendChild(hiddenID);

        var _csrf = document.createElement('input');
        _csrf.type = 'hidden';
        _csrf.name = '_csrf';
        _csrf.value = $('#addCar input[name=_csrf]').val();
        div.appendChild(_csrf);

        parentForm.appendChild(div);
    }


    function edit(div, car) {

    }

}

function orderStateChanged(whatChanged, orderId) {
    var formData;
    switch (whatChanged) {
        case "call":
        {
            formData = orderCallStateChanged(orderId);
            break;
        }
        case "email":
        {
            formData = orderEmailStateChanged(orderId);
            break;
        }
    }
    $.ajax({
        url: '/mycabinet/changeOrderState',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'text',
        success: function (result) {
            console.log("[orderStateChanged]: success");
        },
        error: function (result) {
            alert(result.responseText);
        }
    });

    function orderCallStateChanged(callOrderId) {
        var _csrf = $('#order-calls input[name=_csrf]').val();
        var formData = new FormData();
        formData.append('callOrderId', callOrderId);
        formData.append('table', 'orderCall');
        formData.append('_csrf', _csrf);
        return formData;
    }

    function orderEmailStateChanged(emailOrderId) {
        var _csrf = $('#order-emails input[name=_csrf]').val();
        var formData = new FormData();
        formData.append('emailOrderId', emailOrderId);
        formData.append('table', 'orderEmail');
        formData.append('_csrf', _csrf);
        return formData;
    }
}

function deletePhotoFromGallery(photoId) {
    var _csrf = $('#galleryPhoto' + photoId + ' input[name=_csrf]').val();
    var formData = new FormData();
    formData.append('photoId', photoId);
    formData.append('_csrf', _csrf);
    $.ajax({
        url: '/mycabinet/deleteFromGallery',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'text',
        success: function (result) {
            var div = document.getElementById('galleryPhoto' + photoId);
            div.parentNode.removeChild(div);
        },
        error: function (result) {
            alert(result.responseText);
        }
    });
}

function modalDiscountEdit(discountId) {
    var fid = '#discount' + discountId + ' ';
    console.log("[modalDiscountEdit]: fid: " + fid);
    try {
        console.log($(fid + 'input[name=title]').val());
        $('#modal-discount input[name=title]').val($(fid + 'input[name=title]').val());

        console.log($(fid + 'input[name=info]').val());
        $('#modal-discount textarea[name=info]').val($(fid + 'input[name=info]').val());

        console.log($(fid + 'input[name=discountId').val());
        $('#modal-discount input[name=discountId]').val($(fid + 'input[name=discountId').val());

        console.log($(fid + 'input[name=date_start').val());
        var date_start = new Date($(fid + 'input[name=date_start').val());
        date_start = dateToyyyyMMdd(date_start);
        console.log("[date_start]: " + date_start);
        $('#modal-discount input[name=date_start]').val(date_start);

        console.log($(fid + 'input[name=date_end').val());
        var date_end = new Date($(fid + 'input[name=date_end').val());
        date_end = dateToyyyyMMdd(date_end);
        console.log("[date_end]: " + date_end);
        $('#modal-discount input[name=date_end]').val(date_end);

        console.log($('#modal-discount input[name=photo]').val(null));
        $('#modal-discount input[name=photo]').val(null);
        console.log();
        $('#modal-discount').modal('show');
    } catch (err) {
        $('#modal-discount').modal('hide');
        $('#modal-discount input[name=discountID]').val(null);
        $('#modal-discount input[name=station]').val(null);
        $('#modal-discount input[name=title]').val(null);
        $('#modal-discount textarea[name=info]').val(null);
        $('#modal-discount input[name=date_start]').val(null);
        $('#modal-discount input[name=date_end]').val(null);
        $('#modal-discount input[name=photo]').val(null);
        console.log("[modal-discount]: " + err);
    }
}

function modalSendRecall(bidId) {
    $('#send-recall').modal('show');
    $('#recall_form input[name=bidId]').val(bidId);
}

function dateToyyyyMMdd(date) {
    var d = new Date(date);
    var separator = '-';
    var day = d.getDate();
    if (day < 10)
        day = '0' + day;
    var month = d.getMonth() + 1;
    if (month < 10)
        month = '0' + month;
    var year = d.getFullYear();
    return new String(year + separator + month + separator + day);
}

function modalShowMyRecall(rId) {
    var myrecall = document.getElementById('my-recall');
    while (myrecall.firstChild)
        myrecall.removeChild(myrecall.firstChild);
    var _csrf = $('#archivebids input[name=_csrf]').val();
    var formData = new FormData();
    formData.append('_csrf', _csrf);
    $.ajax({
        url: '/mycabinet/showMyRecall?recallId=' + rId,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            console.log(result);
            myrecall.insertAdjacentHTML('beforeend',result);
            $('#show-my-recall').modal('show');
        },
        error: function (result) {
            alert(result.textContent)
        }
    });
}

function galleryValidation() {
    if($('#galleryAddForm input[name=photo]').val().length > 0)
        $('#galleryAddForm button[type=submit]').prop('disabled',false);
    else
        $('#galleryAddForm button[type=submit]').prop('disabled',true);
}

function clientCancelBid(bidId){
    $('#modalClientCancelBid input[name=bidId]').val(bidId);
    $('#modalClientCancelBid').modal('show');
}

function clientRestoreBid(bidId) {
    $('#modalClientRestoreBid input[name=bidId]').val(bidId);
    $('#modalClientRestoreBid').modal('show');
}

function stationCloseBid(sBidId) {
    $('#modalStationCloseBid input[name=sBidId]').val(sBidId);
    $('#modalStationCloseBid').modal('show');
}

function stationCancelDirectBid(sBidId)
{
    $('#modalStationCancelDirectBid input[name=sBidId]').val(sBidId);
    $('#modalStationCancelDirectBid').modal('show');
}

function stationCancelActiveBid(sBidId) {
    $('#modalStationCancelActiveBid input[name=sBidId]').val(sBidId);
    $('#modalStationCancelActiveBid').modal('show');
}

function stationRestoreBid(sBidId) {
    $('#modalStationRestoreBid input[name=sBidId]').val(sBidId);
    $('#modalStationRestoreBid').modal('show');
}