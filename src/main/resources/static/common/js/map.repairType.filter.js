/**
 * Created by Tensky on 22.11.2016.
 */
/**
 * Получает id выбранных видов ремонта из формы с id <code>selectFormID</code>
 * @param selectFormID id формы фильтра
 */
function getFilter(selectFormID) {
    var selectForm = document.getElementById(selectFormID);
    $.ajax(
        {
            url: '/map/getFilter',
            type: 'GET',
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'text',
            success: function (data) {
                console.log(JSON.parse(data));
                var json = JSON.parse(data);
                var disOpt=document.createElement('option');
                
                for(var i=0;i<json.length;i++)
                {
                    console.log("[getFilter]: "+json[i].name);
                    var option=document.createElement('option');
                    option.id="option-id!"+i;
                    option.text=json[i].name;
                    option.value=json[i].id;
                    selectForm.append(option);
                }
            },
            fail: function (data) {
                alert("FAIL: " + data);
            },
            error: function (data) {
                alert("ERROR: " + data.toString());
            }
        }
    )
}

function sendFilter() {
    var data = $('#filter').val();
    console.log("[sendFilter]: data is "+data);
    for(var i=0;i<data.length;i++)
    {
        if(data[i]=="")
            data.remove(i);
    }
    if(data.length==0){
        getAllStationBalloons();
        return;
    }
    console.log("[sendFilter]: data is "+data);

    var _csrf = $('body input[name=_csrf]').val();
    var formData = new FormData();

    formData.append('_csrf',_csrf);
    formData.append("repairTypes",data);
    $.ajax({
        url: '/map/sendFilter',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'text',
        success: function (result) {
           filterBalloons(result);
        },
        error: function (result) {
            alert('Ошибка при обработке фильтра, информация выведена в консоль');
            console.log(result);
            }
        });
}