$(document).ready(function() {
	  var locked = $('#stations tr');
	  locked.each(function() {
		  var clazz = 'active';
		  tr = this;
		  if(tr.cells[0].nodeName.toLowerCase()=='th')return;
		  
		  var balance = tr.cells[3].innerText;
		  var balance = parseFloat(balance);
		  if(balance<=0){
			  clazz = 'warning'
		  }
		  var  locked = tr.cells[0].children[1].value;
		  
		  if(locked=='true'){
			  clazz='danger';
		  }
		  $( this ).addClass( clazz );
	  });
});