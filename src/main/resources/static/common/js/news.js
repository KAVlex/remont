/**
 * Created by Tensky on 17.01.2017.
 */
var nextPage = 1;
var _csrf;
var nextPageButton;
var container;

$(document).ready(function () {
    _csrf = $('#_csrf').val();
    container = document.getElementById('container');
    nextPageButton = document.getElementById('nextPageButton');
});

function getMonthAsString(month) {
    switch (month) {
        case 0:
            return 'янв';
        case 1:
            return 'фев';
        case 2:
            return 'мар';
        case 3:
            return 'апр';
        case 4:
            return 'май';
        case 5:
            return 'июн';
        case 6:
            return 'июл';
        case 7:
            return 'авг';
        case 8:
            return 'сен';
        case 9:
            return 'окт';
        case 10:
            return 'ноя';
        case 11:
            return 'дек';
        default:
            console.log("[getMonthAsString]: input month number is: " + month);
    }
}

function getNextPageNews() {
    var formData = new FormData();
    formData.append('_csrf', _csrf);
    $.ajax({
        url: 'news/nextportion/' + nextPage,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            if (result.last)
                nextPageButton.setAttribute('hidden', 'hidden');

            result = result.content;
            var place = document.getElementById('container');
            console.log(place);

            var mainDiv = document.createElement('div');
            mainDiv.className = 'page articles_padding';
            container.appendChild(mainDiv);

            var divRow = document.createElement('div');
            divRow.className = 'row';
            mainDiv.appendChild(divRow);

            for (var i = 0; i < result.length; i++) {
                var divNews = document.createElement('div');
                divNews.className = 'col-md-3';
                divRow.appendChild(divNews);

                var divItem = document.createElement('div');
                divItem.className = 'item';
                divNews.appendChild(divItem);

                var divImages = document.createElement('div');
                divImages.className = 'images';
                divItem.appendChild(divImages);

                var aImage = document.createElement('a');
                aImage.href = '/news/view/' + result[i].id;
                divImages.appendChild(aImage);

                var img = document.createElement('img');
                if (result[i].preview_image != null)
                    img.setAttribute('src', result[i].preview_image.pathMedium);
                else
                    img.setAttribute('src','/common/images/press.jpg');
                img.width = '256';
                img.height = '256';
                img.onerror = function () {
                    this.src = '../common/images/news.png';
                };
                aImage.appendChild(img);

                var dateDiv = document.createElement('div');
                dateDiv.className = 'date';
                var postDate = new Date(result[i].timestamp);

                console.log(postDate);

                dateDiv.textContent = postDate.getDay() + '.'
                    + getMonthAsString(postDate.getMonth()) + '.'
                    + postDate.getYear() + ' ' + postDate.getHours() + ':' + postDate.getMinutes();
                divItem.appendChild(dateDiv);

                var divTitle = document.createElement('div');
                divTitle.className = 'title';
                divTitle.align = 'justify';
                divItem.appendChild(divTitle);

                var aTitle = document.createElement('a');
                aTitle.href = '/news/view/' + result[i].id;
                aTitle.textContent = result[i].title.length > 100 ? result[i].title.substr(0, 96).toUpperCase() + '...' : result[i].title.toUpperCase();
                divTitle.appendChild(aTitle);
                divRow.appendChild(divNews);
            }
            nextPage++;
        },
        error: function (result) {
            console.log(result);
        }
    });
}