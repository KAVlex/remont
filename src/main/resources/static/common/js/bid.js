// /**
//  * Created by Tensky on 08.12.2016.
//  */
//
// /**
//  * Изменяет опции моделей ТС, в зависимости от выбранной марки
//  * @param markFieldID
//  * @param modelFieldID
//  */
// function markChanged() {
//     // var input_carmark = document.getElementById('input_carmark');
//     var cm = document.getElementById('input_carmodel');
//     var mf = document.getElementById('model');
//     // var y=document.getElementById('input_year');
//     // y.value='';
//     // input_carmark.value = $('#mark').val();
//     // cm.value = '';
//     $('#model').empty();
//
//     var disOpt = document.createElement('option');
//     disOpt.value = '';
//     disOpt.text = '';
//     disOpt.selected = 'selected';
//
//     $('#model').select2({placeholder: 'Модель', width: '30%'});
//     // $("#bid_year").val("").trigger("change");
//     mf.appendChild(disOpt);
//     $.ajax({
//         url: 'getModels?markID=' + $('#mark').val(),
//         contentType: false,
//         processData: false,
//         cache: false,
//         dataType: 'json',
//         success: function (data) {
//
//             for (var i = 0; i < data.length; i++) {
//                 var option = document.createElement('option');
//                 option.value = data[i].id;
//                 option.text = data[i].name;
//                 mf.appendChild(option);
//             }
//         }
//     });
// }
//
// function modelChanged() {
//     // var input_carmodel = document.getElementById('input_carmodel');
//     // console.log(input_carmodel);
//     // input_carmodel.value = $('#model').val();
//     // console.log($('#' + modelFieldID).val());
// }
//
// function submitBid() {
//     var markID = $('#mark').val();
//     var modelID = $('#model').val();
//     var year = $('#bid_year').val();
//     var desiredDate=$('#bid_desired_date').val();
//     var city = $('#bid_city').val();
//     var comment =$('#bid_comment').val();
//     var firstName =$('#bid_firstName').val();
//     var lastName=$('#bid_lastName').val();
//     var telephoneNumber = $('#bid_telephonenumber');
//     var stationID = $('#stationID').val();
//     var _csrf = $('#_csrf').val();
//
//     var formData = new FormData();
//     formData.append("markID",markID);
//     formData.append("modelID",modelID);
//     formData.append("year",year);
//     formData.append("desired_date",desiredDate);
//     formData.append("city",city);
//     formData.append("comment",comment);
//     formData.append("firstName",firstName);
//     formData.append("lastName",lastName);
//     formData.append("telephoneNumber",telephoneNumber);
//     formData.append("_csrf",_csrf);
//
//     $.ajax({
//         url: "new-bid/submit",
//         type: 'post',
//         data: formData,
//         success: function () {
//             alert("Заявка принята");
//         }
//     })
// }

function markChanged(selectMarkId, selectModelId) {
    var markId = $('#' + selectMarkId).val();
    $('#' + selectModelId).empty();
    var _csrf = $('body input[name=_csrf]').val();
    console.log($('body input[name=_csrf]').val());

    $.ajax({
        url: '/getModels?markID=' + markId + '&_csrf=' + _csrf,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: 'none',
        dataType: 'json',
        success: function (models) {
            var selectModel = document.getElementById(selectModelId);
            for (var i = 0; i < models.length; i++) {
                var option = document.createElement('option');
                option.value = models[i].id;
                option.text = models[i].name;
                selectModel.appendChild(option);
            }
        },
        error: function (models) {
            alert("Can't load models from server");
        }
    });
}

function createBid() {
    var markId = $('#mark').val();
    var modelId = $('#model').val();
    var year = $('#year').val();
    var comment = $('#type_work').val();
    var date = $('#main_form text[name=desired-date]').val();
    console.log(markId);
    console.log(modelId);
    console.log(year);
    console.log(comment);
    console.log(date);
    date = $('#main_form text[name=desired-date]').text();
    console.log(date);
}

$(document).ready(function () {
    var validRules = {
        rules: {
            mark: {
                required: true,
            },
            model: {
                required: true,
            },
            year: {
                required: true,
            },
            type_work: {
                required: true,
                minlength: 10,
                maxlength: 255,
            },
            desired_date: {
                required: false,
                pattern: /(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))/,
            },
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true,
            },
            phone: {
                required: true,
                pattern: /9[0-9]{9}/,
                minlength: 10,
                maxlength: 10
            },
        },
        messages: {
            mark: {
                required: "Поле \"Марка\" является обязательным",
            },
            model: {
                required: "Поле \"Модель\" является обязательным",
            },
            year: {
                required: "Поле \"Год выпуска\" должно быть заполнено",
            },
            type_work: {
                required: "Поле \"Тип работ\" должно быть заполнено",
                minlength: "минимальная длинна должна быть 10 символов",
                maxlength: "максимальная длинна не может быть больше 255 символов",
            },
            desired_date: {
                required: "Поле \"Предполагаемая дата проведения работ\" должно быть заполнено",
                pattern: "Недопустимый формат",
            },
            name: {
                required: "Поле \"Имя\" является обязательным",
                minlength: "минимальная длинна должна быть более 2 символов",
            },
            email: {
                required: "Поле \"email\" является обязательным",
                email: "пожалуйста введите корректный адрес электронной почты",
            },
            phone: {
                required: "Поле \"Телефон\" должно быть заполнено",
                minlength: "минимальная длинна должна быть 10 символов",
                maxlength: "максимальная длинна должна быть 10 символов",
                pattern: "Недопустимый формат"
            }
        }
    };
    $("#main_form").validate(validRules);
    if ($('#filter').length > 0) {
        $('#filter').select2({
            allowClear: true,
            placeholder: 'Виды ремонтных работ'
        });
    }
});

function stationCategoryChanged() {
    var stringCategory = $('#stationCategory').val();
    var $mainForm = $('#main_form');
    var textarea = $mainForm.find('textarea')[0];
    own_parts_div(false);
    need_evac(true);
    file_input(false);
    switch (stringCategory) {
        case "Автосервис":
        {
            textarea.setAttribute('placeholder', 'Например: Замена масла, замена ремня ГРМ...');
            own_parts_div(true);
            break;
        }
        case "Автомойка":
        {
            textarea.setAttribute('placeholder', 'Например: Стандартная мойка, химчистка салона, Полировка...');
            break;
        }
        case "Автоэкспертиза":
        {
            textarea.setAttribute('placeholder', 'Например: Требуется провести независимую экспертизу после ДТП; Требуется помощь в выборе автомобиля...')
            //todo - Добавить форму “Тип экспертизы” (Выездная; У офиса – не помню как называется)
            break;
        }
        case "Автомагазин":
        {
            textarea.setAttribute('placeholder', 'Например: Шаровая опора – 2 шт; Рулевая рейка - 1шт;...');
            need_evac(false);
            break;
        }
        case "Кузовной ремонт":
        {
            textarea.setAttribute('placeholder', 'Например: Покраска водительской двери; Выправление вмятины на заднем правом крыле...');
            file_input(true);
            break;
        }
        case "Тюнинг ателье":
        {
            textarea.setAttribute('placeholder', 'Например: Пошив салона, изменение выхлопной системы, чип-тюнинг, тюнинг оптики...');
            break;
        }
        case "Шиномонтаж":
        {
            textarea.setAttribute('placeholder','Напримеп: Прокатка диска – 4 шт; Аргоновая сварка – 2шт;...');
            need_evac(false);
        }
        case "Эвакуатор":
        {
            textarea.setAttribute('placeholder','Напримеп: Прокатка диска – 4 шт; Аргоновая сварка – 2шт;...');
        }
    }
}

function own_parts_div(visible) {
    var $ownPartsDiv = $('#own_parts_div');
    var cb = $ownPartsDiv.find('input')[0];
    if (visible) {
        $ownPartsDiv.attr('hidden', null);
    } else {
        $ownPartsDiv.attr('hidden', 'hidden');
    }
    cb.checked = false;
    cb.value = null;
}

/**
 * 
 * @param evac true - evac, false - shipping
 */
function need_evac(evac)
{
    var evacuatorLabel = $('#evacuator_label')[0];
    var evacuator = $('#evacuator')[0];
    evacuator.value=null;
    evacuator.checked = false;
    if(evac){
        evacuatorLabel.textContent = 'Требуется эвакуатор';
        evacuator.name='need-evacuator';
    }else{
        evacuatorLabel.textContent = 'Требуется доставка';
        evacuator.name='need-shipment';
    }
}

function file_input(visible)
{
    var $imageDiv = $('#image_div');
    $('#image_div input[name=image]').val(null);
    $imageDiv.value = null;
    if(visible)
        $imageDiv.attr('hidden',null);
    else $imageDiv.attr('hidden','hidden');
}