/**
 * Created by Tensky on 14.12.2016.
 */
function carDelete(carID,formID) {
    var _csrf= $('#'+formID+' input[name=_csrf').val();
    var formData = new FormData();
    formData.append("action","delete");
    formData.append("carID",carID);
    formData.append("_csrf",_csrf);
    $.ajax({
        url: "/mycars/",
        type: "POST",
        contentType: false,
        processData: false,
        cache:false,
        data: formData,
        success: function () {
            var element = document.getElementById(formID);
            element.parentNode.removeChild(element);
        }
    })
}
