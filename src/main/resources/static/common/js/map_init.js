/**
 * Edited by Tensky.
 */

// ymaps.ready(function () {
//     init("station"/"response")
// });
// Использовать на странице с картой
// дополнять строкой:
//    station - для того, чтобы выводить с самого начала баллуны станций
//    response - выводить с самого начала балуны с ответами
var myMap, objectManager;

function init(type) {

    console.log("[init]: Изменение размеров в соответствии с размерами окна");
    console.log("[init]: Инициализация...");

    myMap = new ymaps.Map('map', {
        //Дефолтные значения для отображения центра карты
        //53.198597, 45.001237 - Пенза
        center: [53.198597, 45.001237],
        //Дефолтный зум
        zoom: 10,
        controls: []
        //Убирает все элементы управления Яндекс.Карт
        //Для отображения стандартных элементов просто закомментировать (удалить) эту строку
        //Набор элементов управления:
        // controls: ['smallMapDefaultSet'] - набор кнопок, предназначенный для маленьких (менее 300 пикселей в ширину) карт
        // controls: ['largeMapDefaultSet'] - набор кнопок, предназначенный для больших (более 1000 пикселей в ширину) карт.
        // controls: [] //-вот это можно убрать, тогда будет просто карта без элементов управления
    }, {
        searchControlProvider: 'yandex#search'
    });
    objectManager = new ymaps.ObjectManager({
        //Включить кластеризацию меток
        clusterize: true,
        gridSize: 32
    });

    myMap.controls.add('geolocationControl'); //- добавление на карту кнопки геолокации
    //myMap.controls.add('searchControl');      - добавление на карту поисковой строки:
    myMap.controls.add('routeEditor');        //- добавление на карту редактора маршрута
    myMap.controls.add('trafficControl');     //- добавление на карту элемента управления «Пробки»
    myMap.controls.add('typeSelector');       //- добавление на карту переключателя типа карты (в выпадающем списке будут отображены все допустимые типы)
    myMap.controls.add('fullscreenControl');  //- добавление на карту кнопки переключения полноэкранного режима
    myMap.controls.add('zoomControl');        //- добавление на карту панели изменения масштаба
    myMap.controls.add('rulerControl');       //- добавление на карту элементов управления для измерения расстояний

    console.log("[init]: Карта инициализирована.");
    console.log("[init]: Установка пресетов меток и кластеров");

    //Задание пресетов для кластеров и меток на карте
    //В данном случае метки будут выводиться как greenDotIcon (зеленая метка),
    //а кластер как greenClusterIcon (пончик зеленый)
    // objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    // objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
    //
    // console.log("[init]: Пресеты установлены");
    // console.log("[init]: Отправка запроса для получения json всех меток");
    getUserLocation(true);
    switch (type) {
        case "station":
        {
            objectManager.objects.options.set('preset', 'islands#greenDotIcon');
            objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
            myMap.geoObjects.add(objectManager);
            getAllStationBalloons();
            break;
        }
        case "response":
        {
            // objectManager.objects.options.set('preset', 'islands#greenStretchyIcon');
            // objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
            myMap.geoObjects.add(objectManager);
            getResponsesByBidId();
            break;
        }
        default:
        {
            objectManager.objects.options.set('preset', 'islands#greenDotIcon');
            objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');

            alert(">>Map.init(type): указан неизвестный тип type=\'" + type + "\'");
        }

            console.log("[init]: Пресеты установлены");
            console.log("[init]: Отправка запроса для получения json всех меток");
    }
}

function getAllStationBalloons() {
    objectManager.removeAll();
    $.ajax({
        url: "/map/getAllStationBalloons"
    }).done(function (data) {
        console.log("[init]: Данные получены:");
        console.log(data);
        console.log("[init]: Добавление меток на карту...");
        objectManager.add(data);
        console.log("[init]: Метки добавлены.");
    });
}

function filterBalloons(data) {
    console.log("[fitlerBalloons]: Удаление всех меток с карты...");
    objectManager.removeAll();
    console.log("[filterBalloons]: Метки удалены.");
    getUserLocation(false);
    console.log("[filterBalloons]: Добавление меток в соответствии с фильтром...");
    if (data.length < 10)
        return;
    objectManager.add(data);
    console.log("[filterBalloons]: Метки добавлены.");

}

function getUserLocation(autoApply) {
    console.log("[getUserLocation]: Определение местоположения пользователя");
    ymaps.geolocation.get({

        // Определять местоположение будет браузер, т.к. yandex смог определить только центр города
        // Доступные опции для provider: 'browser' - встроенная браузерная геолокация,
        //                               'yandex'  - геолокация по данным Яндекса на основе ip пользователя,
        //                               'auto'    - провести геолокацию всеми доступными способами
        //                                           и выбрать лучшее значение
        provider: 'browser'
        // mapStateAutoApply: true

        // Автоматическая центрация карты по положению пользователя
        //mapStateAutoApply: autoApply

    }).then(function (result) {

        console.log("[getUserLocation]: Местоположение определено.");
        console.log("[getUserLocation]: Настройка метки пользователя...");

        //Настройки метки с местоположением пользователя
        //Настройка иконки
        result.geoObjects.options.set('preset', 'islands#redCircleIcon');

        //Настройка содержимого баллуна
        result.geoObjects.get(0).properties.set(
            {
                balloonContentBody: 'Ваше местоположение'
            }
        );

        console.log("[getUserLocation]: Добавление метки пользователя на карту...");

        //Добавление метки на карту
        myMap.geoObjects.add(result.geoObjects);

        console.log("[getUserLocation]: Метка добавлена.");
    });
}

function getResponsesByBidId() {
    console.log("[getResponsesByBidId]: Удаление предыдущих меток с карты");
    myMap.geoObjects.removeAll();
    getUserLocation();
    var formData = new FormData();
    var bidId = $('#selectResponse').val();
    if (bidId == '-1' || bidId == 'null' || bidId == '') {
        return;
    }
    var _csrf = $('#responsemapbids input[name=_csrf]').val();
    formData.append('_csrf', _csrf);
    $.ajax({
        url: '/mycabinet/map/responses/' + bidId,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        success: function (result) {
            if (result == null || result == "")
                return;
            console.log(result);
            console.log("[init]: Данные получены");
            console.log("[init]: Вывод в виде объекта:");
            console.log(result);
            console.log("[init]: Добавление меток на карту...");

            // objectManager.add(result);
            try {
                console.log(JSON.parse(result));
            }
            catch (err) {
                console.log(err);
            }
            console.log(result);
            for (var i = 0; i < result[0].features.length; i++) {

                var geoObject = new ymaps.GeoObject(result[0].features[i], result[1]);

                console.log("[geoObject]:");
                console.log(geoObject);
                myMap.geoObjects.add(geoObject);
            }

            console.log("[init]: Метки добавлены");
        },
        error: function (result) {

        }
    });
}