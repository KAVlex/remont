/**
 * Created by Tensky on 15.12.2016.
 */

var callOrderForm;

function showCallOrderForm(buttonElement) {
    callOrderForm=document.createElement('div');
    callOrderForm.id="callOrderForm";
    // buttonElement.onclick="hideCallOrderForm(this)";
    buttonElement.onclick= function () {
        hideCallOrderForm(buttonElement);
    };

    var name = document.createElement('input');
    name.placeholder='Ваше имя';
    name.setAttribute("required","required");
    // name.type('text');

    var phone = document.createElement('input');
    phone.placeholder='Контактный телефон';
    phone.maxLength=10;
    // phone.type('text');

    var orderbtn = document.createElement('button');
    orderbtn.onclick = function () {
    orderCall(buttonElement,name,phone)
    };

    callOrderForm.appendChild(name);
    callOrderForm.appendChild(phone);
    callOrderForm.appendChild(orderbtn);

    buttonElement.parentNode.insertBefore(callOrderForm,buttonElement.nextSibling);
}

function hideCallOrderForm(buttonElement) {
    callOrderForm.parentNode.removeChild(callOrderForm);
    callOrderForm=null;
    buttonElement.onclick=function () {showCallOrderForm(buttonElement)};
}

function orderCall(buttonElement,name,phone){
    var _csrf = document.getElementById('_csrf').value;
    var stationID=document.getElementById('stationID').value;
    console.log("[orderCall]: name.value: "+name.value);
    console.log("[orderCall]: phone.value: "+phone.value);
    console.log("[orderCall]: _csrf: "+_csrf);
    console.log("[orderCall]: stationID: "+stationID);

    hideCallOrderForm(buttonElement);

    var formData= new FormData();
    formData.append("name",name.value);
    formData.append("phone",phone.value);
    formData.append("stationID",stationID);
    formData.append("_csrf",_csrf);

    $.ajax({
        url: '/station/order_call',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        success: function (msg) {
            console.log("[success]: message: "+msg);
        }
    });
}