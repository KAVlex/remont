function jq( myid ) {
 
    return myid.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" );
 
}

function connect() {
	socket = new SockJS('/chat');
	stompClient = Stomp.over(socket);
	stompClient.connect('', '', function(frame) {
		whoami = frame.headers['user-name'];
		console.log('Connected: ' + frame);
		stompClient.subscribe('/user/queue/messages', function(message) {
			showMessage(JSON.parse(message.body));
		});
		stompClient.subscribe('/topic/active', function(activeMembers) {
			showActive(activeMembers);
		});
	});
}

$(document).ready(function() {
	console.log("ready!");
	connect();
});

function showMessage(message) {
	var chatWindowTarget = (message.recipient === whoami) ? message.sender
			: message.recipient;
	var chatContainer = getChatWindow(chatWindowTarget);
	var chatWindow = chatContainer.find('.chat');
	var userDisplay = $('<h5>',
			{
				class : (message.sender === whoami ? 'chat-sender '
						: 'chat-recipient ')
						+ 'media-heading'
			});
	userDisplay.html(message.sender);
	var messageDisplay = $('<small>', {
		class : 'col-sm-11'
	});
	messageDisplay.html(message.message);
	chatWindow.append(userDisplay).append(messageDisplay).append('<br/>');
	// chatWindow.animate({ scrollTop: chatWindow[0].scrollHeight}, 1);
	if (message.sender !== whoami) {
		var sendingUser = $('#user-' + jq(message.sender));
		if (!sendingUser.hasClass('user-selected')
				&& !sendingUser.hasClass('pending-messages')) {
			sendingUser.append(newMessageIcon());
			sendingUser.addClass('pending-messages');
		}
	}
}

function showActive(activeMembers) {
	//renderActive(activeMembers.body);
	stompClient.send('/app/activeUsers', {}, '');
}

function renderActive(activeMembers) {
	var previouslySelected = $('.user-selected').text();
	var usersWithPendingMessages = new Object();
	$.each($('.pending-messages'), function(index, value) {
		usersWithPendingMessages[value.id.substring(5)] = true; // strip the
		// user-
	});
	var members = $.parseJSON(activeMembers);
	var userDiv = $('<div>', {
		id : 'users',
		class : 'row content-wrap'
	});
	$.each(members, function(index, value) {
		if (value === whoami) {
			return true;
		}
		var userLine = $('<div>', {
			id : 'user-' + value,
			class : 'conversation btn'
		});
		userLine.addClass('user-entry');
		if (previouslySelected === value) {
			userLine.addClass('user-selected');
		} else {
			userLine.addClass('user-unselected');
		}
		var userNameDisplay = $('<h5>', {
			class : "media-heading"
		}); // <h5 class="media-heading" id="contactName">Walter White</h5>
		userNameDisplay.html(value);
		userLine.append(userNameDisplay);
		userLine.click(function() {
			var foo = this;
			$('.chat-container').hide();
			$('.user-entry').removeClass('user-selected');
			$('.user-entry').addClass('user-unselected');
			userLine.removeClass('user-unselected');
			userLine.removeClass('pending-messages');
			userLine.addClass('user-selected');
			userLine.children('.newmessage').remove();
			var chatWindow = getChatWindow(value);
			chatWindow.show();
		});
		if (value in usersWithPendingMessages) {
			userLine.append(newMessageIcon());
			userLine.addClass('pending-messages');
		}
		userDiv.append(userLine);
	});
	$('#userList').html(userDiv);
}

function getResponses(element) {
	var id = $(element).val();
	var jqxhr = $.get("/bid/responses?id=" + id).done(
			function(data) {
				renderResponses(data);
			}).fail(
			function() {
				alert("Не удалось получить ответы по выбранной заявке. "
						+ "Попробуйте позже или сообщите администартору");
			}).always(function() {
				// Not need
				});
};

function renderResponses(bidResponses) {
	if (bidResponses.length == 0)
		alert("По данной заявке пока нет ответов");
	var previouslySelected = $('.user-selected').text();
	var usersWithPendingMessages = new Object();
	$.each($('.pending-messages'), function(index, value) {
		usersWithPendingMessages[value.id.substring(5)] = true;
	});
	var userDiv = $('<div>', {
		id : 'users',
		class : 'row content-wrap'
	});
	$.each(bidResponses, function(index, value) {
		var userLine = $('<div>', {
			id : 'user-' + jq(value.station.email),
			name: 'response-' + value.id,
			class : 'conversation btn'
		});
		userLine.addClass('user-entry');
		if (previouslySelected === value) {
			userLine.addClass('user-selected');
		} else {
			userLine.addClass('user-unselected');
		}
		var userNameDisplay = $('<h5>', {
			class : "media-heading"
		}); // <h5 class="media-heading" id="contactName">Walter White</h5>
		userNameDisplay.html(value.station.email + " - " +value.cost);
		userLine.append(userNameDisplay);
		userLine.click(function() {
			var foo = this;
			$('.chat-container').hide();
			$('.user-entry').removeClass('user-selected');
			$('.user-entry').addClass('user-unselected');
			userLine.removeClass('user-unselected');
			userLine.removeClass('pending-messages');
			userLine.addClass('user-selected');
			userLine.children('.newmessage').remove();
			var chatWindow = getChatWindow(value.station.email);
			chatWindow.show();
		});
		if (value in usersWithPendingMessages) {
			userLine.append(newMessageIcon());
			userLine.addClass('pending-messages');
		}
		userDiv.append(userLine);
	});
	$('#userList').html(userDiv);
}

function newMessageIcon() {
	var newMessage = $('<span>', {
		class : 'newmessage'
	});
	newMessage.html('&#x2709;');
	return newMessage;
}

function sendMessageTo(user) {
	var chatInput = '#input-chat-' + jq(user);
	var message = $(chatInput).val();
	if (!message.length) {
		return;
	}
	stompClient.send("/app/chat", {}, JSON.stringify({
		'response' : 2,
		'recipient' : user,
		'message' : message
	}));
	$(chatInput).val('');
	$(chatInput).focus();
}

function getChatWindow(userName) {
	var existingChats = $('.chat-container');
	var elementId = 'chat-' + userName;
	escapeElementId = jq(elementId);
	var containerId = escapeElementId + '-container';
	var selector = '#' + containerId;
	var inputId = 'input-' + elementId;
	if (!$(selector).length) {
		inputId = 'input-' + elementId;
		containerId = elementId + '-container';
		var chatContainer = $('<div>', {
			id : containerId,
			class : 'chat-container row content-wrap messages'
		});
		var chatWindow = $('<div>', {
			class : 'msg'
		});
		var chat = $('<div>', {
			id : elementId,
			class : 'chat media-body'
		});
		chatWindow.append(chat);

		var chatInput = $(
				'<textarea>',
				{
					id : inputId,
					type : 'text',
					class : 'chat-input',
					rows : '2',
					cols : '75',
					placeholder : 'Введите сообщение'
				});
		var chatSubmit = $('<button>', {
			id : 'submit-' + elementId,
			type : 'submit',
			class : 'chat-submit'
		})
		chatSubmit.html('Send');

		chatInput.keypress(function(event) {
			if (event.which == 13) {
				var user = event.currentTarget.id.substring(11); // gets rid
																	// of
																	// 'input-chat-'
				event.preventDefault();
				sendMessageTo(user);
			}
		});

		chatSubmit.click(function(event) {
			var user = event.currentTarget.id.substring(12); // gets rid of
																// 'submit-chat-'
			sendMessageTo(user);
		});

		chatContainer.append(chatWindow);
		chatContainer.append(chatInput);
		chatContainer.append(chatSubmit);

		
		chatContainer.hide();
		$('#Messages').append(chatContainer);
		var responseId = $('.user-selected').attr('name').substring(9);
		getMassages(responseId);
	}
	return $(selector);
}

function getMassages(rId){
	var jqxhr = $.get("/response/messages?id=" + rId).done(
			function(messages) {
				$.each(messages, function(index, message) {
					showMessage(message);	
				});
			}).fail(
			function() {
				alert("Не удалось получить ответы по выбранной заявке. "
						+ "Попробуйте позже или сообщите администартору");
			}).always(function() {
				// Not need
				});
}