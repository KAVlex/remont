/**
 * Created by Tensky on 31.01.2017.
 */
var sms_currentPage;
var sms_hasPrev;
var sms_hasNext;
var checkedCheckboxes;
var checkedIdsDiv;
var crnt_filter='?phone=&?status=';

$(document).ready(function () {
    $('#btnSmsPrev').hide();
    sms_currentPage = 0;
    sms_hasPrev = false;
    sms_hasNext = $('body input[name=init_hasNext]').val();
    if (!sms_hasNext)
        $('#btnSmsNext').hide();
    checkedCheckboxes = [];
    checkedIdsDiv = document.getElementById('checkedIds');
    //todo проверка для чекбокса при нажатии на кнопку вперед/назад
});

//Когда состояние чекбокса изменилось
function cbclk(cbChanged) {
    //Берем значение нового чекбокса (sms.id)
    var val = cbChanged.value;

    //Если он теперь отмечен,
    if (cbChanged.checked)
    //то добавляем в список (массив) отмеченных чекбоксов
        checkedCheckboxes.push(val);
    //иначе
    else {
        //берем его индекс в списке
        var index = checkedCheckboxes.indexOf(val);
        //удаляем из списка 1 (второй параметр) элемент с полученным индексом
        checkedCheckboxes.splice(index, 1);
    }
    //Если отмечены все, то так же отмечаем верхний чекбокс
    checkIfIsAllChecked();

    //Вывод на страницу
    checkedIdsDiv.textContent = 'Выбранные СМС [id] = ';
    for (var i = 0; i < checkedCheckboxes.length; i++) {
        checkedIdsDiv.textContent += checkedCheckboxes[i] + '; ';
    }

    if (checkedCheckboxes.length < 1) {
        $('#status-select').prop('disabled', true);
        if ($('#status-select').val() != null)
            $('#status-select').select2('val', '');
    }
    else {
        $('#status-select').prop('disabled', false);
    }
}

function checkIfIsAllChecked() {
    var cb = $('#table input[type=checkbox]')[0];
    cb.checked = isAllChecked();
}

function isAllChecked() {
    var allcb = $('#table input[type=checkbox]');
    var is = true;
    for (var i = 1; i < allcb.length; i++) {
        if (!allcb[i].checked && !allcb[i].disabled) {
            is = false;
            break;
        }
    }
    return is;
}

function getFilter()
{
    var formData = new FormData();
    formData.append('phone', $('#filter select[name=phone]').val());
    formData.append('status', $('#filter select[name=status]').val());
    formData.append('_csrf', $('#filter input[name=_csrf]').val());
    return formData;
}

function changeFilter()
{
    crnt_filter = '?phone='+$('#filter select[name=phone]').val()+
        '&status='+$('#filter select[name=status]').val()+
        '&date_start='+$('#filter input[name=date_start]').val()+
        '&date_end='+$('#filter input[name=date_end]').val();
}


function getNextSMSPage() {
    if (!sms_hasNext) {
        return;
    }
    var formData = getFilter();
    // formData = getFilter();
    $.ajax({
        url: '/admin/sms/' + (sms_currentPage+1)+'/filter'+crnt_filter,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            // if(result.first)
            //     $('#btnSmsPrev').hide();
            // else
            //     $('#btnSmsPrev').show();
            // if(result.last)
            //     $('#btnSmsNext').hide();
            // else $('#btnSmsNext').show();
            // result = result.content;
            fillTable(result);
            checkIfIsAllChecked();
        },
        error: function (result) {
            console.log(result);
        }
    });
}

function getPrevSMSPage() {
    if (!sms_hasPrev)
        return;
    var formData = getFilter();
    // formData = getFilter();
    // formData.append('ids', checkedCheckboxes);
    // formData.append('status', $('#status-select').val());
    // formData.append('_csrf', $('#_csrf').val());
    console.log(formData);

    $.ajax({
        url: '/admin/sms/' + (sms_currentPage-1)+'/filter'+crnt_filter,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            fillTable(result);
            checkIfIsAllChecked();
        },
        error: function (result) {
            console.log(result);
        }
    });
}

function fillTable(result) {
    // console.log(result);
    sms_currentPage=result.number;
    if (result.first) {
        $('#btnSmsPrev').hide();
        sms_hasPrev = false;
    }
    else {
        $('#btnSmsPrev').show();
        sms_hasPrev = true
    }
    if (result.last) {
        $('#btnSmsNext').hide();
        sms_hasNext = false;
    }
    else {
        $('#btnSmsNext').show();
        sms_hasNext = true;
    }
    result = result.content;
    var trs = document.getElementById('table').getElementsByTagName('tr');
    // console.log("[trs]: ");
    for (var i = 0; i < result.length; i++) {
        var tds = trs[i + 1].getElementsByTagName('td');
        // console.log("[tds]: ");
        // console.log(tds);
        tds[0].textContent = result[i].id;
        tds[1].textContent = result[i].phone;
        tds[2].textContent = result[i].message;
        tds[3].textContent = getStringByStatus(result[i].status);
        var date = new Date(result[i].timestamp);
        tds[4].textContent =
            date.getDate() + '.'
            + getMonthAsString(date.getMonth()) + '.'
            + date.getFullYear() + ' '
            + date.getHours() + ':' + date.getMinutes();

        var cbs = tds[5].getElementsByTagName('input');
        cb = cbs[0];
        cb.value = result[i].id;
        cb.checked = checkedCheckboxes.indexOf(result[i].id.toString()) >= 0;
        cb.disabled = false;
    }
    if (result.length + 1 < trs.length) {
        for (var i = result.length + 1; i < trs.length; i++) {
            var tds = trs[i].getElementsByTagName('td');
            for (var j = 0; j < tds.length - 1; j++) {
                tds[j].textContent = "---";
            }
            var cb = tds[tds.length - 1].getElementsByTagName('input')[0];
            cb.disabled = true;
            cb.checked = false;
        }
    }
}

function getStringByStatus(status)
{
    switch (status)
    {
        case "QUEUE": return 'В очереди на отправку';
        case "PROCESS": return 'В процессе отправки';
        case "SUCCESS": return 'Успешно отправлено';
        case "ERROR": return 'Ошибка при отправке';
        default: return status;
    }
}

function statusChanged() {
    var newStatus=$('#status-select').val();
    if (newStatus == null || newStatus == '')
        return;
    var formData = new FormData();
    formData.append('ids', checkedCheckboxes);
    formData.append('status', $('#status-select').val());
    formData.append('_csrf', $('#_csrf').val());
    $.ajax({
        url: '/admin/sms/changeStatus',
        type: 'POST',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            console.log(result);
            $('#status-select').val('').trigger('change');
            var trs = $('#table tr');
            for (var i = 0; i < result.length; i++) {
                for (var j = 1; j < trs.length; j++) {
                    var tds = trs[j].getElementsByTagName('td');
                    if (tds[0].textContent == result[i].id) {
                        tds[3].textContent = newStatus;
                    }
                }
            }
            alert('Статус выделенных сообщений изменен');
        },
        error: function (result) {
            alert("Не удалось изменить статус сообщения, информация выведена в консоль.");
            console.log(result);
        }
    });
}

function selectAll(cbChanged) {
    var trs = $('#table tr');
    cbPrevState = cbChanged.checked;
    for (var i = 1; i < trs.length; i++) {
        var cb = trs[i].getElementsByTagName('input')[0];
        if (cb.checked != cbChanged.checked) {
            cb.click();
            cbChanged.checked = cbPrevState;
        }
    }
}

function smsFilter() {
    checkedCheckboxes=[];
    checkedIdsDiv.textContent='';
    changeFilter();
    $('#status-select').prop('disabled','disabled');

    var formData = new FormData();
    formData.append('phone', $('#filter select[name=phone]').val());
    formData.append('status', $('#filter select[name=status]').val());
    formData.append('_csrf', $('#filter input[name=_csrf]').val());
    $.ajax({
        url: '/admin/sms/0/filter'+crnt_filter,
        type: 'GET',
        contentType: false,
        processData: false,
        cache: false,
        data: formData,
        dataType: 'json',
        success: function (result) {
            console.log("[result]: ");
            console.log(result);

            sms_currentPage=0;
            fillTable(result);
            checkIfIsAllChecked();

        },
        error: function (result) {
            alert('Ошибка при обработке фильтра, информация выведена в консоль');
            console.log("[error]: ");
            console.log(result);
        }
    });
}

function getMonthAsString(month) {
    switch (month) {
        case 0:
            return 'янв';
        case 1:
            return 'фев';
        case 2:
            return 'мар';
        case 3:
            return 'апр';
        case 4:
            return 'май';
        case 5:
            return 'июн';
        case 6:
            return 'июл';
        case 7:
            return 'авг';
        case 8:
            return 'сен';
        case 9:
            return 'окт';
        case 10:
            return 'ноя';
        case 11:
            return 'дек';
        default:
            console.log("[getMonthAsString]: input month number is: " + month);
    }
}