<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="679" align="center" style="width: 679px; border-collapse: collapse; margin: 0px auto 0px auto;">
    <tbody>
    <tr style="height: 80px;">
        <td style="width: 50%; text-align: left; padding-top: 12px; padding-left: 17px;">
            <img src="${link}/autoservice/img/logo.png" style="max-width: 184px; width: 100%;">
        </td>
        <td style="width: 50%; text-align: right; padding-top: 5px; padding-right: 20px;">
            <div style="font-family: Arial; font-size: 1em;">8 (937) 428 53 76</div>
        </td>
    </tr>

    <tr style="max-height: 315px; height: 100%;">
        <td colspan="3" valign="bottom">
            <hr/>
        </td>
    </tr>

    <tr>
        <td colspan="3">
            <table width="100%" style="max-width: 679px" cellpadding="0" cellspacing="0">
                <tbody><tr><td valign="top" style="background: #fff; padding: 0 30px;">
                    <p>Здравствуйте, ${user.getName()}!</p>
                    <p>Статус заявки №${bid.getId()} изменился на "${bid.getStatus()}"</p>
                    <p><i>${message}</i></p>
                    <p>Перейти к <a target="_blank" href="${link}/mycabinet">списку заявок</a></p>
                    <br>
                    <hr>
                    <p> Спасибо, что выбрали нас<br>
                    </p><p> С Уважением,<br>
                    Администрация <b>GrandRemAvto</b></p>
                    <p></p></td>
                </tr></tbody></table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>