<div>
    <div>
        <a href="/station/${response.station.id}">
        <#if response.station.photo??>
            <img style="border-radius: 50%; position: relative; float: left; display: inline-block"
                 src="/images/?filename=${response.station.photo.pathSmall}"/>
        <#else>
            <img style="border-radius: 50%; position: relative; float: left; display: inline-block"
                 src="/common/autoservice/img/avatar_default.png"/>
        </#if>
        </a>
        <div class="title" style="position: relative;">
            <a href="/station/${response.station.id}">
                <span class="span_name" style="display: block">${response.station.name}</span>
            </a>
        <#if response.station.rating??>
            <#assign rating=response.station.rating>
            <#list 0..4 as i>
                <#if i<rating>
                    <img src="/autoservice/img/zvezda_1.png"/>
                <#else>
                    <img src="/autoservice/img/zvezda_null.png"/>
                </#if>
            </#list>
        <#else>
            <span>Оценок нет</span>
        </#if>
        </div>
    </div>
    <div class="item_otsyv">
        <div class="otsyv_title" style="display: block; clear: left">
            Комментарий станции
        </div>
        <div align="justify">
        ${response.comment}
        </div>
        <div class="otsyv_title">
            Цена
        </div>
        <div align="justify">
        ${response.cost}&#8381;
        </div>
        <div class="otsyv_title">
            Время ответа: <span class="span_date" style="display: inline-block">${response.time}</span>
        </div>
    </div>
    <div>
        <script>
            function acceptFromMap(bidId, statLog) {
                var inputs = $('#modalClientAcceptStation').find('input');
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i].name == "bidId") {
                        inputs[i].value = bidId;
                    }
                    else if (inputs[i].name == "stationLogin") {
                        inputs[i].value = statLog;
                    }
                }
                $('#modalClientAcceptStation').modal('show');
            }
        </script>
        <button class="btn btn_main" onclick="acceptFromMap(${response.bid.id},${response.station.phone})">Записаться на
            прием
        </button>
    </div>
</div>