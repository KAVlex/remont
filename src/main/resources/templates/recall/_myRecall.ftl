<#-- @ftlvariable name="recall" type="com.ss.remont.entity.Recall" -->
<div class="item_otsyv">
    <div class="col-md-2">
    <#if recall.client.photo??>
        <img src="/images/?filename=${recall.client.photo.pathSmall}" class="img-responsive"
             style="border-radius: 50%; position: relative; float: left; display: inline-block"/>
    <#else>
        <img src="/common/autoservice/img/avatar_default.png" class="img-responsive"
             style="border-radius: 50%; position: relative; float: left; display: inline-block"/>
    </#if>
    </div>
    <div class="col-md-10">
        <div class="title">
            <span class="span_name" text="${recall.client.name}"/>
                <span class="span_car">
                    <#assign car=(recall.bid.car.mark.name+' '+recall.bid.car.model.name)>
                    <#if car?length \lte 20 >
                        ${car}
                    <#else>
                    ${car?substring(0,17)+'...'}
                    </#if>
                </span>
                <span class="span_date">
                    ${recall.timestamp?string["dd.MMM.yyyy HH:mm"]}
                </span>
        </div>
        <div class="zvezda">
        <#assign rating = recall.rating>
        <#list 0..4 as i>
            <#if i < rating>
                <img src="/autoservice/img/zvezda_1.png"/>
            <#else>
                <img src="/autoservice/img/zvezda_null.png"/>
            </#if>
        </#list>
        </div>
        <div class="otsyv_title">Отзыв</div>
        <div>${recall.comment}</div>
    </div>
    <div class="cl"></div>
</div>