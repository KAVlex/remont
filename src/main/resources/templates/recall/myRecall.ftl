<#-- @ftlvariable name="recall" type="com.ss.remont.entity.Recall" -->
<div class="modal-content">
    <div class="modal-header">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <img src="/autoservice/img/recall.png"/>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <button class="close" type="button" data-dismiss="modal"></button>
                <h4 class="modal-title">Отзыв</h4>
            </div>
        </div>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-3"></div>
            <!--/*@thymesVar id="recalls" type="com.ss.remont.entity.Recall"*/-->
            <!--/*@thymesVar id="recall" type="com.ss.remont.entity.Recall"*/-->
            <!--/*@thymesVar id="authUser" type="com.ss.remont.auth.AuthUser"*/-->
            <div class="col-md-6">
                <div class="zvezda">
                    <span class="span_name">Оценка</span>
                <#assign rating = recall.rating>
                <#list 0..4 as i>
                    <#if i < rating>
                        <img src="/autoservice/img/zvezda_1.png"/>
                    <#else>
                        <img src="/autoservice/img/zvezda_null.png"/>
                    </#if>
                </#list>
                </div>
                <div align="justify">
                ${recall.comment}
                </div>
            </div>
        </div>
    </div>
</div>